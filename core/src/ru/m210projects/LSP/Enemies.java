// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import org.jetbrains.annotations.Nullable;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.LSP.Types.PlayerStruct;

import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.*;
import static ru.m210projects.LSP.Screens.GameScreen.getMap;
import static ru.m210projects.LSP.Sounds.*;
import static ru.m210projects.LSP.Sprites.changehealth;

public class Enemies {

    public static final int[] gEnemyClock = new int[MAXSPRITES];
    public static final short[] gMoveStatus = new short[MAXSPRITES];
    public static final short[] nKills = new short[6];
    public static final short[] nTotalKills = new short[6];
    public static int nEnemyKills, nEnemyMax;
    private static final Object[] attackFrames = new Object[MAXSPRITES];
    private static final short[] timeCounter = new short[MAXSPRITES];
    private static final short[] frameIndex = new short[MAXSPRITES];
    private static final Frame[] blueattack = {new Frame(1556, 20, null), new Frame(1557, 12, i -> {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int target = spr.getHitag();
        if (target != -1) {
            int dx = gPlayer[target].x - spr.getX();
            int dy = gPlayer[target].y - spr.getY();
            if (klabs(dx) + klabs(dy) < 1024) {
                changehealth(target, -((engine.krand() & 3) + nDifficult));
                playsound(31, i);
//						if (dword_516B4 != 0) XXX
//							sub_32E4C(500, 10, 10);
            }
        }
    }), new Frame(1558, 12, null),};
    private static final Callback greencallback = i -> {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        Sprite nspr = buildProjectile(i, spr.getHitag(), (engine.krand() & 3) + 2032);
        if (nspr != null) {
            nspr.setZ(nspr.getZ() - 2 * 3584);
            nspr.setPicnum(1840);
            nspr.setShade(-20);
            nspr.setXrepeat(32);
            nspr.setYrepeat(32);
            nspr.setLotag((short) (-4 - nDifficult));
            playsound(6, i);
        }
    };

    private static final Frame[] greenattack = {new Frame(1812, 12, null), new Frame(1813, 12, greencallback), new Frame(1814, 12, null),};
    private static final Callback redcallback = i -> {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        Sprite nspr = buildProjectile(i, spr.getHitag(), (engine.krand() & 1) + 2032);
        if (nspr != null) {
            nspr.setZ(nspr.getZ() - 2 * 2560);
            nspr.setPicnum(2096);
            nspr.setShade(-20);
            nspr.setLotag((short) (-8 - nDifficult));
            playsound(8, i);
        }
    };
    private static final Frame[] redattack = {new Frame(2068, 8, null), new Frame(2069, 8, null), new Frame(2070, 8, null),
            new Frame(2071, 8, null), new Frame(2072, 8, null), new Frame(2073, 12, redcallback),};
    private static final Callback purpcallback = i -> {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        Sprite nspr = buildProjectile(i, spr.getHitag(), (engine.krand() & 1) + 2040);
        if (nspr != null) {
            nspr.setZ(nspr.getZ() - 2 * 2560);
            nspr.setPicnum(2352);
            nspr.setShade(-20);
            nspr.setLotag((short) (-16 - nDifficult));
            playsound(0, i);
        }
    };
    private static final Frame[] purpattack = {new Frame(2324, 8, null), new Frame(2325, 8, null), new Frame(2326, 8, null),
            new Frame(2327, 16, purpcallback)};
    private static final Callback yellowcallback = i -> {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        Sprite nspr = buildProjectile(i, spr.getHitag(), (engine.krand() & 1) + 2040);
        if (nspr != null) {
            nspr.setZ(nspr.getZ() - 2 * 2560);
            nspr.setPicnum(2608);
            nspr.setShade(-25);
            nspr.setLotag((short) (-32 - nDifficult));
            playsound(2, i);
        }
    };
    private static final Frame[] yellattack = {new Frame(2580, 20, null), new Frame(2581, 12, null),
            new Frame(2582, 12, yellowcallback), new Frame(2583, 12, null),};
    private static int ocount;
    private static BuildPos out = new BuildPos();

    private static void setframes(int i, Frame[] frms) {
        Sprite spr = boardService.getSprite(i);
        if (spr != null) {
            spr.setExtra(spr.getPicnum());

            frameIndex[i] = 0;
            attackFrames[i] = frms;

            spr.setPicnum(frms[0].tile);
            timeCounter[i] = frms[0].ticksPerFrame;
        }
    }

    private static boolean frameprocess(Frame[] frms, final int sn) {
        timeCounter[sn] -= TICSPERFRAME;
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return false;
        }

        if (cfg.bOriginal) {
            switch (spr.getExtra()) {
                case GREENDUDE:
                    greencallback.invoke(sn);
                    break;
                case REDDUDE:
                    redcallback.invoke(sn);
                    break;
                case PURPLEDUDE:
                    purpcallback.invoke(sn);
                    break;
                case YELLOWDUDE:
                    yellowcallback.invoke(sn);
                    break;
            }
        }

        if (timeCounter[sn] < 0) {
            if (++frameIndex[sn] >= frms.length) {
                spr.setPicnum(spr.getExtra());
                engine.changespritestat(sn, CHASE);
                attackFrames[sn] = null;
                return true;
            }

            Frame frm = frms[frameIndex[sn]];
            timeCounter[sn] += frm.ticksPerFrame;
            if (frm.trigger != null) {
                frm.trigger.invoke(sn);
            }
            spr.setPicnum(frm.tile);
        }

        return false;
    }

    @Nullable
    private static Sprite buildProjectile(int i, int target, int dang) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return null;
        }

        int j = engine.insertsprite(spr.getSectnum(), PROJECTILE);
        if (j == -1) {
            return null;
        }

        Sprite nspr = boardService.getSprite(j);
        if (nspr == null) {
            return null;
        }

        nspr.setX(spr.getX());
        nspr.setY(spr.getY());
        nspr.setZ(spr.getZ());
        nspr.setCstat(128);
        nspr.setXrepeat(64);
        nspr.setYrepeat(64);

        int dx = gPlayer[target].x - spr.getX();
        int dy = gPlayer[target].y - spr.getY();
        nspr.setAng((short) ((EngineUtils.getAngle(dx, dy) + dang) & 0x7FF));
        nspr.setXvel((short) (EngineUtils.sin((nspr.getAng() + 2560) & 0x7FF) >> 6));
        nspr.setYvel((short) (EngineUtils.sin((nspr.getAng() + 2048) & 0x7FF) >> 6));
        nspr.setZvel((short) (((gPlayer[target].z + 8192 - nspr.getZ()) << 8) / EngineUtils.sqrt(dx * dx + dy * dy)));
        nspr.setOwner((short) i);
        nspr.setHitag((short) target);

        return nspr;
    }

    public static void inienemies() {
        // #GDX 13.06.2024
        Arrays.fill(gEnemyClock, (short) 0);
        Arrays.fill(attackFrames, null);
        Arrays.fill(timeCounter, (short) 0);
        Arrays.fill(frameIndex, (short) 0);

        Arrays.fill(gMoveStatus, (short) 1);
        Arrays.fill(nKills, (short) 0);
        Arrays.fill(nTotalKills, (short) 0);

        nEnemyKills = 0;
        nEnemyMax = 0;

        // attack animate
        engine.getTile(1556).disableAnimation();
        engine.getTile(1812).disableAnimation();
        engine.getTile(2068).disableAnimation();
        engine.getTile(2324).disableAnimation();
        engine.getTile(2580).disableAnimation();

        for (short i = 0; i < MAXSPRITES; i++) {
            Sprite spr = boardService.getSprite(i);
            if (spr == null) {
                return;
            }

            if (spr.getStatnum() >= 32) {
                continue;
            }

            switch (spr.getPicnum()) {
                case 232: // budda
                case 1410: // skull
                    engine.changespritestat(i, GUARD);
                    spr.setLotag(9999);
                    break;
                case BLUEDUDE:
                    engine.changespritestat(i, GUARD);
                    spr.setLotag(24);
                    nTotalKills[1]++;
                    nEnemyMax++;
                    break;
                case GREENDUDE:
                    engine.changespritestat(i, GUARD);
                    spr.setLotag(48);
                    nTotalKills[2]++;
                    nEnemyMax++;
                    break;
                case REDDUDE:
                    engine.changespritestat(i, GUARD);
                    spr.setLotag(72);
                    nTotalKills[3]++;
                    nEnemyMax++;
                    break;
                case PURPLEDUDE:
                    engine.changespritestat(i, GUARD);
                    spr.setLotag(240);
                    nTotalKills[4]++;
                    nEnemyMax++;
                    break;
                case YELLOWDUDE:
                    engine.changespritestat(i, GUARD);
                    spr.setLotag(480);
                    nTotalKills[5]++;
                    nEnemyMax++;
                    break;
            }

            switch (spr.getPicnum()) {
                case 50: // concrete
                case 232: // budda
                case 1410: // skull
                case 1536: // blue enemy
                case 1600: // empty
                case 1700: // empty
                case 1792: // green enemy
                case 2048: // red enemy
                case 2304: // purple enemy
                case 2560: // yellow enemy
                    spr.setCstat(spr.getCstat() | 257);
                    break;
            }

            if (nNextMap == 6) {
                switch (spr.getPicnum()) {
                    case 2608: // delete projectiles
                    case 2356:
                    case 2352:
                    case 2096:
                    case 1844:
                    case 1840:
                    case 1374:
                    case 1369:
                    case 1364:
                    case 1358:
                    case 1354:
                    case 1345:
                    case 1341:
                    case 1335:
                    case 1328:
                    case 1259:
                    case 1225:
                    case 1214:
                    case 1203:
                    case 1192:
                    case 1179:
                    case 1166:
                    case 1156:
                    case 1146:
                    case 1136:
                    case 1126:
                    case 1116:
                    case 249:
                        engine.deletesprite(i);
                        break;
                    case 2612:
                        break;
                }
            }
        }
    }

    public static void enemydie(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null || spr.getStatnum() == DYING) {
            return;
        }

        switch (spr.getPicnum()) {
            case BLUEDUDE:
                nKills[1]++;
                nEnemyKills++;
                break;
            case GREENDUDE:
                nKills[2]++;
                nEnemyKills++;
                break;
            case REDDUDE:
                nKills[3]++;
                nEnemyKills++;
                break;
            case PURPLEDUDE:
                nKills[4]++;
                nEnemyKills++;
                break;
            case YELLOWDUDE:
                nKills[5]++;
                nEnemyKills++;
                break;
        }

        for (short sec = 0; sec < boardService.getSectorCount(); sec++) {
            for (ListNode<Sprite> node = boardService.getSectNode(sec); node != null; node = node.getNext()) {
                int j = node.getIndex();
                Sprite pspr = node.get();
                switch (pspr.getPicnum()) {
                    case BLUEDUDE:
                    case GREENDUDE:
                    case REDDUDE:
                    case PURPLEDUDE:
                    case YELLOWDUDE:
                        if (engine.cansee(spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum(), pspr.getX(), pspr.getY(),
                                pspr.getZ() - (engine.getTile(pspr.getPicnum()).getHeight() << 7), pspr.getSectnum())) {
                            if (pspr.getStatnum() == GUARD) {
                                engine.changespritestat(j, CHASE);
                            }
                        }
                        break;
                }
            }
        }

        if (spr.getPicnum() != PURPLEDUDE && spr.getPicnum() != YELLOWDUDE) {
            spr.setPicnum(spr.getPicnum() + 4 * (engine.krand() & 1) + 28);
        } else {
            spr.setPicnum(spr.getPicnum() + 28);
        }
        spr.setCstat(0);
        spr.setLotag(240);
        spr.setOwner(spr.getPicnum());
        engine.changespritestat((short) i, DYING);
    }

    //	private static int player_dist;
    private static int findplayer(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return -1;
        }

        int mindist = 0x7fffffff;
        int target = connecthead;

        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
            int dist = klabs(spr.getX() - gPlayer[p].x) + klabs(spr.getY() - gPlayer[p].y);
            if (dist < mindist) {
                mindist = dist;
                target = p;
            }
        }
        return target;
    }

    public static void moveenemies() {
        int j;
        int dx, dy;

        int ang;
        int dang;
        boolean canAttack;
        int count = 0;

        ListNode<Sprite> next;
        for (ListNode<Sprite> node = boardService.getStatNode(ATTACK); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();

            Sprite spr = node.get();
            switch (spr.getExtra()) {
                case GREENDUDE:
                case REDDUDE:
                case PURPLEDUDE:
                case YELLOWDUDE:
                case BLUEDUDE:
                    if (!frameprocess((Frame[]) attackFrames[i], i)) {
                        count++;
                    }
                    break;
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(CHASE); node != null; node = next) {
            next = node.getNext();

            int target = findplayer(node.getIndex());
            int v56 = engine.krand();

            Sprite spr = node.get();
            int i = node.getIndex();
            switch (spr.getPicnum()) {
                case 232:
                    if (cfg.bOriginal || (totalmoves % 4) == 0) {
                        if (engine.cansee(gPlayer[target].x, gPlayer[target].y, gPlayer[target].z, gPlayer[target].sectnum,
                                spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(232).getHeight() << 7), spr.getSectnum()) && (engine.krand() & 3) == 0) {
                            j = engine.insertsprite(spr.getSectnum(), PROJECTILE);
                            Sprite spr2 = boardService.getSprite(j);
                            if (spr2 == null) {
                                continue;
                            }

                            spr2.setX(spr.getX());
                            spr2.setY(spr.getY());
                            spr2.setZ(spr.getZ() - 2560);
                            spr2.setCstat(128);
                            spr2.setPicnum(1179);
                            spr2.setShade(-20);
                            spr2.setXrepeat(32);
                            spr2.setYrepeat(32);
                            spr2.setAng((short) (engine.krand() & 0x7FF));
                            spr2.setXvel((short) (EngineUtils.sin((spr2.getAng() + 2560) & 0x7FF) >> 6));
                            spr2.setYvel((short) (EngineUtils.sin((spr2.getAng() + 2048) & 0x7FF) >> 6));
                            dx = gPlayer[target].x - spr2.getY();
                            dy = gPlayer[target].y - spr2.getX();
                            spr2.setZvel((short) (((gPlayer[target].z + 2048 - spr2.getZ()) << 8) / EngineUtils.sqrt(dx * dx + dy * dy)));
                            spr2.setOwner(i);
                            spr2.setLotag(-8);
                        }
                    }
                    break;
                case 1410:
                    if (cfg.bOriginal || (totalmoves % 4) == 0) {
                        if (engine.cansee(spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum(), gPlayer[target].x,
                                gPlayer[target].y, gPlayer[target].z, gPlayer[target].sectnum)
                                && (engine.krand() & 7) == 0) {
                            j = engine.insertsprite(spr.getSectnum(), PROJECTILE);
                            Sprite spr2 = boardService.getSprite(j);
                            if (spr2 == null) {
                                continue;
                            }

                            spr2.setX(spr.getX());
                            spr2.setY(spr.getY());
                            spr2.setZ(spr.getZ() - 2560);
                            spr2.setCstat(128);
                            spr2.setPicnum(1322);
                            spr2.setShade(-20);
                            spr2.setXrepeat(32);
                            spr2.setYrepeat(32);

                            dx = gPlayer[target].x - spr.getX();
                            dy = gPlayer[target].y - spr.getY();
                            spr2.setAng((short) (((engine.krand() & 1) + EngineUtils.getAngle(dx, dy) + 2040) & 0x7FF));
                            spr2.setXvel((short) (EngineUtils.sin((spr2.getAng() + 2560) & 0x7FF) >> 6));
                            spr2.setYvel((short) (EngineUtils.sin((spr2.getAng() + 2048) & 0x7FF) >> 6));
                            spr2.setZvel((short) (((gPlayer[target].z + 2048 - spr2.getZ()) << 8)
                                    / EngineUtils.sqrt(dx * dx + dy * dy)));
                            spr2.setOwner(i);
                            spr2.setLotag(-16);
                            playsound(0, i);
                        }
                    }
                    break;
                case GREENDUDE:
                case REDDUDE:
                case PURPLEDUDE:
                case YELLOWDUDE:
                case BLUEDUDE:
                    Sector sec = boardService.getSector(spr.getSectnum());
                    if (sec == null) {
                        break;
                    }

                    count++;
                    if (lockclock - gEnemyClock[i] < 0) {
                        gEnemyClock[i] = lockclock;
                    }

                    game.pInt.setsprinterpolate(i, spr);
                    
                    if (spr.getZ() < sec.getFloorz()) {
                        spr.setZ(spr.getZ() + (18 << 6));
                    }

                    int h = ((engine.getTile(spr.getPicnum()).getHeight() * spr.getYrepeat()) << 2);
                    int zTop = spr.getZ() - h;
                    if (spr.getZ() >= sec.getFloorz() && zTop < sec.getFloorz()) {
                        spr.setZ(sec.getFloorz());
                    }

                    if ((sec.getLotag() == 4 || sec.getLotag() == 5)) {
                        spr.setZ(sec.getFloorz() + h / 2);
                    }

                    dx = gPlayer[target].x - spr.getX();
                    dy = gPlayer[target].y - spr.getY();
                    int dz = (gPlayer[target].z + 0x2000) - spr.getZ();

                    ang = EngineUtils.getAngle(dx, dy);
                    dang = ((1024 + ang - spr.getAng()) & 0x7FF) - 1024;

                    boolean cansee = engine.cansee(gPlayer[target].x, gPlayer[target].y, gPlayer[target].z,
                            gPlayer[target].sectnum, spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum());

                    if (spr.getPicnum() == BLUEDUDE) {
                        canAttack = false;
                        if (cansee && klabs(dx) + klabs(dy) < 1024 && klabs(dz) < 8192) {
                            spr.setAng(ang);
                            canAttack = true;
                        }
                    } else {
                        canAttack = cansee;
                    }

                    if ((v56 % 10) == 9 && gMoveStatus[i] != 1) {
                        if (gMoveStatus[i] == 2 && lockclock - gEnemyClock[i] > 180 || gMoveStatus[i] == 0) {
                            gMoveStatus[i] = 1;
                            gEnemyClock[i] = lockclock;
                        }
                    } else if ((v56 % 10) == 0 && klabs(dang) < 256) {
                        if (lockclock - gEnemyClock[i] > 60) {
                            if (canAttack && gPlayer[target].nHealth > 0) {
                                spr.setHitag(target);
                                switch (spr.getPicnum()) {
                                    case GREENDUDE:
                                        setframes(i, greenattack);
                                        break;
                                    case REDDUDE:
                                        setframes(i, redattack);
                                        break;
                                    case PURPLEDUDE:
                                        setframes(i, purpattack);
                                        break;
                                    case YELLOWDUDE:
                                        setframes(i, yellattack);
                                        break;
                                    case BLUEDUDE:
                                        setframes(i, blueattack);
                                        break;
                                }
                                engine.changespritestat(i, ATTACK);
                                gMoveStatus[i] = 0;
                                break;
                            }
                            gEnemyClock[i] = lockclock;
                        }
                    } else if (lockclock - gEnemyClock[i] > 120) {
                        spr.setAng(ang);

                        int v30 = (engine.krand() & 256) - 128;
                        spr.setAng((short) ((v30 + spr.getAng()) & 0x7FF));
                        gEnemyClock[i] = lockclock;
                    }

                    if (gMoveStatus[i] == 0) {
                        break;
                    }

                    out.x = EngineUtils.cos(spr.getAng() & 0x7FF) >> 9;
                    out.y = EngineUtils.sin(spr.getAng() & 0x7FF) >> 9;

                    if (klabs(dang) < 256) {
                        out = sub_16849(target, i, out.x, out.y);
                    }

                    short sect = spr.getSectnum();
                    int move = engine.movesprite(i, out.x, out.y, 0, spr.getClipdist() << 2, 1024, 1024, CLIPMASK0,
                            TICSPERFRAME);

                    engine.pushmove(spr.getX(), spr.getY(), spr.getZ() - h / 2, sect, spr.getClipdist() << 2, 1024, 1024, CLIPMASK0);
                    spr.setX(pushmove_x);
                    spr.setY(pushmove_y);
                    spr.setZ(pushmove_z + h / 2);
                    if (sect != pushmove_sectnum) {
                        engine.changespritesect(i, pushmove_sectnum);
                    }

                    if (move == 0) {
                        break;
                    }

                    if (spr.getPicnum() == BLUEDUDE) {
                        if ((engine.krand() & 3) == 0) {
                            spr.setAng(EngineUtils.getAngle(dx, dy));
                            break;
                        }
                    } else {
                        if (((spr.getAng() + 2048 - ang) & 0x7FF) >= 1024) {
                            spr.setAng((short) (spr.getAng() + 2 * TICSPERFRAME));
                        } else {
                            spr.setAng((short) ((spr.getAng() + 2048) - 2 * TICSPERFRAME));
                        }
                        spr.setAng(spr.getAng() & 0x7FF);
                    }

                    break;
            }
        }

        if (count < 1 && ocount != count) {
            nMusicClock = lockclock;
        }

        if (count < 1 && (getMap(mapnum).music - 1) != currSong) {
            if (lockclock - nMusicClock > 780) {
                startmusic(getMap(mapnum).music - 1);
            }
        } else if (count > 1 && currSong != 7) {
            startmusic(7);
        }

        ocount = count;

        ListNode<Sprite> i = boardService.getStatNode(DYING), nexti;
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 210 && spr.getLotag() > 175) {
                spr.setPicnum((short) (spr.getOwner() + 1));
            } else if (spr.getLotag() < 155 && spr.getLotag() > 120) {
                spr.setPicnum((short) (spr.getOwner() + 2));
            } else if (spr.getLotag() < 75 && spr.getLotag() > 45) {
                spr.setPicnum((short) (spr.getOwner() + 3));
            } else if (spr.getLotag() < 0) {
                spr.setPicnum((short) (spr.getOwner() + 8));
                engine.changespritestat(i.getIndex(), INANIMATE);
            }

            i = nexti;
        }

        i = boardService.getStatNode(GUARD);
        while (i != null) {
            nexti = i.getNext();
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                int k = i.getIndex();
                if (isvisible(k, p)) {
//					if (word_51767 != 8)
//						sub_1F401(500); XXX
                    engine.changespritestat(k, CHASE);
                    switch (i.get().getPicnum()) {
                        case 1536: // blue enemy
                            playsound(28, k);
                            break;
                        case 1792: // green enemy
                            playsound(34, k);
                            break;
                        case 2048: // red enemy
                            playsound(53, k);
                            break;
                        case 2304: // purple enemy
                            playsound(52, k);
                            break;
                        case 2560: // yellow enemy
                            playsound(63, k);
                            break;
                    }
                }
            }
            i = nexti;
        }
    }

    public static boolean isvisible(int i, int target) {
        PlayerStruct plr = gPlayer[target];
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return false;
        }

        int sin = EngineUtils.sin(spr.getAng() & 2047);
        int cos = EngineUtils.sin((spr.getAng() + 512) & 2047);

        if (plr.sectnum >= 0 && (cos * (plr.x - spr.getX())) + (sin * (plr.y - spr.getY())) >= 0) {
            return engine.cansee(plr.x, plr.y, plr.z, plr.sectnum, spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                    spr.getSectnum());
        }
        return false;
    }

    private static BuildPos sub_16849(int nTarget, int nSprite, int xvel, int yvel) {
        out.x = xvel;
        out.y = yvel;

        Sprite spr = boardService.getSprite(nSprite);
        if (spr == null) {
            return out;
        }

        int dx = gPlayer[nTarget].x - spr.getX();
        int dy = gPlayer[nTarget].y - spr.getY();

        boolean v19 = false;
        boolean v18 = false;
        if (xvel < 0 && (dx - xvel) < -350 || xvel > 0 && (dx - xvel) > 350) {
            v19 = true;
        }

        if (yvel < 0 && (dy - yvel) < -350 || yvel > 0 && (dy - yvel) > 350) {
            v18 = true;
        }

        if (!v19) {
            int vel = dx < 0 ? (dx + 350) : (dx - 350);

            if ((klabs(vel) == vel) == (klabs(xvel) == xvel)) {
                out.x = vel;
            } else {
                out.x = 0;
            }
        }

        if (!v18) {
            int vel = dy < 0 ? (dy + 350) : (dy - 350);

            if ((klabs(vel) == vel) == (klabs(yvel) == yvel)) {
                out.y = vel;
            } else {
                out.y = 0;
            }
        }
        return out;
    }

    private interface Callback {
        void invoke(int spriteid);
    }

    private static class Frame {
        public final short tile;
        public final Callback trigger;
        public final short ticksPerFrame;

        public Frame(int tile, int ticksPerFrame, Callback trigger) {
            this.tile = (short) tile;
            this.ticksPerFrame = (short) ticksPerFrame;
            this.trigger = trigger;
        }
    }

}
