// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;
import ru.m210projects.Build.Render.RenderedSpriteList;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.LSP.Factory.LSPRenderer;
import ru.m210projects.LSP.Screens.DemoScreen;

import java.util.concurrent.atomic.AtomicInteger;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Pragmas.scale;
import static ru.m210projects.Build.Strhandler.*;
import static ru.m210projects.LSP.Enemies.nEnemyKills;
import static ru.m210projects.LSP.Enemies.nEnemyMax;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.*;
import static ru.m210projects.LSP.Weapons.getIcon;

public class View {

    public static final int kView2D = 2;
    public static final int kView3D = 3;
    public static final int kView2DIcon = 4;
    private static final char[] statbuffer = new char[80];
    public static final AtomicInteger fz = new AtomicInteger();
    public static final AtomicInteger cz = new AtomicInteger();
    public static int nPalDiff, nPalDelay;
    public static int rtint, btint, gtint;

    public static void drawscreen(int snum, int dasmoothratio) {
        int cposx = gPlayer[snum].x;
        int cposy = gPlayer[snum].y;
        int cposz = gPlayer[snum].z;
        float choriz = gPlayer[snum].horiz + gPlayer[snum].nBobbing;
        int czoom = gPlayer[snum].zoom;
        float cang = gPlayer[snum].ang;
        int csect = gPlayer[snum].sectnum;
        LSPRenderer renderer = game.getRenderer();

        if ((!game.menu.gShowMenu && !Console.out.isShowing()) || DemoScreen.isDemoPlaying()) {
            int ix = gPlayer[snum].ox;
            int iy = gPlayer[snum].oy;
            int iz = gPlayer[snum].oz;
            float iHoriz = gPlayer[snum].ohoriz;
            float inAngle = gPlayer[snum].oang;
            int izoom = gPlayer[snum].ozoom;

            ix += mulscale(cposx - gPlayer[snum].ox, dasmoothratio, 16);
            iy += mulscale(cposy - gPlayer[snum].oy, dasmoothratio, 16);
            iz += mulscale(cposz - gPlayer[snum].oz, dasmoothratio, 16);
            iHoriz += ((choriz - gPlayer[snum].ohoriz) * dasmoothratio) / 65536.0f;
            inAngle += ((BClampAngle(cang - gPlayer[snum].oang + 1024) - 1024) * dasmoothratio) / 65536.0f;
            izoom += mulscale(czoom - gPlayer[snum].ozoom, dasmoothratio, 16);

            cposx = ix;
            cposy = iy;
            cposz = iz;
            czoom = izoom;

            choriz = iHoriz;
            cang = inAngle;
        }

        engine.getzsofslope(csect, cposx, cposy, fz, cz);
        int lz = 4 << 8;
        if (cposz < cz.get() + lz) {
            cposz = cz.get() + lz;
        }
        if (cposz > fz.get() - lz) {
            cposz = fz.get() - lz;
        }

        if (gPlayer[snum].gViewMode != kView2DIcon) {
            renderer.drawrooms(cposx, cposy, cposz, cang, choriz, csect);
            analyzesprites(snum, dasmoothratio);
            renderer.drawmasks();

            byte[] gotsector = renderer.getRenderedSectors();
            for (int i = 0; i < boardService.getSectorCount(); i++) {
                if ((gotsector[i >> 3] & pow2char[i & 7]) != 0) {
                    MarkSectorSeen(i);
                }
            }
        }

        if (gPlayer[snum].gViewMode != kView3D) {
            if (followmode) {
                cposx = followx;
                cposy = followy;
                cang = followa;
            }

            if (gPlayer[snum].gViewMode == kView2DIcon) {
                renderer.clearview(0); // Clear screen to specified color
                renderer.drawmapview(cposx, cposy, czoom, (int) cang);
            }
            renderer.drawoverheadmap(cposx, cposy, czoom, (short) cang);
        }
    }

    public static void MarkSectorSeen(int sect) {
        Sector sec = boardService.getSector(sect);
        if (sec == null) {
            return;
        }

        if (!show2dsector.getBit(sect)) {
            show2dsector.setBit(sect);
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                show2dwall.setBit(wn.getIndex());
            }
        }
    }

    public static void analyzesprites(int snum, int smoothratio) {
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        for (int i = 0; i < renderedSpriteList.getSize(); i++) {
            Sprite pTSprite = renderedSpriteList.get(i);
            if (pTSprite.getOwner() == -1) {
                continue;
            }

            ILoc oldLoc = game.pInt.getsprinterpolate(pTSprite.getOwner());
            if (oldLoc != null) {
                int x = oldLoc.x;
                int y = oldLoc.y;
                int z = oldLoc.z;
                short nAngle = oldLoc.ang;

                // interpolate sprite position
                x += mulscale(pTSprite.getX() - oldLoc.x, smoothratio, 16);
                y += mulscale(pTSprite.getY() - oldLoc.y, smoothratio, 16);
                z += mulscale(pTSprite.getZ() - oldLoc.z, smoothratio, 16);
                nAngle += (short) mulscale(((pTSprite.getAng() - oldLoc.ang + 1024) & 0x7FF) - 1024, smoothratio, 16);

                pTSprite.setX(x);
                pTSprite.setY(y);
                pTSprite.setZ(z);
                pTSprite.setAng(nAngle);
            }
        }

        int k;
        int dax = gPlayer[snum].x;
        int day = gPlayer[snum].y;

        for (int i = 0; i < renderedSpriteList.getSize(); i++) {
            Sprite tspr = renderedSpriteList.get(i);
            short owner = tspr.getOwner();
            Sprite ownerSpr = boardService.getSprite(owner);
            if (ownerSpr == null) {
                tspr.setOwner(-1);
                continue;
            }

            int dx = tspr.getX() - dax;
            int dy = tspr.getY() - day;

            k = EngineUtils.getAngle(dx, dy);
            k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;

            switch (tspr.getPicnum()) {
                case 275:
                case 339:
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + ((k + 3) & 4));
                        tspr.setCstat(tspr.getCstat() & ~4);
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k + 3) & 4));
                        tspr.setCstat(tspr.getCstat() | 4);
                    }
                    break;
                case BLUEDUDE:
                case GREENDUDE:
                case REDDUDE:
                case PURPLEDUDE:
                case YELLOWDUDE:
                    if (ownerSpr.getStatnum() == CHASE || ownerSpr.getStatnum() == GUARD) {
                        if (k <= 4) {
                            tspr.setPicnum(tspr.getPicnum() + 4 * k);
                            tspr.setCstat(tspr.getCstat() & ~4);
                        } else {
                            tspr.setPicnum(tspr.getPicnum() + 4 * (8 - k));
                            tspr.setCstat(tspr.getCstat() | 4);
                        }
                    }

                    if (tspr.getStatnum() != 99) {
                        if (renderedSpriteList.getSize() < (MAXSPRITESONSCREEN - 2)) {
                            Sector sec = boardService.getSector(ownerSpr.getSectnum());
                            if (sec != null) {
                                int fz = sec.getFloorz();
                                if (fz > gPlayer[snum].z) {
                                    Sprite tshadow = renderedSpriteList.obtain();
                                    tshadow.set(tspr);
                                    int camangle = EngineUtils.getAngle(dax - tshadow.getX(), day - tshadow.getY());
                                    tshadow.setX(tshadow.getX() - mulscale(EngineUtils.sin((camangle + 512) & 2047), 100, 16));
                                    tshadow.setY(tshadow.getY() + mulscale(EngineUtils.sin((camangle + 1024) & 2047), 100, 16));
                                    tshadow.setZ(fz + 1);
                                    tshadow.setStatnum(99);

                                    tshadow.setYrepeat((short) (tspr.getYrepeat() >> 3));
                                    if (tshadow.getYrepeat() < 4) {
                                        tshadow.setYrepeat(4);
                                    }

                                    tshadow.setShade(127);
                                    tshadow.setCstat(tshadow.getCstat() | 2);
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }

    public static void drawhealth(int snum, int x, int y, int scale, int nFlags) {
        Renderer renderer = game.getRenderer();
        String name = "health: ";
        float fscale = scale / 65536.0f;
        int ydim = renderer.getHeight();

        game.getFont(1).drawText(renderer, x, y, toCharArray(name), fscale, 0, 175, TextAlign.Left, Transparent.None, true);
        x += game.getFont(1).getWidth(name, fscale);

        int nGauge = gPlayer[snum].nHealth * engine.getTile(641).getWidth() / (2 * MAXHEALTH);
        renderer.rotatesprite(x << 16, y << 16, scale, 0, 641, 0, 0, 8 | 16 | nFlags, 0, 0,
                x + scale(nGauge, scale, 65536), ydim);
    }

    public static void drawmana(int snum, int x, int y, int scale, int nFlags) {
        Renderer renderer = game.getRenderer();
        String name = "mana:   ";
        float fscale = scale / 65536.0f;
        int ydim = renderer.getHeight();

        game.getFont(1).drawText(renderer, x, y, toCharArray(name), fscale, 0, 197, TextAlign.Left, Transparent.None, true);
        x += game.getFont(1).getWidth(name, fscale);

        ArtEntry pic = engine.getTile(641);

        int nGauge = gPlayer[snum].nMana * pic.getWidth() / (2 * MAXMANNA);

        renderer.rotatesprite((x - scale(pic.getWidth() / 2, scale, 65536) - 1) << 16, y << 16, scale, 0, 641, 0, 0,
                8 | 16 | nFlags, x, 0, x + scale(nGauge, scale, 65536), ydim);
    }

    public static void drawammo(int snum, int x, int y, int scale, int nFlags) {
        Renderer renderer = game.getRenderer();
        String name = "ammo:   ";
        float fscale = scale / 65536.0f;
        ArtEntry pic = engine.getTile(640);
        int ydim = renderer.getHeight();


        game.getFont(1).drawText(renderer, x, y, toCharArray(name), fscale, 0, 120, TextAlign.Left, Transparent.None, true);
        x += game.getFont(1).getWidth(name, fscale);

        int nGauge = gPlayer[snum].nAmmo[gPlayer[snum].nLastChoosedWeapon] * pic.getWidth() / (2 * MAXAMMO);
        renderer.rotatesprite((x - scale(pic.getWidth() / 2, scale, 65536) - 1) << 16, y << 16, scale, 0, 640, 0, 0,
                8 | 16 | nFlags, x, 0, x + scale(nGauge, scale, 65536), ydim);
    }

    public static void drawbar(int x, int y, int scale, int snum) {
        Renderer renderer = game.getRenderer();
        float fscale = (scale / 65536.0f);

        int sx1 = (int) ((x - 6) * fscale);
        int sy1 = y + (int) (4 * fscale);
        int sx2 = (int) ((x + 230) * fscale);
        int sy2 = y + (int) (45 * fscale);
        renderer.rotatesprite(sx1 << 16, sy1 << 16, scale, 0, 611, 64, 0, 1 | 8 | 16 | 32 | 256, 0, 0, sx2, sy2);

        x *= (int) fscale;
        int yoffs = (int) (10 * fscale);
        if (gPlayer[snum].nWeapon > 6) {
            drawmana(myconnectindex, x, y += yoffs, scale, 256);
        } else {
            drawammo(myconnectindex, x, y += yoffs, scale, 256);
        }

        drawhealth(myconnectindex, x, y += yoffs, scale, 256);

        y += yoffs;
        for (int i = 1; i < 13; i++) {
            if (gPlayer[snum].nAmmo[i] != 0) {
                renderer.rotatesprite((x + 1) << 16, (y + 1) << 16, scale, 0, getIcon(i), 48, 0, 8 | 16 | 256);
                renderer.rotatesprite(x << 16, y << 16, scale, 0, getIcon(i), 0, 0, 8 | 16 | 256);
            } else {
                renderer.rotatesprite((x + 1) << 16, (y + 1) << 16, scale, 0, getIcon(i), 48, 0, 8 | 16 | 256);
            }

            if (gPlayer[snum].nWeapon == i) {
                renderer.rotatesprite((x + 1) << 16, (y + (int) (13 * fscale)) << 16, scale / 2, 0, 9258, 0, 0,
                        8 | 16 | 256);
            }

            x += (int) (19 * fscale);
        }

    }

    public static void TintPalette(int r, int g, int b) {
        r = BClipRange(r, (r != 0 && r < 3) ? 3 : 0, 63);
        g = BClipRange(g, (g != 0 && g < 3) ? 3 : 0, 63);
        b = BClipRange(b, (b != 0 && b < 3) ? 3 : 0, 63);

        if (g != 0 && gtint > 64) {
            return;
        }

        gtint += g;

        if (r != 0 && rtint > 64) {
            return;
        }

        rtint += r;

        if (b != 0 && btint > 64) {
            return;
        }

        btint += b;

        int nDiff = r;
        if (nDiff <= g) {
            nDiff = g;
        }
        if (nDiff <= b) {
            nDiff = b;
        }
        nPalDiff += nDiff;
        nPalDelay = 0;
    }

    public static void GrabPalette() {
        byte[] palette = engine.getPaletteManager().getBasePalette();
        engine.setbrightness(cfg.getPaletteGamma(), palette);

        nPalDiff = 0;
        nPalDelay = 0;
        btint = 0;
        gtint = 0;
        rtint = 0;
    }

    public static void FixPalette() {
        if (nPalDiff == 0) {
            return;
        }

        if (nPalDelay-- > 0) {
            return;
        }

        nPalDelay = 1;
        nPalDiff = BClipLow(nPalDiff - 3, 0);
        rtint = BClipLow(rtint - 3, 0);
        gtint = BClipLow(gtint - 3, 0);
        btint = BClipLow(btint - 3, 0);
    }

    public static void viewDrawStats(int x, int y, int zoom) {
        Renderer renderer = game.getRenderer();
        if (cfg.gShowStat == 0 || cfg.gShowStat == 2 && gPlayer[myconnectindex].gViewMode == kView3D) {
            return;
        }

        int nBits = 256;
        float fscale = (zoom / 65536.0f);

        Font f = game.getFont(1);
        int yoffset = (int) (2.5f * f.getSize() * fscale);
        int sx1 = (int) ((x - 6) * fscale);
        int sy1 = y - (int) (4 * fscale) - yoffset;
        CharInfo charInfo = f.getCharInfo('x');
        int sx2 = (int) ((x + (11 * charInfo.getWidth() + 6)) * fscale);
        int sy2 = y - (int) fscale;

        renderer.rotatesprite(sx1 << 16, sy1 << 16, zoom, 0, 611, 64, 0, 1 | 8 | 16 | 32 | nBits, 0, 0, sx2, sy2);

        buildString(statbuffer, 0, "k: ");
        int alignx = f.getWidth(statbuffer, fscale);

        x *= (int) fscale;
        y -= yoffset;

        int statx = x;
        int staty = y;

        f.drawText(renderer, statx, staty, statbuffer, fscale, 0, 175, TextAlign.Left, Transparent.None, true);

        int offs = Bitoa(nEnemyKills, statbuffer);
        buildString(statbuffer, offs, " / ", nEnemyMax);
        f.drawText(renderer, statx + (alignx + 2), staty, statbuffer, fscale, 0, 4, TextAlign.Left, Transparent.None, true);

        staty = (int) (y + (f.getSize() * fscale));

        buildString(statbuffer, 0, "t: ");
        f.drawText(renderer, statx, staty, statbuffer, fscale, 0, 175, TextAlign.Left, Transparent.None, true);
        alignx = f.getWidth(statbuffer, fscale);

        int sec = (totalmoves / 30) % 60;
        int minutes = (totalmoves / (30 * 60)) % 60;
        int hours = (totalmoves / (30 * 3600)) % 60;

        offs = Bitoa(hours, statbuffer, 2);
        offs = buildString(statbuffer, offs, ":", minutes, 2);
        buildString(statbuffer, offs, ":", sec, 2);
        f.drawText(renderer, statx + (alignx + 2), staty, statbuffer, fscale, 0, 4, TextAlign.Left, Transparent.None, true);
    }
}
