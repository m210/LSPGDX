// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import org.jetbrains.annotations.NotNull;
import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Pattern.LogSender;
import ru.m210projects.Build.Types.MemLog;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.CommandResponse;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.commands.OsdCallback;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.LSP.Factory.*;
import ru.m210projects.LSP.Menus.*;
import ru.m210projects.LSP.Screens.*;
import ru.m210projects.LSP.Types.LSPDatFile;
import ru.m210projects.LSP.Types.PlayerStruct;
import ru.m210projects.LSP.Types.SwingDoor;

import java.io.IOException;
import java.util.List;

import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.NORMAL;
import static ru.m210projects.LSP.Animate.initanimations;
import static ru.m210projects.LSP.Factory.LSPMenuHandler.*;
import static ru.m210projects.LSP.GdxResources.appdef;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.LoadSave.FindSaves;
import static ru.m210projects.LSP.Quotes.InitQuotes;
import static ru.m210projects.LSP.Sounds.sndInit;
import static ru.m210projects.LSP.Sounds.stopallsounds;

public class Main extends BuildGame {

    public static Main game;
    public static LSPEngine engine;
    public static BoardService boardService;
    public static Config cfg;
    public static LoadingScreen gLoadingScreen;
    public static GameScreen gGameScreen;
    public static MovieScreen gMovieScreen;
    public static IntroScreen gIntroScreen;
    public static MenuScreen gMenuScreen;
    public static StatisticScreen gStatisticScreen;
    public static DemoScreen gDemoScreen;
    public static LSPDatFile gMapGroup;
    public static LSPDatFile gResGroup;
    public LSPMenuHandler menu;
    public final Runnable toMenu = new Runnable() {
        @Override
        public void run() {
            stopallsounds();
            gDemoScreen.onStopRecord();

            if (numplayers > 1 || gDemoScreen.demofiles.isEmpty() || cfg.gDemoSeq == 0 || !gDemoScreen.showDemo()) {
                changeScreen(gMenuScreen);
            }

            if (!menu.gShowMenu) {
                menu.mOpen(menu.mMenus[MAIN], -1);
            }
        }
    };

    public Main(List<String> args, GameConfig bcfg, String appname, String version, boolean isRelease) throws IOException {
        super(args, bcfg, appname, version, isRelease);
        game = this;
        cfg = (Config) bcfg;
    }

    @Override
    protected MessageScreen createMessage(String header, String text, MessageType type) {
        return new LSPMessageScreen(this, header, text, type);
    }

    @Override
    @NotNull
    public LSPRenderer getRenderer() {
        Renderer renderer = super.getRenderer();
        if (renderer instanceof LSPRenderer) {
            return (LSPRenderer) renderer;
        }
        return new LSPDummyRenderer();
    }

    @Override
    public GameProcessor createGameProcessor() {
        return new LSPInput(this);
    }

    @Override
    public BuildFactory getFactory() {
        return new LSPFactory(this);
    }

    @Override
    public boolean init() {
        boardService = engine.getBoardService();

        ConsoleInit();

        sndInit();

        for (int i = 0; i < MAXPLAYERS; i++) {
            gPlayer[i] = new PlayerStruct();
        }
        for (int i = 0; i < MAXSWINGDOORS; i++) {
            swingdoor[i] = new SwingDoor();
        }
        initanimations();

        InitQuotes();

        game.pNet.ResetTimers();
        Directory gameDir = cache.getGameDirectory();
        Console.out.println("Initializing def-scripts...");
        cache.loadGdxDef(baseDef, appdef, "lspgdx.dat");

        if (pCfg.isAutoloadFolder()) {
            Console.out.println("Initializing autoload folder");
            for (Entry file : gameDir.getDirectory(gameDir.getEntry("autoload"))) {
                switch (file.getExtension()) {
                    case "PK3":
                    case "ZIP": {
                        Group group = cache.newGroup(file);
                        Entry def = group.getEntry(appdef);
                        if (def.exists()) {
                            cache.addGroup(group, NORMAL); // HIGH?
                            baseDef.loadScript(file.getName(), def);
                        }
                    }
                    break;
                    case "DEF":
                        baseDef.loadScript(file);
                        break;
                }
            }
        }

        FileEntry filgdx = gameDir.getEntry(appdef);
        if (filgdx.exists()) {
            baseDef.loadScript(filgdx);
        }
        this.setDefs(baseDef);

        FindSaves(getUserDirectory());

        menu.mMenus[LOADGAME] = new LSPMenuLoad(this);
        menu.mMenus[SAVEGAME] = new LSPMenuSave(this);
        menu.mMenus[AUDIOSET] = new LSPMenuAudio(this);
        menu.mMenus[OPTIONS] = new MenuOptions(this);
        menu.mMenus[QUIT] = new MenuQuit(this);

        menu.mMenus[GAME] = new GameMenu(this);
        menu.mMenus[MAIN] = new MainMenu(this);
        menu.mMenus[ADVERTISING] = new AdvertisingMenu();
        menu.mMenus[LASTSAVE] = new MenuLastLoad(this);
        menu.mMenus[CORRUPTLOAD] = new MenuCorruptGame(this);

        gLoadingScreen = new LoadingScreen(this);
        gGameScreen = new GameScreen(this);
        gMovieScreen = new MovieScreen(this);
        gIntroScreen = new IntroScreen(this);
        gMenuScreen = new MenuScreen(this);
        gStatisticScreen = new StatisticScreen(this);
        gDemoScreen = new DemoScreen(this);
        gDemoScreen.demoscan(gameDir);

        System.gc();
        MemLog.log("create");

        return true;
    }

    @Override
    public void onDropEntry(FileEntry entry) {
        if (entry.isExtension("dmo")) {
            Console.out.println("Start dropped demo: " + entry.getName());
            gDemoScreen.showDemo(entry);
        }
    }


    private void ConsoleInit() {
        Console.out.println("Initializing on-screen display system");
//		Console.out.getPrompt().setParameters(0, 0, 0, 0, 0, 0);
        Console.out.getPrompt().setVersion(getTitle(), OsdColor.YELLOW, 10);

        Console.out.registerCommand(new OsdCallback("noclip", "noclip", argv -> {
            if (isCurrentScreen(gGameScreen)) {
                gPlayer[myconnectindex].noclip = !gPlayer[myconnectindex].noclip;
                Console.out.println("Noclip: " + (gPlayer[myconnectindex].noclip ? "ON" : "OFF"));
            } else {
                Console.out.println("noclip: not in a game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));

        Console.out.registerCommand(new OsdCallback("god", "god", argv -> {
            if (isCurrentScreen(gGameScreen)) {
                if (gPlayer[myconnectindex].nPlayerStatus != 5) {
                    gPlayer[myconnectindex].nPlayerStatus = 5;
                } else {
                    gPlayer[myconnectindex].nPlayerStatus = 0;
                }
                Console.out.println("God mode: " + (gPlayer[myconnectindex].nPlayerStatus == 5 ? "ON" : "OFF"));
            } else {
                Console.out.println("god: not in a game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));

        Console.out.registerCommand(new OsdCallback("changemap", "changemap: map number", argv -> {
            if (isCurrentScreen(gGameScreen)) {
                if (argv.length != 1) {
                    Console.out.println("changemap: map number");
                    return CommandResponse.SILENT_RESPONSE;
                }

                try {
                    int num = Integer.parseInt(argv[0]);
                    gGameScreen.changemap(num);
                } catch (Exception e) {
                    Console.out.println("type out of range");
                }
            } else {
                Console.out.println("changemap: not in a game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));


//		Console.out.registerCommand(new OsdCallback("savemap", "savemap: map name", new OsdCallback.OsdRunnable() {
//			@Override
//			public CommandResponse execute(String[] argv) {
//				if (isCurrentScreen(gGameScreen)) {
//					if (Console.osd_argc != 2) {
//						Console.out.println("savemap: map name with .map");
//						return;
//					}
//
//					try {
//						String name = osd_argv[1];
//						ru.m210projects.Build.FileHandle.FileResource fil = BuildGdx.compat.open(name, ru.m210projects.Build.FileHandle.Compat.Path.Game, ru.m210projects.Build.FileHandle.FileResource.Mode.Write);
//						PlayerStruct pos = gPlayer[myconnectindex];
//						engine.saveboard(fil, pos.x, pos.y, pos.z, (int)pos.ang, pos.sectnum);
//						fil.close();
//						Console.out.println("Map saved to file " + name);
//					} catch(Exception e) { Console.out.println("type out of range"); }
//				} else {
//					Console.out.println("savemap: not in a game");
//				}
//			}
//		}));
    }

    @Override
    public void show() {
        gDemoScreen.onStopRecord();

        engine.getTileManager().setTilesPath(0);
        game.changeScreen(gIntroScreen.setSkipping(toMenu));
    }

    @Override
    public DefaultPrecacheScreen getPrecacheScreen(Runnable readyCallback, PrecacheListener listener) {
        return new PrecacheScreen(readyCallback, listener);
    }


    @Override
    public LogSender getLogSender() {
        return new LogSender(this) {
            @Override
            public byte[] reportData() {
                String report = "Map: " + mapnum;
                report += "\r\n";
                report += "Skill " + nDifficult;
                report += "\r\n";

                if (gPlayer[myconnectindex] != null) {
                    report += "PlayerX " + gPlayer[myconnectindex].x;
                    report += "\r\n";
                    report += "PlayerY " + gPlayer[myconnectindex].y;
                    report += "\r\n";
                    report += "PlayerZ " + gPlayer[myconnectindex].z;
                    report += "\r\n";
                    report += "PlayerAng " + gPlayer[myconnectindex].ang;
                    report += "\r\n";
                    report += "PlayerSect " + gPlayer[myconnectindex].sectnum;
                    report += "\r\n";
                }

                return report.getBytes();
            }
        };
    }

    @Override
    public void dispose() {
        gDemoScreen.onStopRecord();
        super.dispose();
    }

    public enum UserFlag {
        None, UserMap
    }
}
