// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import com.badlogic.gdx.Input.Keys;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.settings.*;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;

public class Config extends GameConfig {

    public static final int[] defclassickeys = {
            Keys.UP, // GameKeys.Move_Forward,
            Keys.DOWN, // GameKeys.Move_Backward,
            Keys.LEFT, // GameKeys.Turn_Left,
            Keys.RIGHT, // GameKeys.Turn_Right,
            Keys.BACKSPACE, // GameKeys.Turn_Around,
            Keys.ALT_LEFT, // GameKeys.Strafe,
            Keys.COMMA, // GameKeys.Strafe_Left,
            Keys.PERIOD, // GameKeys.Strafe_Right,
            Keys.A, // GameKeys.Jump,
            Keys.Z, // GameKeys.Crouch,
            Keys.SHIFT_LEFT, //GameKeys.Run
            Keys.SPACE, // GameKeys.Use
            Keys.CONTROL_LEFT,// GameKeys.Weapon_Fire,
            Keys.APOSTROPHE, //GameKeys.Next_Weapon,
            Keys.SEMICOLON, //GameKeys.Previous_Weapon,
            Keys.PAGE_UP, //GameKeys.Look_Up,
            Keys.PAGE_DOWN, //GameKeys.Look_Down,
            Keys.TAB, //GameKeys.Map_Toggle,
            Keys.EQUALS, //GameKeys.Enlarge_Screen,
            Keys.MINUS, //GameKeys.Shrink_Screen,
            0, // Send_Message
            Keys.U, //GameKeys.Mouse_Aiming,
            Keys.ESCAPE,    // GameKeys.Menu_Toggle,
            Keys.GRAVE,        //Show_Console

            Keys.NUM_1, //LSPKeys.Weapon_1
            Keys.NUM_2, //LSPKeys.Weapon_2
            Keys.NUM_3, //LSPKeys.Weapon_3
            0, //LSPKeys.Last_Used_Weapon,
            Keys.F, //LSPKeys.Map_Follow_Mode,
            Keys.I, //LSPKeys.Toggle_Crosshair,
            Keys.CAPS_LOCK,        //AutoRun
            Keys.F9, //LSPKeys.Quickload,
            Keys.F6, //LSPKeys.Quicksave,
            Keys.F2, //LSPKeys.Show_Savemenu,
            Keys.F3, //LSPKeys.Show_Loadmenu,
            Keys.F4, //LSPKeys.Show_Sounds,
            Keys.F5, //LSPKeys.Show_Options,
            Keys.F10, //LSPKeys.Quit,
            Keys.F11, //LSPKeys.Gamma,
            0, //LSPKeys.Crouch_toggle,
            Keys.F12, //LSPKeys.Make_Screenshot,
    };
    public static final int[] defkeys = {
            Keys.W, // GameKeys.Move_Forward,
            Keys.S, // GameKeys.Move_Backward,
            Keys.LEFT, // GameKeys.Turn_Left,
            Keys.RIGHT, // GameKeys.Turn_Right,
            Keys.BACKSPACE, // GameKeys.Turn_Around,
            Keys.ALT_LEFT, // GameKeys.Strafe,
            Keys.A, // GameKeys.Strafe_Left,
            Keys.D, // GameKeys.Strafe_Right,
            Keys.SPACE, // GameKeys.Jump,
            Keys.CONTROL_LEFT, // GameKeys.Crouch,
            Keys.SHIFT_LEFT, //GameKeys.Run
            Keys.E, // GameKeys.Use
            0,// GameKeys.Weapon_Fire,
            Keys.APOSTROPHE, //GameKeys.Next_Weapon,
            Keys.SEMICOLON, //GameKeys.Previous_Weapon,
            Keys.PAGE_UP, //GameKeys.Look_Up,
            Keys.PAGE_DOWN, //GameKeys.Look_Down,
            Keys.TAB, //GameKeys.Map_Toggle,
            Keys.EQUALS, //GameKeys.Enlarge_Screen,
            Keys.MINUS, //GameKeys.Shrink_Screen,
            0, // Send_Message
            Keys.U, //GameKeys.Mouse_Aiming,
            Keys.ESCAPE,    // GameKeys.Menu_Toggle,
            Keys.GRAVE,        //Show_Console

            Keys.NUM_1, //LSPKeys.Weapon_1
            Keys.NUM_2, //LSPKeys.Weapon_2
            Keys.NUM_3, //LSPKeys.Weapon_3
            Keys.Q, //LSPKeys.Last_Used_Weapon,
            Keys.F, //LSPKeys.Map_Follow_Mode,
            Keys.I, //LSPKeys.Toggle_Crosshair,
            Keys.CAPS_LOCK,        //AutoRun
            Keys.F9, //LSPKeys.Quickload,
            Keys.F6, //LSPKeys.Quicksave,
            Keys.F2, //LSPKeys.Show_Savemenu,
            Keys.F3, //LSPKeys.Show_Loadmenu,
            Keys.F4, //LSPKeys.Show_Sounds,
            Keys.F5, //LSPKeys.Show_Options,
            Keys.F10, //LSPKeys.Quit,
            Keys.F11, //LSPKeys.Gamma,
            0, //LSPKeys.Crouch_toggle,
            Keys.F12 //LSPKeys.Make_Screenshot,
    };

    public boolean gAutoRun = false;
    public int gOverlayMap = 2;
    public boolean gShowMessages = true;
    public boolean gCrosshair = true;
    public int gCrossSize = 65536;
    public int gShowStat = 0;
    public int gHUDSize = 65536;
    public int showMapInfo = 1;
    public boolean bHeadBob = true;
    public boolean bShowExit = false;
    public boolean bOriginal = false;
    public int gDemoSeq = 1;

    public Config(Path path) {
        super(path);
    }

    public GameKey[] getKeyMap() {
        return new GameKey[]{
                GameKeys.Move_Forward,
                GameKeys.Move_Backward,
                GameKeys.Turn_Left,
                GameKeys.Turn_Right,
                GameKeys.Turn_Around,
                GameKeys.Strafe,
                GameKeys.Strafe_Left,
                GameKeys.Strafe_Right,
                GameKeys.Jump,
                GameKeys.Crouch,
                LSPKeys.Crouch_toggle,
                GameKeys.Open,
                GameKeys.Weapon_Fire,
                GameKeys.Menu_Toggle,
                LSPKeys.Weapon_1,
                LSPKeys.Weapon_2,
                LSPKeys.Weapon_3,
                GameKeys.Previous_Weapon,
                GameKeys.Next_Weapon,
                LSPKeys.Last_Used_Weapon,
                GameKeys.Look_Up,
                GameKeys.Look_Down,
                GameKeys.Mouse_Aiming,
                GameKeys.Run,
                LSPKeys.AutoRun,
                GameKeys.Map_Toggle,
                GameKeys.Enlarge_Screen,
                GameKeys.Shrink_Screen,
                GameKeys.Show_Console,

                LSPKeys.Map_Follow_Mode,
                LSPKeys.Toggle_Crosshair,
                LSPKeys.Quickload,
                LSPKeys.Quicksave,
                LSPKeys.Show_Savemenu,
                LSPKeys.Show_Loadmenu,
                LSPKeys.Show_Sounds,
                LSPKeys.Show_Options,
                LSPKeys.Quit,
                LSPKeys.Gamma,
                LSPKeys.Make_Screenshot,
        };

    }

    @Override
    protected InputContext createDefaultInputContext() {
        return new InputContext(getKeyMap(), defkeys, defclassickeys);
    }

    @Override
    protected ConfigContext createDefaultGameContext() {
        return new ConfigContext() {
            @Override
            public void load(Properties prop) {
                if (prop.setContext("Options")) {
                    gAutoRun = prop.getBooleanValue("AutoRun", gAutoRun);
                    gOverlayMap = prop.getIntValue("OverlayMap", gOverlayMap);
                    gShowMessages = prop.getBooleanValue("ShowMessages", gShowMessages);
                    gCrosshair = prop.getBooleanValue("Crosshair", gCrosshair);
                    gCrossSize = Math.max(16384, prop.getIntValue("CrossSize", gCrossSize));
                    gShowStat = prop.getIntValue("ShowStat", gShowStat);
                    gHUDSize = Math.max(16384, prop.getIntValue("HUDSize", gHUDSize));
                    showMapInfo = prop.getIntValue("showMapInfo", showMapInfo);
                    bHeadBob = prop.getBooleanValue("HeadBob", bHeadBob);
                    bOriginal = prop.getBooleanValue("ProjectileSpam", bOriginal);
                    bShowExit = prop.getBooleanValue("ShowExits", bShowExit);
                    gDemoSeq = prop.getIntValue("DemoSequence", gDemoSeq);
                }
            }

            @Override
            public void save(OutputStream os) throws IOException {
                putString(os, "[Options]\r\n");
                //Options

                putBoolean(os, "AutoRun", gAutoRun);
                putInteger(os, "OverlayMap", gOverlayMap);
                putBoolean(os, "ShowMessages", gShowMessages);
                putBoolean(os, "Crosshair", gCrosshair);
                putInteger(os, "CrossSize", gCrossSize);
                putInteger(os, "ShowStat", gShowStat);
                putInteger(os, "HUDSize", gHUDSize);
                putInteger(os, "showMapInfo", showMapInfo);
                putBoolean(os, "HeadBob", bHeadBob);
                putBoolean(os, "ProjectileSpam", bOriginal);
                putBoolean(os, "ShowExits", bShowExit);
                putInteger(os, "DemoSequence", gDemoSeq);
            }
        };
    }

    public enum LSPKeys implements GameKey {
        Weapon_1,
        Weapon_2,
        Weapon_3,
        Last_Used_Weapon,
        Map_Follow_Mode,
        Toggle_Crosshair,
        AutoRun,
        Quickload,
        Quicksave,
        Show_Savemenu,
        Show_Loadmenu,
        Show_Sounds,
        Show_Options,
        Quit,
        Gamma,
        Crouch_toggle,
        Make_Screenshot;

        public int getNum() {
            return GameKeys.values().length + ordinal();
        }

        public String getName() {
            return name();
        }

    }

}
