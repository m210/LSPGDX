// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.LSP.Factory.LSPRenderer;
import ru.m210projects.LSP.Types.CompareService.CompareService;

import static ru.m210projects.Build.Engine.neartag;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.LSP.Animate.*;
import static ru.m210projects.LSP.Factory.LSPMenuHandler.ADVERTISING;
import static ru.m210projects.LSP.Factory.LSPMenuHandler.LOADGAME;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.*;
import static ru.m210projects.LSP.Screens.DemoScreen.isDemoPlaying;
import static ru.m210projects.LSP.Sounds.*;
import static ru.m210projects.LSP.Sprites.operatesprite;
import static ru.m210projects.LSP.Types.ANIMATION.*;

public class Sectors {

    private static final short[] opwallfind = new short[2];

    public static void tagcode() {
        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
            Sector plrSec = boardService.getSector(gPlayer[p].sectnum);
            if (plrSec == null) {
                continue;
            }

            if (mapnum == 0) {
                if (plrSec.getLotag() == 77) {
                    bActiveTrain = true;
                    bTrainSoundSwitch = true;
                }

                if (plrSec.getLotag() == 78) {
                    Wall w = boardService.getWall(nTrainWall);
                    if (w != null) {
                        w.setCstat(17);
                        bActiveTrain = false;
                        bTrainSoundSwitch = false;
                    }
                }

                if (gPlayer[p].osectnum != gPlayer[p].sectnum) {
                    if (plrSec.getLotag() == 81) { // advertising
                        game.menu.mOpen(game.menu.mMenus[ADVERTISING], -1);
                    }

                    if (plrSec.getLotag() == 82) { // load game
                        game.menu.mOpen(game.menu.mMenus[LOADGAME], -1);
                    }
                }
            }

            if (plrSec.getLotag() == 1 || (plrSec.getLotag() == 2 && gPlayer[p].osectnum != gPlayer[p].sectnum)) {
                for (int i = 0; i < boardService.getSectorCount(); i++) {
                    Sector sec = boardService.getSector(i);
                    if (sec != null && sec.getHitag() == plrSec.getHitag() && sec.getLotag() != 1) {
                        operatesector(i);
                    }
                }

                if (plrSec.getLotag() == 1) {
                    plrSec.setLotag(0);
                }

                Sprite tagSprite = boardService.getSprite(neartag.tagsprite);
                if (tagSprite != null) {
                    ListNode<Sprite> node = boardService.getStatNode(0);
                    while (node != null) {
                        ListNode<Sprite> next = node.getNext();
                        if (node.get().getHitag() == tagSprite.getHitag()) {
                            operatesprite(node.getIndex());
                        }
                        node = next;
                    }
                }

            } else {
                switch (plrSec.getLotag()) { // new level
                    case 99:
                        nNextMap = 1;
                        break;
                    case 98:
                        nNextMap = 2;
                        break;
                    case 97:
                        nNextMap = 3;
                        break;
                }

                if (nNextMap != 0 && !isDemoPlaying()) {
                    gDemoScreen.onStopRecord();

                    startmusic(1);
                    stopallsounds();
                    game.changeScreen(gStatisticScreen.setSkipping(() -> {
                        LSPRenderer renderer = game.getRenderer();
                        renderer.setVisibility(15);
                        if (gGameScreen.NextMap()) {
                            if (gMovieScreen.init("LFART000.DAT")) {
                                gMovieScreen.setCallback(() -> game.show());
                                game.changeScreen(gMovieScreen.escSkipping(true));
                            } else {
                                game.show();
                            }
                        }
                    }));
                }
            }
            gPlayer[p].osectnum = gPlayer[p].sectnum;
        }

        for (int i = 0; i < warpsector2cnt; i++) {
            int dasector = warpsector2list[i];
            int j = (lockclock & 127) >> 2;
            if (j >= 16) {
                j = 31 - j;
            }

            Sector sec = boardService.getSector(dasector);
            if  (sec != null) {
                sec.setCeilingshade((byte) j);
                sec.setFloorshade((byte) j);
                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    wal.setShade((byte) j);
                }
            }
        }

        for (int i = 0; i < warpsectorcnt; i++) {
            int dasector = warpsectorlist[i];
            int j = (lockclock & 127) >> 2;
            if (j >= 16) {
                j = 31 - j;
            }

            Sector sec = boardService.getSector(dasector);
            if  (sec != null) {
                sec.setCeilingshade((byte) j);
                sec.setFloorshade((byte) j);
                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    wal.setShade((byte) j);
                }
            }
        }

        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
            Sector plrSec = boardService.getSector(gPlayer[p].sectnum);
            if (plrSec != null && plrSec.getLotag() == 10) // warp sector
            {
                if (gPlayer[p].word_586FC == 0) {
//					sndPlaySound("warp.wav", sub_1D021(gPlayer[p].x, gPlayer[p].y)); XXX

                    for (int i = 0; i < warpsectorcnt; i++) {
                        if (warpsectorlist[i] == gPlayer[p].sectnum) {
                            if (i == warpsectorcnt - 1) {
                                gPlayer[p].sectnum = warpsectorlist[0];
                            } else {
                                gPlayer[p].sectnum = warpsectorlist[i + 1];
                            }
                            plrSec = boardService.getSector(gPlayer[p].sectnum);
                            break;
                        }
                    }

                    if (plrSec == null) {
                        continue;
                    }

                    // find center of sector
                    int startwall = plrSec.getWallptr();
                    int endwall = (startwall + plrSec.getWallnum() - 1);
                    int dax = 0;
                    int day = 0;
                    Wall iwal = null;
                    for (int s = startwall; s <= endwall; s++) {
                        Wall wal = boardService.getWall(s);
                        if (wal != null) {
                            dax += wal.getX();
                            day += wal.getY();
                            if (wal.getNextsector() >= 0) {
                                iwal = wal;
                            }
                        }
                    }

                    if (iwal != null) {
                        gPlayer[p].x = dax / (endwall - startwall + 1);
                        gPlayer[p].y = day / (endwall - startwall + 1);
                        gPlayer[p].sectnum = engine.updatesector(gPlayer[p].x, gPlayer[p].y, gPlayer[p].sectnum);
                        plrSec = boardService.getSector(gPlayer[p].sectnum);
                        if (plrSec != null) {
                            gPlayer[p].z = plrSec.getFloorz() - 0x2000;

                            dax = (iwal.getWall2().getX() + iwal.getX()) >> 1;
                            day = (iwal.getWall2().getY() + iwal.getY()) >> 1;

                            gPlayer[p].ang = EngineUtils.getAngle(dax - gPlayer[p].x, day - gPlayer[p].y);

                            engine.setsprite(gPlayer[p].nSprite, gPlayer[p].x, gPlayer[p].y, gPlayer[p].z + 0x2000);
                            Sprite plrSpr = boardService.getSprite(gPlayer[p].nSprite);
                            if (plrSpr != null) {
                                plrSpr.setAng((short) gPlayer[p].ang);
                            }
                            gPlayer[0].word_586FC = 1;
                        }
                    }
                }
            } else {
                gPlayer[p].word_586FC = 0;
            }
        }

        for (int i = 0; i < xpanningsectorcnt; i++) {
            Sector sec = boardService.getSector(xpanningsectorlist[i]);
            if  (sec != null) {
                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    wal.setXpanning((byte) ((lockclock >> 2) & 255));
                }
            }
        }

        for (int i = 0; i < ypanningwallcnt; i++) {
            Wall w = boardService.getWall(ypanningwalllist[i]);
            if (w != null) {
                w.setYpanning((byte) ~(lockclock & 255));
            }
        }

        for (int i = 0; i < floorpanningcnt; i++) {
            Sector sec =  boardService.getSector(floorpanninglist[i]);
            if (sec != null) {
                sec.setFloorxpanning((byte) ((lockclock >> 2) & 255));
                sec.setFloorypanning((byte) ((lockclock >> 2) & 255));
            }
        }

        for (int i = 0; i < dragsectorcnt; i++) {
           final int dasector = dragsectorlist[i];
           Sector sec = boardService.getSector(dasector);
           if (sec == null) {
               continue;
           }

           int startwall = sec.getWallptr();
           Wall startWall = boardService.getWall(startwall);
           if (startWall == null) {
               continue;
           }

            if (startWall.getX() + dragxdir[i] < dragx1[i]) {
                dragxdir[i] = 16;
            }
            if (startWall.getY() + dragydir[i] < dragy1[i]) {
                dragydir[i] = 16;
            }
            if (startWall.getX() + dragxdir[i] > dragx2[i]) {
                dragxdir[i] = -16;
            }
            if (startWall.getY() + dragydir[i] > dragy2[i]) {
                dragydir[i] = -16;
            }

            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                engine.dragpoint(wn.getIndex(), wal.getX() + dragxdir[i], wal.getY() + dragydir[i]);
            }

            int j = sec.getFloorz();
            game.pInt.setfloorinterpolate(dasector, sec);
            sec.setFloorz(dragfloorz[i] + (EngineUtils.sin((lockclock << 4) & 2047) >> 3));
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                if (gPlayer[p].sectnum == dasector) {
                    gPlayer[p].x += dragxdir[i];
                    gPlayer[p].y += dragydir[i];
                    gPlayer[p].z += (sec.getFloorz() - j);
                    engine.setsprite(gPlayer[p].nSprite, gPlayer[p].x, gPlayer[p].y, gPlayer[p].z + 0x2000);

                    Sprite plrSpr = boardService.getSprite(gPlayer[p].nSprite);
                    if (plrSpr != null) {
                        plrSpr.setAng((short) gPlayer[p].ang);
                    }
                }
            }
        }

        for (int i = 0; i < swingcnt; i++) {
            if (swingdoor[i].anginc != 0) {
                int oldang = (short) swingdoor[i].ang;
                for (int j = 0; j < (TICSPERFRAME << 2); j++) {
                    swingdoor[i].ang = ((swingdoor[i].ang + 2048 + swingdoor[i].anginc) & 2047);
                    if (swingdoor[i].ang == swingdoor[i].angclosed) {
                        playsound(21);
                        swingdoor[i].anginc = 0;
                    }
                    if (swingdoor[i].ang == swingdoor[i].angopen) {
                        swingdoor[i].anginc = 0;
                    }
                }
                for (int k = 1; k <= 3; k++) {
                    Point out = EngineUtils.rotatepoint(swingdoor[i].x[0], swingdoor[i].y[0], swingdoor[i].x[k],
                            swingdoor[i].y[k], (short) swingdoor[i].ang);

                    engine.dragpoint((short) swingdoor[i].wall[k], out.getX(), out.getY());
                }

                if (swingdoor[i].anginc != 0) {
                    for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                        Sector plrSec = boardService.getSector(gPlayer[p].sectnum);
                        if (plrSec != null && ((gPlayer[p].sectnum == swingdoor[i].sector))) {
                            int cnt = 256;
                            int good;
                            do {
                                good = 1;

                                // swingangopendir is -1 if forwards, 1 is backwards
                                int l = (swingdoor[i].angopendir > 0) ? 1 : 0;
                                for (int k = l + 3; k >= l; k--) {
                                    if (engine.clipinsidebox(gPlayer[p].x + 64, gPlayer[p].y + 64,
                                            (short) swingdoor[i].wall[k], 128) != 0) {
                                        good = 0;
                                        break;
                                    }
                                }

                                if (good == 0) {
                                    if (cnt == 256) {
                                        swingdoor[i].anginc = (short) -swingdoor[i].anginc;
                                        swingdoor[i].ang = oldang;
                                    } else {
                                        swingdoor[i].ang = (short) ((swingdoor[i].ang + 2048 - swingdoor[i].anginc)
                                                & 2047);
                                    }
                                    for (int k = 1; k <= 3; k++) {
                                        Point out = EngineUtils.rotatepoint(swingdoor[i].x[0], swingdoor[i].y[0],
                                                swingdoor[i].x[k], swingdoor[i].y[k], (short) swingdoor[i].ang);

                                        engine.dragpoint((short) swingdoor[i].wall[k], out.getX(), out.getY());
                                    }
                                    if (swingdoor[i].ang == swingdoor[i].angclosed) {
                                        swingdoor[i].anginc = 0;
                                        break;
                                    }
                                    if (swingdoor[i].ang == swingdoor[i].angopen) {
                                        swingdoor[i].anginc = 0;
                                        break;
                                    }
                                    cnt--;
                                }
                            } while ((good == 0) && (cnt > 0));
                        }
                    }
                }
            }
        }

        for (int i = 0; i < revolvecnt; i++) {
            Sector sec = boardService.getSector(revolvesector[i]);
            if (sec == null) {
                continue;
            }

            int startwall = sec.getWallptr();
            int endwall = (startwall + sec.getWallnum() - 1);
            revolveang[i] = (short) ((revolveang[i] + 2048 - (TICSPERFRAME << 2)) & 2047);
            for (int w = startwall; w <= endwall; w++) {
                Point out = EngineUtils.rotatepoint(revolvepivotx[i], revolvepivoty[i], revolvex[i][w - startwall],
                        revolvey[i][w - startwall], revolveang[i]);
                int dax = out.getX();
                int day = out.getY();
                engine.dragpoint(w, dax, day);
            }
        }

        if (bActiveTrain) {
            for (int i = 0; i < subwaytrackcnt; i++) {
                final int dasector = subwaytracksector[i][0];
                Sector sec = boardService.getSector(dasector);
                if (sec == null) {
                    continue;
                }

                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    game.pInt.setwallinterpolate(wn.getIndex(), wal);
                    if (wal.getX() > subwaytrackx1[i]) {
                        if (wal.getY() > subwaytracky1[i]) {
                            if (wal.getX() < subwaytrackx2[i]) {
                                if (wal.getY() < subwaytracky2[i]) {
                                    wal.setX(wal.getX() + (subwayvel[i] & 0xfffffffc));
                                }
                            }
                        }
                    }
                }

                for (int j = 1; j < subwaynumsectors[i]; j++) {
                    Sector sec2 = boardService.getSector(subwaytracksector[i][j]);
                    if (sec2 == null) {
                        continue;
                    }

                    for (ListNode<Wall> wn = sec2.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        game.pInt.setwallinterpolate(wn.getIndex(), wal);
                        wal.setX(wal.getX() + (subwayvel[i] & 0xfffffffc));
                    }

                    ListNode<Sprite> node = boardService.getSectNode(subwaytracksector[i][j]);
                    while (node != null) {
                        ListNode<Sprite> next = node.getNext();
                        Sprite spr = node.get();
                        game.pInt.setsprinterpolate(node.getIndex(), spr);
                        spr.setX(spr.getX() + (subwayvel[i] & 0xfffffffc));
                        node = next;
                    }
                }

                for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                    Sector plrSec = boardService.getSector(gPlayer[p].sectnum);
                    Sector sec2 = boardService.getSector(subwaytracksector[i][0]);

                    if (sec2 != null && plrSec != null && gPlayer[p].sectnum != subwaytracksector[i][0]) {
                        if (plrSec.getFloorz() != sec2.getFloorz()) {
                            if (gPlayer[p].x > subwaytrackx1[i]) {
                                if (gPlayer[p].y > subwaytracky1[i]) {
                                    if (gPlayer[p].x < subwaytrackx2[i]) {
                                        if (gPlayer[p].y < subwaytracky2[i]) {
                                            gPlayer[p].x += (subwayvel[i] & 0xfffffffc);


//											if ( engine.getTotalClock() - dword_5AF58 > 200 )
//								            {
//								              playsound(97);
//								              dword_5AF58 = engine.getTotalClock();
//								            }

                                            // Update sprite representation of player
                                            engine.setsprite(gPlayer[p].nSprite, gPlayer[p].x, gPlayer[p].y,
                                                    gPlayer[p].z + 0x2000);

                                            Sprite plrSpr = boardService.getSprite(gPlayer[p].nSprite);
                                            if (plrSpr != null) {
                                                plrSpr.setAng((short) gPlayer[p].ang);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                subwayx[i] += (subwayvel[i] & 0xfffffffc);
                if (subwaygoalstop[i] >= 8 || subwaygoalstop[i] < 0) {
                    continue;
                }

                int k = subwaystop[i][subwaygoalstop[i]] - subwayx[i];
                if (k > 0) {
                    if (k > 2048) {
                        if (subwayvel[i] < 128) {
                            subwayvel[i]++;
                        }
                    } else {
                        subwayvel[i] = (k >> 4) + 1;
                    }
                } else if (k < 0) {
                    if (k < -2048) {
                        if (subwayvel[i] > -128) {
                            subwayvel[i]--;
                        }
                    } else {
                        subwayvel[i] = ((k >> 4) - 1);
                    }
                }

                if (bTrainSoundSwitch && (k <= 320 && k >= 280 || k >= -320 && k <= -280)) {
                    if (gPlayer[0].x < 0 && subwaygoalstop[i] == 0 || gPlayer[0].x > 0 && subwaygoalstop[i] == 1) {
                        playsound(96);
                    }
                    bTrainSoundSwitch = false;
                } else if (!bTrainSoundSwitch) {
                    if (k <= 44 && k >= -44 && subwaypausetime[i] < 490) {
                        if (gPlayer[0].x < 0 && subwaygoalstop[i] == 0 || gPlayer[0].x > 0 && subwaygoalstop[i] == 1) {
                            playsound(95);
                        }
                        bTrainSoundSwitch = true;
                    }
                }

                if (((subwayvel[i] >> 2) == 0) && (Math.abs(k) < 2048)) {
                    if (subwaypausetime[i] == 2400) {
                        for (int j = 1; j < subwaynumsectors[i]; j++) // Open all subway doors
                        {
                            int dasector2 = subwaytracksector[i][j];
                            Sector sec2 = boardService.getSector(dasector2);
                            if (sec2 == null) {
                                continue;
                            }

                            if (sec2.getLotag() == 17) {
                                sec2.setLotag(16);
                                playsound(98);
                                operatesector(dasector2);
                                sec2.setLotag(17);
                            }
                        }
                    }

                    if ((subwaypausetime[i] >= 480) && (subwaypausetime[i] - TICSPERFRAME < 480)) {
                        for (int j = 1; j < subwaynumsectors[i]; j++) // Close all subway doors
                        {
                            int dasector2 = subwaytracksector[i][j];
                            Sector sec2 = boardService.getSector(dasector2);
                            if (sec2 == null) {
                                continue;
                            }

                            if (sec2.getLotag() == 17) {
                                sec2.setLotag(16);
                                playsound(99);
                                operatesector(dasector2);
                                sec2.setLotag(17);
                            }
                        }
                    }

                    subwaypausetime[i] -= TICSPERFRAME;
                    if (subwaypausetime[i] < 0) {
                        subwaypausetime[i] = 2400;
                        if (subwayvel[i] < 0 && --subwaygoalstop[i] < 0) {
                            subwaygoalstop[i] = 1;
                        }
                        if (subwayvel[i] > 0) {
                            if (++subwaygoalstop[i] >= subwaystopcnt[i]) {
                                subwaygoalstop[i] = subwaystopcnt[i] - 2;
                            }
                        }
                    }
                }
            }
        }
    }

    public static void operatesector(final int dasector) { // Door code
        Sector sec = boardService.getSector(dasector);
        if (sec == null) {
            return;
        }

        int datag = sec.getLotag();
        final int startwall = sec.getWallptr();
        final int endwall = (startwall + sec.getWallnum() - 1);
        int centx = 0;
        int centy = 0;
        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall wal = wn.get();
            centx += wal.getX();
            centy += wal.getY();
        }
        centx /= (endwall - startwall + 1);
        centy /= (endwall - startwall + 1);

        if (datag == 6 || datag == 8) { // ceil door, split door
            int i = getanimationgoal(sec, CEILZ);
            if (i >= 0) {
                int goalz = sec.getFloorz();
                if (datag == 8) {
                    goalz = (sec.getFloorz() + sec.getCeilingz()) >> 1;
                }

                if (goalz == gAnimationData[i].goal) {
                    Sector s = boardService.getSector(engine.nextsectorneighborz(dasector, sec.getFloorz(), -1, -1));
                    if (s != null) {
                        gAnimationData[i].goal = s.getCeilingz();
                    }
                } else {
                    gAnimationData[i].goal = goalz;
                }
            } else {
                int s = engine.nextsectorneighborz(dasector, sec.getCeilingz(), -1, -1);
                int goalz = sec.getFloorz();
                Sector sec2 = boardService.getSector(s);
                if (sec.getCeilingz() == sec.getFloorz() && sec2 != null) {
                    goalz = sec2.getCeilingz();
                } else if (datag == 8) {
                    goalz = (sec.getFloorz() + sec.getCeilingz()) >> 1;
                }

                if (setanimation(dasector, goalz, 128, 0, CEILZ) >= 0) {
                    playsound(74);
                }
            }
        }

        if (datag == 7 || datag == 8) { // floor door, split door
            int i = getanimationgoal(sec, FLOORZ);
            if (i >= 0) {
                int goalz = sec.getCeilingz();
                if (datag == 8) {
                    goalz = (sec.getFloorz() + sec.getCeilingz()) >> 1;
                }

                if (gAnimationData[i].goal == goalz) {
                    Sector sec2 = boardService.getSector(engine.nextsectorneighborz(dasector, sec.getCeilingz(), 1,
                            1));
                    if (sec2 != null) {
                        gAnimationData[i].goal = sec2.getFloorz();
                    }
                } else {
                    gAnimationData[i].goal = goalz;
                }
            } else {
                int s = engine.nextsectorneighborz(dasector, sec.getCeilingz(), 1, 1);
                Sector sec2 = boardService.getSector(s);
                int goalz = sec.getCeilingz();
                if (sec.getCeilingz() == sec.getFloorz() && sec2 != null) {
                    goalz = sec2.getFloorz();
                } else if (datag == 8) {
                    goalz = (sec.getFloorz() + sec.getCeilingz()) >> 1;
                }

                if (setanimation(dasector, goalz, 128, 0, FLOORZ) >= 0) {
                    playsound(76);
                }
            }
        }

        if (datag == 20 || datag == 21) { // elevators
            int i = getanimationgoal(sec, FLOORZ);
            if (i >= 0) {
                Sector sec2 = boardService.getSector(engine.nextsectorneighborz(dasector, sec.getCeilingz(), 1, 1));
                if (sec2 != null) {
                    int goalz = sec2.getFloorz();
                    if (goalz == gAnimationData[i].goal) {
                        Sector sec3 = boardService.getSector(engine.nextsectorneighborz(dasector, sec.getCeilingz(), 1, 1));
                        if (sec3 != null) {
                            gAnimationData[i].goal = sec3.getFloorz();
                        }
                    } else {
                        gAnimationData[i].goal = goalz;
                    }
                }
            } else {
                int sect = engine.nextsectorneighborz(dasector, sec.getFloorz(), 1, 1);
                if (sect == -1) {
                    if (datag == 20) {
                        sect = engine.nextsectorneighborz(dasector, sec.getFloorz(), -1, -1);
                    } else {
                        sect = engine.nextsectorneighborz(dasector, sec.getFloorz(), 1, -1);
                    }
                }

                Sector sec2 = boardService.getSector(sect);
                if (sec2 != null && setanimation(dasector, sec2.getFloorz(), 128, 0, FLOORZ) >= 0) {
                    playsound(78);
                }
            }
        }

        if (datag == 9) {
            opwallfind[0] = -1;
            opwallfind[1] = -1;
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                if ((wal.getX() == centx) || (wal.getY() == centy)) {
                    if (opwallfind[0] == -1) {
                        opwallfind[0] = (short) wn.getIndex();
                    } else {
                        opwallfind[1] = (short) wn.getIndex();
                    }
                }
            }

            for (int j = 0; j < 2; j++) {
                Wall w = boardService.getWall(opwallfind[j]);
                if (w == null) {
                    continue;
                }

                if ((w.getX() == centx) && (w.getY() == centy)) {
                    int i = opwallfind[j] - 1;
                    if (i < startwall) {
                        i = endwall;
                    }

                    Wall w2 = boardService.getWall(i);
                    if (w2 == null) {
                        continue;
                    }

                    int dax2 = ((w2.getX() + w.getWall2().getX()) >> 1) - w.getX();
                    int day2 = ((w2.getY() + w.getWall2().getY()) >> 1) - w.getY();
                    if (dax2 != 0) {
                        dax2 = w.getWall2().getWall2().getX();
                        dax2 -= w.getWall2().getX();
                        setanimation(opwallfind[j], w.getX() + dax2, 4, 0, WALLX);
                        setanimation(i, w2.getX() + dax2, 4, 0, WALLX);
                        setanimation(w.getPoint2(), w.getWall2().getX() + dax2, 4, 0,
                                WALLX);
                    } else if (day2 != 0) {
                        day2 = w.getWall2().getWall2().getY();
                        day2 -= w.getWall2().getY();
                        setanimation(opwallfind[j], w.getY() + day2, 4, 0, WALLY);
                        setanimation(i, w2.getY() + day2, 4, 0, WALLY);
                        setanimation(w.getPoint2(), w.getWall2().getY() + day2, 4, 0, WALLY);
                    }
                } else {
                    int i = opwallfind[j] - 1;
                    if (i < startwall) {
                        i = endwall;
                    }

                    Wall w2 = boardService.getWall(i);
                    if (w2 == null) {
                        continue;
                    }

                    int dax2 = ((w2.getX() + w.getWall2().getX()) >> 1) - w.getX();
                    int day2 = ((w2.getY() + w.getWall2().getY()) >> 1) - w.getY();
                    if (dax2 != 0) {
                        setanimation(opwallfind[j], centx, 4, 0, WALLX);
                        setanimation(i, centx + dax2, 4, 0, WALLX);
                        setanimation(w.getPoint2(), centx + dax2, 4, 0, WALLX);
                    } else if (day2 != 0) {
                        setanimation(opwallfind[j], centy, 4, 0, WALLY);
                        setanimation(i, centy + day2, 4, 0, WALLY);
                        setanimation(w.getPoint2(), centy + day2, 4, 0, WALLY);
                    }
                }
            }
            playsound(74);
        }

        // kens swinging door
        if (datag == 13) {
            for (int i = 0; i < swingcnt; i++) {
                if (swingdoor[i].sector == dasector) {
                    if (swingdoor[i].anginc == 0) {
                        if (swingdoor[i].ang == swingdoor[i].angclosed) {
                            swingdoor[i].anginc = swingdoor[i].angopendir;
                            playsound(22);
                        } else {
                            swingdoor[i].anginc = (short) -swingdoor[i].angopendir;
                        }
                    } else {
                        swingdoor[i].anginc = (short) -swingdoor[i].anginc;
                    }
                }
            }
        }

        // kens true sideways double-sliding door
        if (datag == 16) {
            // get 2 closest line segments to center (dax, day)
            opwallfind[0] = -1;
            opwallfind[1] = -1;
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                if (wal.getLotag() == 6) {
                    if (opwallfind[0] == -1) {
                        opwallfind[0] = (short) wn.getIndex();
                    } else {
                        opwallfind[1] = (short) wn.getIndex();
                    }
                }
            }

            for (int j = 0; j < 2; j++) {
                Wall w = boardService.getWall(opwallfind[j]);
                if (w == null) {
                    continue;
                }

                if ((((w.getX() + w.getWall2().getX()) >> 1) == centx)
                        && (((w.getY() + w.getWall2().getY()) >> 1) == centy)) { // door was
                    // closed.
                    // Find
                    // what
                    // direction
                    // door
                    // should
                    // open
                    int i = opwallfind[j] - 1;
                    if (i < startwall) {
                        i = endwall;
                    }

                    Wall w2 = boardService.getWall(i);
                    if (w2 == null) {
                        continue;
                    }

                    int dax2 = w2.getX() - w.getX();
                    int day2 = w2.getY() - w.getY();
                    if (dax2 != 0) {
                        dax2 = w.getWall2().getWall2().getWall2().getX();
                        dax2 -= w.getWall2().getWall2().getX();
                        setanimation(opwallfind[j], w.getX() + dax2, 4, 0, WALLX);
                        setanimation(i, w2.getX() + dax2, 4, 0, WALLX);
                        setanimation(w.getPoint2(), w.getWall2().getX() + dax2, 4, 0,
                                WALLX);
                        setanimation(w.getWall2().getPoint2(),
                                w.getWall2().getWall2().getX() + dax2, 4, 0, WALLX);
                    } else if (day2 != 0) {
                        day2 = w.getWall2().getWall2().getWall2().getY();
                        day2 -= w.getWall2().getWall2().getY();
                        setanimation(opwallfind[j], w.getY() + day2, 4, 0, WALLY);
                        setanimation(i, w2.getY() + day2, 4, 0, WALLY);
                        setanimation(w.getPoint2(), w.getWall2().getY() + day2, 4, 0,
                                WALLY);
                        setanimation(w.getWall2().getPoint2(),
                                w.getWall2().getWall2().getY() + day2, 4, 0, WALLY);
                    }
                } else { // door was not closed
                    int i = opwallfind[j] - 1;
                    if (i < startwall) {
                        i = endwall;
                    }

                    Wall w2 = boardService.getWall(i);
                    if (w2 == null) {
                        continue;
                    }

                    int dax2 = w2.getX() - w.getX();
                    int day2 = w2.getY() - w.getY();
                    if (dax2 != 0) {
                        setanimation(opwallfind[j], centx, 4, 0, WALLX);
                        setanimation(i, centx + dax2, 4, 0, WALLX);
                        setanimation(w.getPoint2(), centx, 4, 0, WALLX);
                        setanimation(w.getWall2().getPoint2(), centx + dax2, 4, 0, WALLX);
                    } else if (day2 != 0) {
                        setanimation(opwallfind[j], centy, 4, 0, WALLY);
                        setanimation(i, centy + day2, 4, 0, WALLY);
                        setanimation(w.getPoint2(), centy, 4, 0, WALLY);
                        setanimation(w.getWall2().getPoint2(), centy + day2, 4, 0, WALLY);
                    }
                }
            }
            if (mapnum > 1) {
                playsound(23);
            }
        }
    }

}
