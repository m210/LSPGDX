// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Screens;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.SkippableAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.LSP.Enemies.nKills;
import static ru.m210projects.LSP.Enemies.nTotalKills;
import static ru.m210projects.LSP.Globals.*;

public class StatisticScreen extends SkippableAdapter {

    public StatisticScreen(BuildGame game) {
        super(game);
    }

    @Override
    public void draw(float delta) {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, 611, 0, 0, 2 | 8);
        renderer.rotatesprite(0, 0, 65536, 0, 611, 127, 0, 8 | 16 | 3);
        int x = 100;
        int y = 10;
        renderer.rotatesprite(x << 16, y << 16, 32768, 0, BLUEDUDE, 0, 0, 2 | 8 | 16);

        Font f = game.getFont(0);

        f.drawTextScaled(renderer, x + 40, y + 10, "left: " + (nTotalKills[1] - nKills[1]), 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
        f.drawTextScaled(renderer, x + 40, y + 20, "total: " + nTotalKills[1], 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);

        renderer.rotatesprite(x << 16, (y += 35) << 16, 32768, 0, REDDUDE, 0, 0, 2 | 8 | 16);

        f.drawTextScaled(renderer, x + 40, y + 10, "left: " + (nTotalKills[2] - nKills[2]), 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
        f.drawTextScaled(renderer, x + 40, y + 20, "total: " + nTotalKills[2], 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);

        renderer.rotatesprite(x << 16, (y += 35) << 16, 32768, 0, GREENDUDE, 0, 0, 2 | 8 | 16);

        f.drawTextScaled(renderer, x + 40, y + 10, "left: " + (nTotalKills[3] - nKills[3]), 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
        f.drawTextScaled(renderer, x + 40, y + 20, "total: " + nTotalKills[3], 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);

        renderer.rotatesprite(x << 16, (y += 35) << 16, 32768, 0, PURPLEDUDE, 0, 0, 2 | 8 | 16);

        f.drawTextScaled(renderer, x + 40, y + 10, "left: " + (nTotalKills[4] - nKills[4]), 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
        f.drawTextScaled(renderer, x + 40, y + 20, "total: " + nTotalKills[4], 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);

        renderer.rotatesprite(x << 16, (y += 35) << 16, 32768, 0, YELLOWDUDE, 0, 0, 2 | 8 | 16);

        f.drawTextScaled(renderer, x + 40, y + 10, "left: " + (nTotalKills[5] - nKills[5]), 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
        f.drawTextScaled(renderer, x + 40, y + 20, "total: " + nTotalKills[5], 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
    }

}
