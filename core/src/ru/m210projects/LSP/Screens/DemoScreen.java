// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Screens;


import com.badlogic.gdx.Screen;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.LSP.Main;
import ru.m210projects.LSP.Types.CompareService.CompareService;
import ru.m210projects.LSP.Types.DemoFile;
import ru.m210projects.LSP.Types.DemoRec;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.LoadSave.lastload;
import static ru.m210projects.LSP.Main.*;

public class DemoScreen extends GameScreen {

    public static final String header = "LSPD";
    public final List<Entry> demofiles = new ArrayList<>();
    public int nDemonum = -1;
    public DemoFile demfile;

    public DemoScreen(Main game) {
        super(game);
    }

    @Override
    public void show() {
        lastload = null;
    }

    public boolean showDemo(Entry entry) {
        onStopRecord();

        demfile = null;
        try (InputStream is = entry.getInputStream()) {
            demfile = new DemoFile(is);
        } catch (Exception e) {
            Console.out.println("Can't play the demo file: " + entry.getName(), OsdColor.RED);
            return false;
        }

        if (numplayers > 1) {
            game.pNet.NetDisconnect(myconnectindex);
        }

        nDifficult = (short) demfile.skill;

        recstat = DEMOSTAT_PLAYING;

//        CompareService.prepare(Paths.get("D:\\DemosData\\LSP\\" + entry.getName() + ".jdm"), CompareService.Type.Read);

        gDemoScreen.newgame(demfile.map);

        Console.out.println("Playing demo " + entry.getName());

        return true;
    }

    @Override
    protected void startboard(final Runnable startboard) {
        game.doPrecache(() -> {
            startboard.run(); //call faketimehandler
            pNet.ResetTimers(); //reset ototalclock
            lockclock = 0;
            pNet.ready2send = false;
        });
    }

    @Override
    public void render(float delta) {
        if (numplayers > 1) {
            pNet.GetPackets();
        }

        DemoRender();

        float smoothratio = 65536;
        if (!game.gPaused) {
            smoothratio = pEngine.getTimer().getsmoothratio(delta);
            if (smoothratio < 0 || smoothratio > 0x10000) {
                smoothratio = BClipRange(smoothratio, 0, 0x10000);
            }
        }

        game.pInt.dointerpolations(smoothratio);
        DrawWorld(smoothratio);

        DrawHud(smoothratio);
        game.pInt.restoreinterpolations();

        if (pMenu.gShowMenu) {
            pMenu.mDrawMenu();
        }

        PostFrame(pNet);

        pEngine.nextpage(delta);
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        return gameKeyDownCommon(gameKey, false);
    }

    private void DemoRender() {
        pNet.ready2send = false;

        if (!game.isCurrentScreen(this)) {
            return;
        }

        if (!game.gPaused && demfile != null) {
            while (engine.getTotalClock() >= (lockclock + TICSPERFRAME)) {
                CompareService.update(demfile.rcnt);
                for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
                    pNet.gFifoInput[pNet.gNetFifoHead[j] & 0xFF][j].Copy(demfile.recsync[demfile.rcnt][j]);
                    pNet.gNetFifoHead[j]++;
                    demfile.reccnt--;
                }

                if (demfile.reccnt <= 0) {
                    demfile = null;

                    if (!showDemo()) {
                        game.changeScreen(gMenuScreen);
                    }
                    return;
                }

                demfile.rcnt++;
                game.pInt.clearinterpolations();

                ProcessFrame(pNet);

            }
        }
    }

    @Override
    public void resume() {
        game.pEngine.getTimer().setTotalClock(lockclock);
        super.resume();
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean showDemo() {
        List<Entry> list = demofiles;
        switch (cfg.gDemoSeq) {
            case 0: //OFF
                return false;
            case 1: //Consistently
                if (nDemonum < (list.size() - 1)) nDemonum++;
                else {
                    nDemonum = 0;
                    // throw new RuntimeException("Done");
                }
                break;
            case 2: //Accidentally
                int nextnum = nDemonum;
                if (list.size() > 1) {
                    while (nextnum == nDemonum) {
                        nextnum = (int) (Math.random() * (list.size()));
                    }
                }
                nDemonum = BClipRange(nextnum, 0, list.size() - 1); // #GDX 28.12.2024
                break;
        }

        if (!list.isEmpty()) {
            boolean result = showDemo(list.get(nDemonum));
            if (!result) {
                list.remove(nDemonum);
                return showDemo();
            }

            return true;
        }

        return false;
    }

    public boolean isDemoFile(Entry file) {
        if (file.exists()) {
            if (file.isExtension("dmo")) {
                try (InputStream is = file.getInputStream()) {
                    String header = StreamUtils.readString(is, DemoScreen.header.length());
                    StreamUtils.skip(is, 4); // rcnt
                    int version = StreamUtils.readUnsignedByte(is);
                    if (header.equals(DemoScreen.header) && version == GDXBYTEVERSION) {
                        return true;
                    }
                } catch (Exception ignore) {
                }
            }
        }
        return false;
    }

    public static boolean isDemoScreen(Screen screen) {
        return screen == gDemoScreen;
    }

    public static boolean isDemoPlaying() {
        return recstat == DEMOSTAT_PLAYING;
    }

    public void onPrepareboard(GameScreen screen) {
        if (screen != this && isDemoPlaying()) {
            gDemoScreen.onStopPlaying();
        }

        if (demfile != null && demfile.version >= GDXBYTEVERSION && isDemoPlaying()) {
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                demfile.playerData.applyTo(gPlayer[i]);
            }
        }

        if (screen != this && isRecordEnabled()) {
            m_recstat = DEMOSTAT_NULL;

            // CompareService.prepare(Paths.get("D:\\DemosData\\LSP\\" + "Demo" + ".jdm"), CompareService.Type.Write);

            int a, b, c, d, democount = 0;
            do {
                a = ((democount / 1000) % 10);
                b = ((democount / 100) % 10);
                c = ((democount / 10) % 10);
                d = (democount % 10);

                String fn = "demo" + a + b + c + d + ".dmo";
                if (!game.getCache().getGameDirectory().getEntry(fn).exists()) {
                    try {
                        Path path = game.getCache().getGameDirectory().getPath().resolve(fn);
                        rec = new DemoRec(new FileOutputStream(path.toFile()), path, 100);
                        Console.out.println("Start recording to " + fn);
                        recstat = DEMOSTAT_RECORD;
                    } catch (Exception e) {
                        Console.out.println("Can't start demo record: " + e, OsdColor.RED);
                    }
                    break;
                }

                democount++;
            } while (democount < 9999);
        }
    }

    public boolean isRecordEnabled() {
        return m_recstat == DEMOSTAT_RECORD;
    }

    public void onStopPlaying() {
        demfile = null;
        recstat = DEMOSTAT_NULL;
    }

    public void onLoad() {
        onStopRecord();
        demfile = null;
        recstat = DEMOSTAT_NULL;
    }


    public void onStopRecord() {
        if (rec == null) {
            return;
        }

        CompareService.close();
        rec.close();
        rec = null;
        recstat = DEMOSTAT_NULL;
    }

    public void onRecord() {
        if (rec != null) {
            rec.record();
        }
    }

    public void demoscan(Directory directory) {
        demofiles.clear();

        for (Entry file : directory.getEntries()) {
            if (file.isExtension("dmo")) {
                if (isDemoFile(file)) {
                    demofiles.add(file);
                }
            }
        }

        if (!demofiles.isEmpty()) {
            demofiles.sort(Comparator.comparing(Entry::getName));
        }
        Console.out.println("There are " + demofiles.size() + " demo(s) in the loop", OsdColor.YELLOW);
    }
}
