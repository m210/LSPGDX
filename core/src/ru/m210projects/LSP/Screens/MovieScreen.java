// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Screens;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.LSP.Sounds;
import ru.m210projects.LSP.Types.LSPMovieFile;

import java.io.FileNotFoundException;

import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.LSP.Globals.TILE_ANIM;

public class MovieScreen extends ru.m210projects.Build.Pattern.ScreenAdapters.MovieScreen {

    public MovieScreen(BuildGame game) {
        super(game, TILE_ANIM);
        nFlags |= 4;
    }

    @Override
    protected MovieFile GetFile(String file) {
        try {
            return new LSPMovieFile(file);
        } catch (FileNotFoundException fnf) {
            Console.out.println(file + " is not found!", OsdColor.RED);
            return null;
        } catch (Exception e) {
            Console.out.println("Error: " + e, OsdColor.RED);
            return null;
        }
    }

    @Override
    protected void StopAllSounds() {
        Sounds.stopAllSounds();
    }

    @Override
    protected byte[] DoDrawFrame(int num) {
        byte[] pic = mvfil.getFrame(num);
        if (((LSPMovieFile) mvfil).paletteChanged) {
            changepalette(mvfil.getPalette());
        }
        return pic;
    }

    @Override
    protected Font GetFont() {
        return game.getFont(1);
    }

    @Override
    protected void DrawEscText(Font font, int pal) {
        Renderer renderer = game.getRenderer();
        int shade = 16 + mulscale(16, EngineUtils.sin((20 * engine.getTotalClock()) & 2047), 16);
        game.getFont(1).drawTextScaled(renderer, 160, 5, "Press ESC to skip", 1.0f, shade, pal, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }
}
