// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Screens;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.ScreenAdapters.GameAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.LSP.Config.LSPKeys;
import ru.m210projects.LSP.Factory.LSPInput;
import ru.m210projects.LSP.Factory.LSPMenuHandler;
import ru.m210projects.LSP.Factory.LSPRenderer;
import ru.m210projects.LSP.Main;
import ru.m210projects.LSP.Menus.MenuInterfaceSet;
import ru.m210projects.LSP.Sounds;
import ru.m210projects.LSP.Types.DemoRec;
import ru.m210projects.LSP.Types.PlayerStruct;

import java.io.FileOutputStream;
import java.nio.file.Path;
import java.util.Arrays;

import static java.lang.Math.min;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Pragmas.scale;
import static ru.m210projects.Build.Render.AbstractRenderer.DEFAULT_SCREEN_FADE;
import static ru.m210projects.LSP.Animate.doanimations;
import static ru.m210projects.LSP.Enemies.inienemies;
import static ru.m210projects.LSP.Factory.LSPMenuHandler.*;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.LoadSave.*;
import static ru.m210projects.LSP.Main.*;
import static ru.m210projects.LSP.Player.*;
import static ru.m210projects.LSP.Quotes.*;
import static ru.m210projects.LSP.Sectors.tagcode;
import static ru.m210projects.LSP.Sounds.*;
import static ru.m210projects.LSP.Sprites.checktouchsprite;
import static ru.m210projects.LSP.Sprites.statuslistcode;
import static ru.m210projects.LSP.View.*;
import static ru.m210projects.LSP.Weapons.drawweapons;

public class GameScreen extends GameAdapter {

    public int gNameShowTime;
    public final Main game;
    private int nonsharedtimer;

    public GameScreen(Main game) {
        super(game, gLoadingScreen);
        this.game = game;
    }

    @Override
    public void ProcessFrame(BuildNet net) {
        FixPalette();
        for (short i = connecthead; i >= 0; i = connectpoint2[i]) {
            gPlayer[i].pInput.Copy(net.gFifoInput[net.gNetFifoTail & 0xFF][i]);
            if ((gPlayer[i].pInput.bits & (1 << 17)) != 0) {
                game.gPaused = !game.gPaused;
                sndHandlePause(game.gPaused);
            }
        }
        net.gNetFifoTail++;

        if (game.gPaused || !DemoScreen.isDemoPlaying() && !DemoScreen.isDemoScreen(this) && (game.menu.gShowMenu || Console.out.isShowing())) {
            if (!game.menu.isOpened(game.menu.mMenus[LASTSAVE])) {
                return;
            }
        }

        gDemoScreen.onRecord();

        int TICSPERFRAME = engine.getTimer().getFrameTicks(); // TODO: Temporaly code to reset interpolation in LegacyTimer
        lockclock += TICSPERFRAME;

        for (short i = connecthead; i >= 0; i = connectpoint2[i]) {
            gPlayer[i].UpdatePlayerLoc();
            processinput(i);
            checktouchsprite(i);
        }

        MarkSectorSeen(gPlayer[myconnectindex].sectnum);

        if (followmode) {
            followa += (int) followang;

            followx += (followvel * EngineUtils.sin((512 + 2048 - followa) & 2047)) >> 10;
            followy += (followvel * EngineUtils.sin((512 + 1024 - 512 - followa) & 2047)) >> 10;

            followx += (followsvel * EngineUtils.sin((512 + 1024 - 512 - followa) & 2047)) >> 10;
            followy -= (followsvel * EngineUtils.sin((512 + 2048 - followa) & 2047)) >> 10;
        }

        doanimations(TICSPERFRAME);
        tagcode();
        statuslistcode();
        updatesounds();
        totalmoves++;
    }

    @Override
    public void DrawWorld(float smooth) {
        drawscreen(screenpeek, (int) smooth);
    }

    @Override
    public void DrawHud(float smooth) {
        if (pMenu.gShowMenu && !(pMenu.getCurrentMenu() instanceof MenuInterfaceSet)) {
            return;
        }

        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();

        if (mapnum > 0) {
            if (gPlayer[screenpeek].gViewMode != kView2DIcon) {
                if (gPlayer[screenpeek].nHealth > 0 && !isonwater(screenpeek) && mapnum > 0) {
                    drawweapons(screenpeek);
                }
            }

            int yoffs = scale(50, cfg.gHUDSize, 65536);
            drawbar(10, ydim - yoffs, cfg.gHUDSize, myconnectindex);
            viewDrawStats(10, ydim - yoffs, cfg.gHUDSize);
        }
        viewDisplayMessage();

        if (gPlayer[myconnectindex].gViewMode != kView3D) {
            int pos = 25;
            if (followmode) {
                game.getFont(1).drawText(renderer, 20, pos += scale(25, cfg.gHUDSize, 65536), "Follow mode", cfg.gHUDSize / 65536.0f, 0, 4,
                        TextAlign.Left, Transparent.None, true);
            }

            game.getFont(1).drawText(renderer, 20, pos + scale(25, cfg.gHUDSize, 65536),
                    book + "b " + chapter + "c " + verse + "v", cfg.gHUDSize / 65536.0f, 0, 4, TextAlign.Left, Transparent.None, true);
        }

        if (engine.getTotalClock() < gNameShowTime) {
            Transparent transp = Transparent.None;
            if (engine.getTotalClock() > gNameShowTime - 20) {
                transp = Transparent.Bit1;
            }
            if (engine.getTotalClock() > gNameShowTime - 10) {
                transp = Transparent.Bit2;
            }

            if (cfg.showMapInfo != 0 && !game.menu.gShowMenu && mapnum > 0) {
//				if (mUserFlag != UserFlag.UserMap) {
                game.getFont(2).drawTextScaled(renderer, 160, 100, book + "b " + chapter + "c " + verse + "v", 1.0f, -128, 70,
                        TextAlign.Center, transp, ConvertType.Normal, true);
//				}
            }
        }

        if (game.gPaused && !game.menu.gShowMenu) {
            game.getFont(2).drawTextScaled(renderer, 160, 100, "GAME PAUSED", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        if (nPalDiff != 0) {
            DEFAULT_SCREEN_FADE.set(min(63, rtint) << 2,min(63, gtint) << 2,min(63, btint) << 2, nPalDiff | 128);
            renderer.showScreenFade(DEFAULT_SCREEN_FADE);
        }
    }

    @Override
    public void processInput(GameProcessor processor) {
        if (gPlayer[myconnectindex].gViewMode != 3) {
            int j = engine.getTotalClock() - nonsharedtimer;
            nonsharedtimer += j;
            if (processor.isGameKeyPressed(GameKeys.Enlarge_Screen)) {
                gPlayer[myconnectindex].zoom += mulscale(j, Math.max(gPlayer[myconnectindex].zoom, 256), 6);
            }
            if (processor.isGameKeyPressed(GameKeys.Shrink_Screen)) {
                gPlayer[myconnectindex].zoom -= mulscale(j, Math.max(gPlayer[myconnectindex].zoom, 256), 6);
            }

            if ((gPlayer[myconnectindex].zoom > 2048)) {
                gPlayer[myconnectindex].zoom = 2048;
            }
            if ((gPlayer[myconnectindex].zoom < 48)) {
                gPlayer[myconnectindex].zoom = 48;
            }
        }
    }

    protected boolean gameKeyDownCommon(GameKey gameKey, boolean inGame) {
        // the super has console button handling
        if (super.gameKeyDown(gameKey)) {
            return true;
        }

        LSPMenuHandler menu = game.menu;
        if (GameKeys.Menu_Toggle.equals(gameKey)) {
            if (inGame) {
                menu.mOpen(menu.mMenus[GAME], -1);
            } else {
                menu.mOpen(menu.mMenus[MAIN], -1);
            }
            return true;
        }

        if (LSPKeys.Show_Loadmenu.equals(gameKey)) {
            if (numplayers > 1 || game.nNetMode != NetMode.Single) {
                return false;
            }

            menu.mOpen(menu.mMenus[LOADGAME], -1);
            return true;
        }

        if (LSPKeys.Show_Sounds.equals(gameKey)) {
            menu.mOpen(menu.mMenus[AUDIOSET], -1);
            return true;
        }

        if (LSPKeys.Show_Options.equals(gameKey)) {
            menu.mOpen(menu.mMenus[OPTIONS], -1);
            return true;
        }

        if (LSPKeys.Gamma.equals(gameKey)) {
            menu.mOpen(menu.mMenus[COLORCORR], -1);
            return true;
        }

        if (LSPKeys.Quit.equals(gameKey)) {
            menu.mOpen(menu.mMenus[QUIT], -1);
            return true;
        }

        if (LSPKeys.Toggle_Crosshair.equals(gameKey)) {
            cfg.gCrosshair = !cfg.gCrosshair;
            viewSetMessage("Crosshair: " + (cfg.gCrosshair ? "ON" : "OFF"));
        }

        if (LSPKeys.Make_Screenshot.equals(gameKey)) {
            makeScreenshot();
            return true;
        }

        return false;
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        LSPMenuHandler menu = game.menu;
        if (gameKeyDownCommon(gameKey, true)) {
            return true;
        }

        // non mappable function keys
        if (LSPKeys.Show_Savemenu.equals(gameKey)) {
            if (game.nNetMode == NetMode.Single) {
                gGameScreen.capture(160, 100);
                menu.mOpen(menu.mMenus[SAVEGAME], -1);
            }
        } else if (LSPKeys.Quicksave.equals(gameKey)) {
            quicksave();
        } else if (LSPKeys.Quickload.equals(gameKey)) {
            quickload();
        } else if (LSPKeys.AutoRun.equals(gameKey)) {
            cfg.gAutoRun = !cfg.gAutoRun;
            viewSetMessage("Autorun: " + (cfg.gAutoRun ? "ON" : "OFF"));
        } else if (GameKeys.Mouse_Aiming.equals(gameKey)) {
            cfg.setgMouseAim(!cfg.isgMouseAim());
            viewSetMessage("Mouse aiming: " + (cfg.isgMouseAim() ? "ON" : "OFF"));
        } else if (LSPKeys.Map_Follow_Mode.equals(gameKey)) {
            followmode = !followmode;
            if (followmode) {
                followx = gPlayer[myconnectindex].x;
                followy = gPlayer[myconnectindex].y;
                followa = (int) gPlayer[myconnectindex].ang;
            }
            viewSetMessage("Follow mode " + (followmode ? "ON" : "OFF"));
        } else if (GameKeys.Map_Toggle.equals(gameKey)) {
            gPlayer[myconnectindex].gViewMode = (byte) setOverHead(gPlayer[myconnectindex].gViewMode);
        }

        return false;
    }

    @Override
    public void PostFrame(BuildNet net) {
        if (gQuickSaving) {
            if (captBuffer != null) {
                savegame(game.getUserDirectory(), "[quicksave_" + quickslot + "]", "quicksav" + quickslot + ".sav");
                quickslot ^= 1;
                gQuickSaving = false;
            } else {
                gGameScreen.capture(160, 100);
            }
        }

        if (gAutosaveRequest) {
            if (captBuffer != null) {
                savegame(game.getUserDirectory(), "[autosave]", "autosave.sav");
                gAutosaveRequest = false;
            } else {
                gGameScreen.capture(160, 100);
            }
        }
    }

    protected void makeScreenshot() {
        String name;

        name = "scr-map" + mapnum + "-xxxx.png";
        Renderer renderer = game.getRenderer();
        String filename = renderer.screencapture(game.getUserDirectory(), name);
        if (filename != null) {
            viewSetMessage(filename + " saved");
        } else {
            viewSetMessage("Screenshot not saved. Access denied!");
        }
    }

    protected int setOverHead(int mode) {
        int out = 0;
        switch (mode) {
            case kView3D:
                if (cfg.gOverlayMap != 0) {
                    out = kView2D;
                } else {
                    out = kView2DIcon;
                }
                break;
            case kView2D:
                if (cfg.gOverlayMap == 1) {
                    out = kView3D;
                } else {
                    out = kView2DIcon;
                }
                break;
            case kView2DIcon:
                out = kView3D;
                break;
        }
        return out;
    }

    @Override
    protected boolean prepareboard(Entry entry) {
        stopmusic();
        stopallsounds();

        try {
            Board board = engine.loadboard(entry);

            BuildPos pos = board.getPos();
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                PlayerStruct pPlayer = gPlayer[i];
                if (mapnum > 1) {
                    pPlayer.savePlayersInventory();
                }
                pPlayer.pInput.reset(); // #GDX 16.06.2024
                pPlayer.ox = pPlayer.x = pos.x;
                pPlayer.oy = pPlayer.y = pos.y;
                pPlayer.oz = pPlayer.z = pos.z;
                pPlayer.oang = pPlayer.ang = pos.ang;
                pPlayer.osectnum = pPlayer.sectnum = pos.sectnum;
                pPlayer.ohoriz = pPlayer.horiz = 100;
                pPlayer.gViewMode = 3;
                pPlayer.zoom = 768;
                pPlayer.nSprite = -1;
                pPlayer.nWeaponState = 0;
                pPlayer.nBobCount = 0;
                pPlayer.nWeaponSeq = 0;
                pPlayer.nWeapon = nPlayerFirstWeapon;
                pPlayer.nLastChoosedWeapon = 6;
                pPlayer.nLastManaWeapon = 13;
                pPlayer.nPlayerStatus = 0;
                pPlayer.word_586FC = 0;
                pPlayer.nWeaponImpact = 0; // #GDX 30.06.2024
                pPlayer.nWeaponImpactAngle = 0;
                pPlayer.hvel = 0;
                pPlayer.isWeaponsSwitching = 0;
                pPlayer.nSwitchingClock = 0;
                pPlayer.word_58794 = 0;
                pPlayer.nWeaponShootCount = 0;
                pPlayer.nSlimeDamageCount = 0;
                pPlayer.nWeaponClock = 0;

                switch (nPlayerFirstWeapon) {
                    case 18:
                    case 20:
                        pPlayer.nFirstWeaponDamage = 9;
//				nMagicWeaponDamage = 19;
                        pPlayer.nSecondWeaponDamage = 18;
                        break;
                    case 19:
                        pPlayer.nFirstWeaponDamage = 7;
//				nMagicWeaponDamage = 15;
                        pPlayer.nSecondWeaponDamage = 21;
                        break;
//				nMagicWeaponDamage = 18;
                    case 21:
                        pPlayer.nFirstWeaponDamage = 8;
//				nMagicWeaponDamage = 18;
                        pPlayer.nSecondWeaponDamage = 18;
                        break;
                    case 22:
                        pPlayer.nFirstWeaponDamage = 6;
//				nMagicWeaponDamage = 13;
                        pPlayer.nSecondWeaponDamage = 17;
                        break;
                    case 23:
                        pPlayer.nFirstWeaponDamage = 6;
//				nMagicWeaponDamage = 15;
                        pPlayer.nSecondWeaponDamage = 16;
                        break;
                    case 24:
                        pPlayer.nFirstWeaponDamage = pPlayer.nRandDamage1;
//				nMagicWeaponDamage = pPlayer.nRandDamage1 + word_58A8E;
                        pPlayer.nSecondWeaponDamage = pPlayer.nRandDamage1 + pPlayer.nRandDamage2;
                        break;
                    default:
                        pPlayer.nFirstWeaponDamage = 9;
//				nMagicWeaponDamage = 18;
                        pPlayer.nSecondWeaponDamage = 16;
                        break;
                }
            }

            LSPRenderer renderer = game.getRenderer();
            renderer.setVisibility(15);
            pskyoff[0] = 0;
            pskyoff[1] = 0;
            pskyoff[2] = 0;
            pskyoff[3] = 0;
            parallaxtype = 0;
            renderer.setParallaxOffset(256);
            pskybits = 2;
            totalmoves = 0;
            nNextMap = 0;
            numQuotes = 0;

            for (int i = 0; i < MAXPLAYERS; i++) {
                waterfountainwall[i] = -1;
                waterfountaincnt[i] = 0;
            }

            warpsectorcnt = 0;
            warpsector2cnt = 0;
            xpanningsectorcnt = 0;
            floorpanningcnt = 0;
            swingcnt = 0;
            revolvecnt = 0;
            subwaytrackcnt = 0;
            dragsectorcnt = 0;

            nKickSprite = -1;
            for (short i = 0; i < boardService.getSectorCount(); i++) {
                Sector sec = boardService.getSector(i);
                if (sec == null) {
                    continue;
                }

//			if (cfgParameter1[3] != 0) {
                switch (sec.getCeilingpicnum()) {
                    case 250:
                        sec.setCeilingpicnum(252);
                        break;
                    case 251:
                        sec.setCeilingpicnum(255);
                        break;
                    case 254:
                        sec.setCeilingpicnum(253);
                        break;
                }

                switch (sec.getFloorpicnum()) {
                    case 250:
                        sec.setFloorpicnum(252);
                        break;
                    case 251:
                        sec.setFloorpicnum(255);
                        break;
                    case 254:
                        sec.setFloorpicnum(253);
                        break;
                }
//			}

                switch (sec.getLotag()) {
                    case 4:
                    case 5:
                        floorpanninglist[floorpanningcnt++] = i;
                        break;
                    case 10:
                        warpsectorlist[warpsectorcnt++] = i;
                        break;
                    case 11:
                        xpanningsectorlist[xpanningsectorcnt++] = i;
                        break;
                    case 12: {
                        int dax = 0x7fffffff;
                        int day = 0x7fffffff;
                        int dax2 = 0x80000000;
                        int day2 = 0x80000000;
                        Wall k = null;

                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            if (wal.getX() < dax) {
                                dax = wal.getX();
                            }
                            if (wal.getY() < day) {
                                day = wal.getY();
                            }
                            if (wal.getX() > dax2) {
                                dax2 = wal.getX();
                            }
                            if (wal.getY() > day2) {
                                day2 = wal.getY();
                            }
                            if (wal.getLotag() == 3) {
                                k = wal;
                            }
                        }

                        if (k != null) {
                            if (k.getX() == dax) {
                                dragxdir[dragsectorcnt] = -16;
                            }
                            if (k.getY() == day) {
                                dragydir[dragsectorcnt] = -16;
                            }
                            if (k.getX() == dax2) {
                                dragxdir[dragsectorcnt] = 16;
                            }
                            if (k.getY() == day2) {
                                dragydir[dragsectorcnt] = 16;
                            }

                            Wall w1 = sec.getWallNode().get();
                            int dasector = w1.getNextsector();
                            Sector nsec = boardService.getSector(dasector);
                            if (nsec != null) {
                                dragx1[dragsectorcnt] = 0x7fffffff;
                                dragy1[dragsectorcnt] = 0x7fffffff;
                                dragx2[dragsectorcnt] = 0x80000000;
                                dragy2[dragsectorcnt] = 0x80000000;
                                for (ListNode<Wall> wn = nsec.getWallNode(); wn != null; wn = wn.getNext()) {
                                    Wall wal = wn.get();
                                    if (wal.getX() < dragx1[dragsectorcnt]) {
                                        dragx1[dragsectorcnt] = wal.getX();
                                    }
                                    if (wal.getY() < dragy1[dragsectorcnt]) {
                                        dragy1[dragsectorcnt] = wal.getY();
                                    }
                                    if (wal.getX() > dragx2[dragsectorcnt]) {
                                        dragx2[dragsectorcnt] = wal.getX();
                                    }
                                    if (wal.getY() > dragy2[dragsectorcnt]) {
                                        dragy2[dragsectorcnt] = wal.getY();
                                    }
                                }

                                dragx1[dragsectorcnt] += (w1.getX() - dax);
                                dragy1[dragsectorcnt] += (w1.getY() - day);
                                dragx2[dragsectorcnt] -= (dax2 - w1.getX());
                                dragy2[dragsectorcnt] -= (day2 - w1.getY());
                                dragfloorz[dragsectorcnt] = sec.getFloorz();
                                dragsectorlist[dragsectorcnt++] = i;
                                sec.setFloorstat(sec.getFloorstat() | 64);
                            }
                        }
                        break;
                    }
                    case 13: {
                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            if (wal.getLotag() == 4) {
                                Wall w4 = wal.getWall2().getWall2().getWall2().getWall2();
                                if ((wal.getX() == w4.getX()) && (wal.getY() == w4.getY())) { // Door opens counterclockwise
                                    swingdoor[swingcnt].wall[0] = wn.getIndex();
                                    swingdoor[swingcnt].wall[1] = wal.getPoint2();
                                    swingdoor[swingcnt].wall[2] = wal.getWall2().getPoint2();
                                    swingdoor[swingcnt].wall[3] = wal.getWall2().getWall2().getPoint2();
                                    swingdoor[swingcnt].angopen = 1536;
                                    swingdoor[swingcnt].angclosed = 0;
                                    swingdoor[swingcnt].angopendir = -1;
                                } else { // Door opens clockwise
                                    swingdoor[swingcnt].wall[0] = wal.getPoint2();
                                    swingdoor[swingcnt].wall[1] = wn.getIndex();
                                    swingdoor[swingcnt].wall[2] = engine.lastwall(wn.getIndex());
                                    swingdoor[swingcnt].wall[3] = engine.lastwall(swingdoor[swingcnt].wall[2]);
                                    swingdoor[swingcnt].angopen = 512;
                                    swingdoor[swingcnt].angclosed = 0;
                                    swingdoor[swingcnt].angopendir = 1;
                                }

                                for (int k = 0; k < 4; k++) {
                                    Wall w = boardService.getWall(swingdoor[swingcnt].wall[k]);
                                    if (w == null) {
                                        continue;
                                    }

                                    swingdoor[swingcnt].x[k] = w.getX();
                                    swingdoor[swingcnt].y[k] = w.getY();
                                }

                                swingdoor[swingcnt].sector = i;
                                swingdoor[swingcnt].ang = swingdoor[swingcnt].angclosed;
                                swingdoor[swingcnt].anginc = 0;
                                swingcnt++;
                            }
                        }
                        break;
                    }
                    case 14: {
                        int startwall = sec.getWallptr();
                        int endwall = (short) (startwall + sec.getWallnum() - 1);
                        int dax = 0;
                        int day = 0;
                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            dax += wal.getX();
                            day += wal.getY();
                        }
                        revolvepivotx[revolvecnt] = dax / (endwall - startwall + 1);
                        revolvepivoty[revolvecnt] = day / (endwall - startwall + 1);

                        int k = 0;
                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            revolvex[revolvecnt][k] = wal.getX();
                            revolvey[revolvecnt][k] = wal.getY();
                            k++;
                        }

                        revolvesector[revolvecnt] = i;
                        revolveang[revolvecnt] = 0;
                        revolvecnt++;
                        break;
                    }
                    case 15: {
                        subwaytracksector[subwaytrackcnt][0] = i;
                        subwaystopcnt[subwaytrackcnt] = 0;
                        int dax = 0x7fffffff;
                        int day = 0x7fffffff;
                        int dax2 = 0x80000000;
                        int day2 = 0x80000000;
                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            if (wal.getX() < dax) {
                                dax = wal.getX();
                            }
                            if (wal.getY() < day) {
                                day = wal.getY();
                            }
                            if (wal.getX() > dax2) {
                                dax2 = wal.getX();
                            }
                            if (wal.getY() > day2) {
                                day2 = wal.getY();
                            }
                        }

                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            if (wal.getLotag() == 5) {
                                if ((wal.getX() > dax) && (wal.getY() > day) && (wal.getX() < dax2) && (wal.getY() < day2)) {
                                    subwayx[subwaytrackcnt] = wal.getX();
                                } else {
                                    subwaystop[subwaytrackcnt][subwaystopcnt[subwaytrackcnt]] = wal.getX();
                                    subwaystopcnt[subwaytrackcnt]++;
                                }
                            }
                        }

                        for (int j = 1; j < subwaystopcnt[subwaytrackcnt]; j++) {
                            for (int k = 0; k < j; k++) {
                                if (subwaystop[subwaytrackcnt][j] < subwaystop[subwaytrackcnt][k]) {
                                    int s = subwaystop[subwaytrackcnt][j];
                                    subwaystop[subwaytrackcnt][j] = subwaystop[subwaytrackcnt][k];
                                    subwaystop[subwaytrackcnt][k] = s;
                                }
                            }
                        }

                        subwaygoalstop[subwaytrackcnt] = 0;
                        for (int j = 0; j < subwaystopcnt[subwaytrackcnt]; j++) {
                            if (Math.abs(subwaystop[subwaytrackcnt][j] - subwayx[subwaytrackcnt]) < Math
                                    .abs(subwaystop[subwaytrackcnt][subwaygoalstop[subwaytrackcnt]] - subwayx[subwaytrackcnt])) {
                                subwaygoalstop[subwaytrackcnt] = j;
                            }
                        }

                        subwaytrackx1[subwaytrackcnt] = dax;
                        subwaytracky1[subwaytrackcnt] = day;
                        subwaytrackx2[subwaytrackcnt] = dax2;
                        subwaytracky2[subwaytrackcnt] = day2;

                        subwaynumsectors[subwaytrackcnt] = 1;
                        for (int j = 0; j < boardService.getSectorCount(); j++) {
                            if (j != i) {
                                Sector sec2 = boardService.getSector(j);
                                if (sec2 == null) {
                                    continue;
                                }

                                int startwall = sec2.getWallptr();
                                Wall w1 = boardService.getWall(startwall);
                                if (w1 != null && w1.getX() > subwaytrackx1[subwaytrackcnt]) {
                                    if (w1.getY() > subwaytracky1[subwaytrackcnt]) {
                                        if (w1.getX() < subwaytrackx2[subwaytrackcnt]) {
                                            if (w1.getY() < subwaytracky2[subwaytrackcnt]) {
                                                if (sec2.getLotag() == 16) {
                                                    sec2.setLotag(17); // Make special subway door
                                                }

                                                if (sec2.getFloorz() != sec.getFloorz()) {
                                                    sec2.setCeilingstat(sec2.getCeilingstat() | 64);
                                                    sec2.setFloorstat(sec2.getFloorstat() | 64);
                                                }
                                                subwaytracksector[subwaytrackcnt][subwaynumsectors[subwaytrackcnt]] = (short) j;
                                                subwaynumsectors[subwaytrackcnt]++;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        subwayvel[subwaytrackcnt] = 64;
                        subwaypausetime[subwaytrackcnt] = 2400;
                        subwaytrackcnt++;
                        break;
                    }
                    case 30:
                        warpsector2list[warpsector2cnt++] = i;
                        break;
                    case 99:
                    case 98:
                    case 97:
                        if (cfg.bShowExit) {
                            show2dsector.setBit(i);
                        }
                        break;
                }

                for (ListNode<Sprite> node = boardService.getSectNode(i); node != null; node = node.getNext()) {
                    Sprite spr = node.get();
                    if (!engine.getTile(spr.getPicnum()).hasSize()) {
                        spr.setCstat(spr.getCstat() & ~1);
                    }

                    switch (spr.getPicnum()) {
                        case 51:
                            if (mapnum == 0) {
                                nKickSprite = node.getIndex();
                            }
                            break;
                        case 186: // fire
                        case 187:
                        case 188:
                        case 189:
                            spr.setCstat(spr.getCstat() & ~48);
                        case 601: // umbrella
                        case 602: // cookie
                        case 603: // armor
                        case 604: // coins
                        case 605: // balls
                        case 606: // pikes
                        case 607: // shurikens
                        case 608: // knifes
                        case 609: // kunai
                        case 700: // tree root1?
                        case 701: // tree root2?
                        case 702: // chocolate
                        case 703: // flowers with roots
                        case 705: // flower
                        case 706: // mushroom
                        case 707: // manna bottle
                        case 710: // blue book
                        case 711: // brown book
                        case 712: // stone
                        case 713: // wooden book
                        case 714: // papirus
                        case 720: // golden key
                        case 721: // bronze key
                        case 722: // silver key
                        case 723: // gray key
                        case 724: // green key
                        case 725: // red key
                        case 727: // gold coin
                            spr.setCstat(spr.getCstat() & ~257);
                            break;
                    }
                }
            }

            ypanningwallcnt = 0;
            for (short w = 0; w < boardService.getWallCount(); w++) {
                Wall wal = boardService.getWall(w);
                if (wal == null) {
                    continue;
                }

                if (wal.getLotag() == 1) {
                    ypanningwalllist[ypanningwallcnt++] = w;
                }

                // GDX Blocked maskedwalls fix
                Wall wal2 = boardService.getWall(wal.getNextwall());
                if ((wal.getCstat() & 1) != 0 && wal2 != null) {
                    wal2.setCstat(wal2.getCstat() | 1);
                }

                if (mapnum == 0) {
//				if (wal.lotag == 102) { //sound setup
//					dword_5AF64 = w;
//				} else if (wal.lotag == 103) { //music setup
//					dword_5AF60 = w;
//				} else
                    if (wal.getLotag() == 121) { // train finish wall
                        nTrainWall = w;
                    }
//				else if (wal.lotag == 123) { //train start wall
//					dword_5AF6C = w;
//				}
                    else if (wal.getLotag() == 124) { // door to difficult
                        nDiffDoor = w;
                    } else if (wal.getLotag() == 125) { // door to difficult backside
                        nDiffDoorBack = w;
                    }
                }
            }

            bActiveTrain = false;

            inienemies();

            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                InitPlayer(i, pos);
                if (mapnum > 1) {
                    gPlayer[i].loadPlayersInventory();
                }
            }

            if (mapnum == 4) {
                Sector sec = boardService.getSector(82);
                if (sec != null && sec.getHitag() == 0 && sec.getLotag() == 20) {
                    sec.setLotag(21);
                }
            }

            if (mapnum == 11) {
                Sector sec = boardService.getSector(22);
                if (sec != null && sec.getFloorz() == -2048 && sec.getLotag() == 2 && sec.getHitag() == 3) {
                    sec.setFloorz(sec.getFloorz() + 4096);
                    for (ListNode<Sprite> node = boardService.getSectNode(22); node != null; node = node.getNext()) {
                        Sprite spr = node.get();
                        spr.setZ(spr.getZ() + 4096);
                    }
                    sec.setLotag(1);
                }
            }

            if (mapnum == 21) {
                Sector sec = boardService.getSector(32);
                if (sec != null && sec.getCeilingz() == sec.getFloorz() && sec.getLotag() == 8 && sec.getHitag() == 2) {
                    sec.setCeilingz(sec.getCeilingz() - 8192);
                    sec.setFloorz(sec.getFloorz() + 8192);
                    sec.setHitag(0);
                }
            }

            if (mapnum == 32) {
                Sector sec = boardService.getSector(29);
                if (sec != null && sec.getFloorz() == -5120 && sec.getLotag() == 30) {
                    sec.setFloorz(sec.getFloorz() + 8192);
                }
            }

            resetQuotes();
            GrabPalette();

            lockclock = 0; // #GDX 13.06.2024
            engine.srand(17);
            ((LSPInput) game.getProcessor()).reset();
            gDemoScreen.onPrepareboard(this);

            startmusic(getMap(mapnum).music - 1);

            gNameShowTime = 500;
            game.pNet.ResetTimers();

            return true;
        } catch (Exception e) {
            Console.out.println("Load map exception: " + e);
            if (e.getMessage() != null) {
                game.GameMessage("Load map exception:\n" + e.getMessage());
            } else {
                game.GameMessage("Can't load the map " + entry);
            }
        }
        return false;
    }

    public static MapStruct getMap(int mapnum) {
        if (mapnum < 0 || mapnum >= maps.length) {
            throw new AssertException("Wrong map");
        }
        return maps[mapnum];
    }

    public void newgame(int num) {
        gDemoScreen.onStopRecord();

        mapnum = num;
        engine.getTileManager().setTilesPath(num == 0 ? 0 : 1);
        String map = "lev" + num + ".map";
        game.nNetMode = NetMode.Single;

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            gPlayer[i].nHealth = MAXHEALTH;
            gPlayer[i].nMana = MAXMANNA;
            Arrays.fill(gPlayer[i].nAmmo, (short) 0);
            gPlayer[i].nAmmo[1] = 100;
            gPlayer[i].nAmmo[7] = 1;
            gPlayer[i].calcRandomVariables();
        }

        updatechapter(mapnum);
        loadboard(game.getCache().getEntry(map, true), null).setTitle("Loading " + map);
    }

    private void updatechapter(int map) {
        int mnum = maps[map].num & 0xFF;
        book = (mnum % 100) % 10;
        chapter = mnum / 100;
        verse = (mnum % 100) / 10;
    }

    public boolean NextMap() {
        int nextmap = getMap(mapnum).nextmap[nNextMap - 1];
        nNextMap = 0;
        if (nextmap == 99) // the end
        {
            return true;
        }

        if (nextmap == 0) {
            nextmap = 1;
        }

        updatechapter(mapnum);

        gAutosaveRequest = true;
        changemap(nextmap);
        return false;
    }

    public void changemap(int num) {
        mapnum = num;

        updatechapter(mapnum);

        String map = "lev" + mapnum + ".map";
        loadboard(game.getCache().getEntry(map, true), null).setTitle("Loading " + map);
    }

    @Override
    public void sndHandlePause(boolean pause) {
        Sounds.sndHandlePause(pause);
    }
}
