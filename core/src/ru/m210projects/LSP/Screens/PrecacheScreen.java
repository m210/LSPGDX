// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Screens;

import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.LSP.Main;

import static ru.m210projects.Build.Engine.MAXSPRITES;
import static ru.m210projects.Build.Engine.MAXSTATUS;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.boardService;

public class PrecacheScreen extends DefaultPrecacheScreen {

    public PrecacheScreen(Runnable toLoad, PrecacheListener listener) {
        super(Main.game, toLoad, listener);

        addQueue("Preload floor and ceiling tiles...", () -> {
            Sector[] sectors = boardService.getBoard().getSectors();
            for (Sector sec : sectors) {
                addTile(sec.getFloorpicnum());
                addTile(sec.getCeilingpicnum());
            }
            doprecache(0);
        });

        addQueue("Preload wall tiles...", () -> {
            for (Wall wall : boardService.getBoard().getWalls()) {
                addTile(wall.getPicnum());
                if (wall.getOverpicnum() >= 0) {
                    addTile(wall.getOverpicnum());
                }
            }
            doprecache(0);
        });

        addQueue("Preload sprite tiles...", () -> {
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite spr = boardService.getSprite(i);
                if (spr != null && spr.getStatnum() < MAXSTATUS) {
                    cachespritenum(spr);
                }
            }
            doprecache(1);
        });
    }

    @Override
    protected void draw(String title, int index) {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, 770, -128,
                0, 2 | 8 | 64);

        game.getFont(2).drawTextScaled(renderer, 160, 80, "Loading", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
        game.getFont(2).drawTextScaled(renderer, 160, 114, "please wait...", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);

        game.getFont(2).drawTextScaled(renderer, 160, 130, title, 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
    }

    private void cachespritenum(Sprite spr) {
        int maxc = 1;
        switch (spr.getPicnum()) {
            case BLUEDUDE:
            case REDDUDE:
            case YELLOWDUDE:
            case PURPLEDUDE:
            case GREENDUDE:
                maxc = 40;
                break;
        }
        for (int j = spr.getPicnum(); j < (spr.getPicnum() + maxc); j++) {
            addTile(j);
        }
    }

}
