package ru.m210projects.LSP.Screens;

import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.Renderer;

public class LSPMessageScreen extends MessageScreen {

    public LSPMessageScreen(BuildGame game, String header, String message, MessageType type) {
        super(game, header, message, game.getFont(2), game.getFont(0), type);
    }

    @Override
    public void drawBackground(Renderer renderer) {
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, 770, 24, 1, 2 | 8);
    }
}
