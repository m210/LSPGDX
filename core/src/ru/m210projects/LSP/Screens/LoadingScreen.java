// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Screens;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.LoadingAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.LSP.Globals.*;

public class LoadingScreen extends LoadingAdapter {

    public LoadingScreen(BuildGame game) {
        super(game);
    }

    @Override
    protected void draw(String title, float delta) {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, 770, 0, 0, 2 | 8);
        game.getFont(2).drawTextScaled(renderer, 160, 80, "Loading...", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        game.getFont(2).drawTextScaled(renderer, 160, 100, book + "b " + chapter + "c " + verse + "v", 1.0f, -128, 70, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }

}
