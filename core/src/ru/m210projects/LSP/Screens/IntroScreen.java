// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Screens;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.SkippableAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;

import static ru.m210projects.Build.Gameutils.coordsConvertXScaled;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.LSP.Sounds.*;

public class IntroScreen extends SkippableAdapter {

    private int toffs;
    private int state;
    private float time;

    public IntroScreen(BuildGame game) {
        super(game);
    }

    @Override
    public void show() {
        toffs = 0;
        state = 0;
        time = 0;

        stopallsounds();
        game.pNet.ResetTimers();
        startmusic(14);
    }

    @Override
    public void draw(float delta) {
        switch (state) {
            case 0:
                screen1(delta);
                break;
            case 1:
                screen2(delta);
                break;
            case 2:
                screen3(delta);
                break;
        }
    }

    private void screen1(float delta) {
        if ((time += delta) >= 0.2f) {
            if (++toffs >= 320) {
                toffs = 0;
            }
            time = 0;
        }

        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        int xscreen = coordsConvertXScaled(1, ConvertType.Normal);
        renderer.rotatesprite(-toffs << 16, 0, 65536, 0, 1102, 0, 0, 2 | 8 | 16, xscreen, 0, xdim, ydim);
        renderer.rotatesprite((319 - toffs) << 16, 0, 65536, 0, 1102, 0, 0, 2 | 8 | 16, 0, 0, xdim - xscreen, ydim);
        renderer.rotatesprite(0, 10 << 16, 65536, 0, 1100, 0, 0, 2 | 8 | 16);

        if (engine.getTotalClock() < 762) {
            int angle = 8 * engine.getTotalClock();
            int scale = angle << 5;
            if (engine.getTotalClock() >= 256) {
                if (engine.getTotalClock() >= 514) {
                    angle = 8 * (engine.getTotalClock() + 1286);
                    scale = (762 - engine.getTotalClock()) << 8;
                } else {
                    scale = 65536;
                    angle = 0;
                }
            }
            renderer.rotatesprite(160 << 16, 100 << 16, scale, angle, 610, 0, 0, 2 | 8, xscreen, 0, xdim - xscreen, ydim);// accend inc
        }

        if (engine.getTotalClock() > 800) {
            int count = (engine.getTotalClock() - 800);
            int xoffs = count >> 3;
            if (count >= 2464) {
                xoffs = 308;
            }

            renderer.rotatesprite((320 - xoffs) << 16, 129 << 16, 65536, 0, 1103, 0, 0, 2 | 8 | 16, 0, 0, xdim - xscreen, ydim);

            if (count <= 2464) {
                if (count > 1440) {
                    renderer.rotatesprite(160 << 16, 176 << 16, (count - 1440) << 6, 0, 1105, 0, 0, 2 | 8);
                }
                if (count > 1952) {
                    renderer.rotatesprite(((177 * EngineUtils.sin((count + 96) & 0x7FF) >> 14) + 104) << 16,
                            ((177 * EngineUtils.sin((count - 1952 + 1536) & 0x7FF) >> 14) + 189) << 16, 65536, 4 * (count - 1952),
                            1107, 0, 0, 2 | 8);
                }
            } else {
                stopmusic();
                renderer.rotatesprite(160 << 16, 176 << 16, 65536, 0, 1105, 0, 0, 2 | 8);
                renderer.rotatesprite(263 << 16, 177 << 16, 65536, 0, 1107, 0, 0, 2 | 8 | 16);
            }

            if (engine.getTotalClock() > 3600) {
                int angle = 8 * (engine.getTotalClock() - 3600);
                int scale = angle << 5;
                if (scale >= 65536) {
                    scale = 65536;
                    angle = 0;
                    toffs = 0;
                    state = 1;
                    time = 0;
                    game.pNet.ResetTimers();
                    startmusic(3);
                }

                renderer.rotatesprite(160 << 16, 100 << 16, scale, angle, 770, 0, 0, 2 | 8, xscreen, 0, xdim - xscreen, ydim);
            }
        }
    }

    private void screen2(float delta) {
        if ((time += delta) >= 0.5f) {
            if (++toffs >= 18) {
                toffs = 0;
            }
            time = 0;
        }
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, 770 + toffs, 0, 0, 2 | 8);

        int yoffs = engine.getTotalClock() >> 5;
        if (engine.getTotalClock() >= 6240) {
            yoffs = 195;
        }

        renderer.rotatesprite(20 << 16, (199 - yoffs) << 16, 65536, 0, 788, klabs(13 - (engine.getTotalClock() >> 8)), 0, 2 | 8 | 16);
        if (engine.getTotalClock() >= 7680) {
            toffs = 0;
            state = 2;
            game.pNet.ResetTimers();
        }
    }

    private void screen3(float delta) {
        if ((time += delta) >= 0.005f) {
            if (++toffs > 256) {
                toffs = 256;
                skip();
            }
            time = 0;
        }
        Renderer renderer = game.getRenderer();

//		renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, 611, 0, 0, 2 | 8);

        renderer.rotatesprite(0, 0, 65536, 0, 1102, 0, 0, 2 | 8 | 16);
        renderer.rotatesprite(0, 10 << 16, 65536, 0, 1100, 0, 0, 2 | 8 | 16);

        renderer.rotatesprite(160 << 16, 100 << 16, (256 - toffs) << 8, 8 * toffs, 770, 0, 0, 2 | 8);
        renderer.rotatesprite(160 << 16, 100 << 16, (256 - toffs) << 8, 8 * (2048 - toffs), 788, 0, 0, 2 | 8);
    }
}
