// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Screens;

import ru.m210projects.Build.Pattern.ScreenAdapters.MenuAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.LSP.Main;

import static ru.m210projects.LSP.Factory.LSPMenuHandler.MAIN;
import static ru.m210projects.LSP.Sounds.*;


public class MenuScreen extends MenuAdapter {

    public MenuScreen(Main game) {
        super(game, game.menu.mMenus[MAIN]);
    }

    @Override
    public void draw(float delta) {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(0, 0, 65536, 0, 1102, 10, 0, 2 | 8 | 16);
        renderer.rotatesprite(0, 10 << 16, 65536, 0, 1100, 10, 0, 2 | 8 | 16);
    }

    @Override
    public void show() {
        stopallsounds();
        startmusic(13);
    }

    @Override
    public void pause() {
        stopmusic();
    }

    @Override
    public void resume() {
        startmusic(13);
    }

}
