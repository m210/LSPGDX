// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Fonts;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.LSP.GdxResources.MENUFONT;
import static ru.m210projects.LSP.Main.engine;

public class MenuFont extends Font {

    public MenuFont(Engine draw) {
        this.size = draw.getTile(MENUFONT).getHeight() / 2;

        this.addCharInfo(' ', new CharInfo(this, -1, 0.5f, 16));

        for (int i = 0; i < 26; i++) {
            int nTile = i + MENUFONT + 1;
            addChar((char) ('A' + i), nTile);
            addChar((char) ('a' + i), nTile);
        }

        for (int i = 0; i < 10; i++) {
            int nTile = i + MENUFONT + 27;
            addChar((char) ('0' + i), nTile);
        }

        this.addChar('&', MENUFONT);
    }

    protected void addChar(char ch, int nTile) {
        int width = engine.getTile(nTile).getWidth();
        this.addCharInfo(ch, new CharInfo(this, nTile, 0.5f, width, width, 0, 0));
    }
}
