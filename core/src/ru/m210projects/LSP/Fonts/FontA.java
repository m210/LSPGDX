// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Fonts;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.font.AtlasCharInfo;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;

public class FontA extends Font {

    public FontA(final Engine draw) {
        this.size = 8;

        final int cols = 16;
        final int rows = 8;
        ArtEntry pic = draw.getTile(628);

        for (char ch = 0; ch < cols * rows; ch++) {
            this.addCharInfo(ch, new AtlasCharInfo(this, ch, 628, pic.getWidth(), pic.getHeight(), cols, rows));
        }

        setVerticalScaled(false);
    }
}
