package ru.m210projects.LSP.Menus;

import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuText;
import ru.m210projects.Build.Pattern.MenuItems.MenuVariants;
import ru.m210projects.LSP.Main;

import static ru.m210projects.LSP.LoadSave.lastload;
import static ru.m210projects.LSP.LoadSave.loadgame;
import static ru.m210projects.LSP.Main.gLoadingScreen;

public class MenuLastLoad extends BuildMenu {

    public MenuLastLoad(final Main game) {
    super(game.pMenu);
        MenuText QuitQuestion = new MenuText("Load Saved game?", game.getFont(0), 160, 90, 1);
        QuitQuestion.pal = 228;
        addItem(QuitQuestion, false);

        MenuVariants question = new MenuVariants(game.pEngine, "[Y/N]",
                game.getFont(0), 160, 105) {
            @Override
            public void positive(MenuHandler menu) {
                game.changeScreen(gLoadingScreen.setTitle(lastload.getName()));
                gLoadingScreen.init(() -> {
                    if (!loadgame(lastload)) {
                        game.GameMessage("Can't load game!");
                    }
                });
                menu.mClose();
            }

            @Override
            public void negative(MenuHandler menu) {
                menu.mClose();
            }
        };
        question.pal = 228;

        addItem(question, true);
    }
}
