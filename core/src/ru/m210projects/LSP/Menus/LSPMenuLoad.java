// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Menus;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuLoadSave;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.LSP.Screens.PrecacheScreen;
import ru.m210projects.LSP.Types.DemoFile;
import ru.m210projects.LSP.Types.PlayerStruct;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Globals.mapnum;
import static ru.m210projects.LSP.LoadSave.*;
import static ru.m210projects.LSP.Main.*;

public class LSPMenuLoad extends MenuLoadSave {

    public LSPMenuLoad(final BuildGame app) {
        super(app, app.getFont(0), 65, 50, 185, 250, 11, 0, 8, 611, (handler, pItem) -> {
            final MenuSlotList item = (MenuSlotList) pItem;
            if (canLoad(item.getFileEntry()) && (!app.isCurrentScreen(gLoadingScreen)
                    && !(app.getScreen() instanceof PrecacheScreen))) {
                app.changeScreen(gLoadingScreen);
                gLoadingScreen.init(() -> {
                    if (!loadgame(item.getFileEntry())) {
                        app.setPrevScreen();
                        if (app.isCurrentScreen(gGameScreen)) {
                            app.pNet.ready2send = true;
                        }
                    } else if (item.getFileEntry().getName().equalsIgnoreCase("autosave.sav") && gDemoScreen.isRecordEnabled()) {
                        gDemoScreen.onStopPlaying();
                        PlayerStruct pp = gPlayer[myconnectindex];
                        DemoFile.PlayerDemoData demoData = new DemoFile.PlayerDemoData();
                        demoData.setFrom(pp);
                        pp.reset();
                        demoData.applyTo(pp);
                        gGameScreen.changemap(mapnum);
                    }
                });
            }
        }, false);
    }

    @Override
    public boolean loadData(FileEntry filename) {
        return lsReadLoadData(filename) != -1;
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        return null;
    }

    @Override
    public MenuPicnum getPicnum(Engine draw, int x, int y) {
        return new MenuPicnum(draw, x - 66, y - 3, 611, 611, 0x9500) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = game.getRenderer();
                if (nTile != defTile) {
                    renderer.rotatesprite(x << 16, y << 16, 2 * nScale, 0, nTile, 0, 0, 10 | 16);
                } else {
                    renderer.rotatesprite(x << 16, y << 16, nScale, 0, nTile, 0, 0, 10 | 16);
                }
            }
        };
    }

    @Override
    public MenuText getInfo(BuildGame app, int x, int y) {
        return new MenuText(lsInf.info, app.getFont(1), x - 58, y + 102, 0) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = handler.getRenderer();
                int ty = y;
                if (lsInf.date != null && !lsInf.date.isEmpty()) {
                    font.drawTextScaled(renderer, x, ty, lsInf.date, 1.0f, -128, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                    ty -= 7;
                }
                if (lsInf.info != null) {
                    font.drawTextScaled(renderer, x, ty, lsInf.info, 1.0f, -128, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                }
            }
        };
    }

}
