// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Menus;

import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler.MenuOpt;
import ru.m210projects.Build.Pattern.MenuItems.MenuItem;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.LSP.Types.PCXFile;

import static ru.m210projects.LSP.Globals.ANIM_PAL;
import static ru.m210projects.LSP.Globals.TILE_ANIM;
import static ru.m210projects.LSP.Main.*;

public class ItemPCX extends MenuItem {

    private PCXFile fil;
    private final byte index;

    public ItemPCX(int x, int y, int index) {
        super(null, null);
        this.x = x;
        this.y = y;
        this.flags |= 11;
        this.index = (byte) index;
    }

    @Override
    public void draw(MenuHandler handler) {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(x << 16, y << 16, 65536, 512, TILE_ANIM, -128, ANIM_PAL, 4 | 10 | 16 | 64);
    }

    @Override
    public boolean callback(MenuHandler handler, MenuOpt opt) {
        switch (opt) {
            case LEFT:
            case BSPACE:
            case RMB:
                m_pMenu.mNavUp();
                return false;
            case RIGHT:
            case ENTER:
            case SPACE:
            case LMB:
                m_pMenu.mNavDown();
                return false;
            case UP:
            case DW:
            case ESC:
            case DELETE:
                return m_pMenu.mNavigation(opt);
            default:
                return false;
        }
    }

    public void show() {
        if (fil == null) {
            try {
                fil = new PCXFile(game.getCache().getEntry(Integer.toString(index & 0xFF), false));
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }

        engine.getPaletteManager().changePalette(fil.getPalette());

        ArtEntry pic = engine.getTile(TILE_ANIM);
        if (!(pic instanceof DynamicArtEntry) || !pic.exists() || pic.getWidth() != fil.getWidth() || pic.getHeight() != fil.getHeight()) {
            pic = engine.allocatepermanenttile(TILE_ANIM, fil.getHeight(), fil.getWidth());
        }
        ((DynamicArtEntry) pic).copyData(fil.getData());
    }

    @Override
    public void open() {
    }

    @Override
    public void close() {
    }

    @Override
    public boolean mouseAction(int x, int y) {
        return false;
    }

}
