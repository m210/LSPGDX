// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Menus;

import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.LSP.Main;

import static ru.m210projects.Build.Engine.show2dsector;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.*;

public class MenuGameSet extends BuildMenu {

    public MenuGameSet(Main app) {
        super(app.pMenu);
        int pos = 50;

        MenuSwitch sAutoload = new MenuSwitch("Autoload folder", app.getFont(0), 25, pos += 12, 280, cfg.isAutoloadFolder(), (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.setAutoloadFolder(sw.value);
        }, "Enabled", "Disabled");

        MenuSwitch sBob = new MenuSwitch("Head bob:", app.getFont(0), 25, pos += 12, 280, cfg.bHeadBob, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.bHeadBob = sw.value;
        }, null, null);

        MenuConteiner sOverlay = new MenuConteiner("Overlay map:", app.getFont(0), 25, pos += 12, 280, null, 0, (handler, pItem) -> {
            MenuConteiner item = (MenuConteiner) pItem;
            cfg.gOverlayMap = item.num;
        }) {

            @Override
            public void open() {
                if (this.list == null) {
                    this.list = new char[3][];
                    this.list[0] = "Full only".toCharArray();
                    this.list[1] = "Overlay only".toCharArray();
                    this.list[2] = "Full and overlay".toCharArray();
                }
                num = cfg.gOverlayMap;
            }
        };

        MenuSwitch sSpamProjs = new MenuSwitch("Enemy attack spam:", app.getFont(0), 25, pos += 12, 280, cfg.bOriginal,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.bOriginal = sw.value;
                }, null, null);

        MenuSwitch sShowExits = new MenuSwitch("Show exits on automap", app.getFont(0), 25, pos += 12, 280, true, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.bShowExit = sw.value;

            if (boardService.getBoard() != null) { // #GDX 31.12.2024 Check if in a game
                for (int i = 0; i < boardService.getSectorCount(); i++) {
                    Sector sec = boardService.getSector(i);
                    if (sec == null) {
                        continue;
                    }

                    switch (sec.getLotag()) {
                        case 99:
                        case 98:
                        case 97:
                            if (cfg.bShowExit) {
                                show2dsector.setBit(i);
                            } else {
                                show2dsector.clearBit(i);
                            }
                            break;
                    }
                }
            }
        }, null, null) {
            @Override
            public void open() {
                value = cfg.bShowExit;
            }
        };

        MenuConteiner mPlayingDemo = new MenuConteiner("Demos playback:", app.getFont(0), 25, pos += 12, 280, null, 0,
                (handler, pItem) -> {
                    MenuConteiner item = (MenuConteiner) pItem;
                    cfg.gDemoSeq = item.num;
                }) {
            @Override
            public void open() {
                if (this.list == null) {
                    this.list = new char[3][];
                    this.list[0] = "Off".toCharArray();
                    this.list[1] = "In order".toCharArray();
                    this.list[2] = "Randomly".toCharArray();
                }
                num = cfg.gDemoSeq;
            }
        };

        MenuSwitch sRecord = new MenuSwitch("Record demo:", app.getFont(0), 25, pos += 12, 280, m_recstat == DEMOSTAT_RECORD,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    m_recstat = sw.value ? DEMOSTAT_RECORD : DEMOSTAT_NULL;
                }, null, null) {

            @Override
            public void open() {
                value = gDemoScreen.isRecordEnabled();
                mCheckEnableItem(!app.isCurrentScreen(gGameScreen));
            }
        };


        MenuSwitch mTimer = new MenuSwitch("Game loop timer:", app.getFont(0), 25, pos + 12, 280, cfg.isLegacyTimer(),
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.setLegacyTimer(sw.value);
                    engine.inittimer(cfg.isLegacyTimer(),  120, TICSPERFRAME);
                }, "Legacy", "Gdx") {
        };

        addItem(sAutoload, true);
        addItem(sBob, false);
        addItem(sOverlay, false);
        addItem(sSpamProjs, false);
        addItem(sShowExits, false);
        addItem(mPlayingDemo, false);
        addItem(sRecord, false);
        addItem(mTimer, false);
    }
}
