// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Menus;

import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuFileBrowser;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.LSP.Main;

public class LSPMenuUserContent extends BuildMenu {

    public LSPMenuUserContent(final Main app) {
        super(app.pMenu);
        int width = 240;
        MenuFileBrowser list = new MenuFileBrowser(app, app.getFont(2), app.getFont(0), app.getFont(2), 40, 55, width, 1, 17, 611) {
            @Override
            protected void drawHeader(Renderer renderer, int x1, int x2, int y) {
                /*directories*/
                app.getFont(1).drawTextScaled(renderer, x1, y, dirs, 32768, -32, topPal, TextAlign.Left, Transparent.None, ConvertType.Normal, fontShadow);
                /*files*/
                app.getFont(1).drawTextScaled(renderer, x2 + 13, y, ffs, 32768, -32, topPal, TextAlign.Left, Transparent.None, ConvertType.Normal, fontShadow);
            }

            @Override
            public void init() {
                registerExtension("map", 0, 0);
            }

            @Override
            public void handleDirectory(Directory dir) {
                /* nothing */
            }

            @Override
            public void handleFile(FileEntry file) {
                if (file.isExtension("map")) {
                    addFile(file);
                }
            }

            @Override
            public void invoke(FileEntry fil) {
                if (fil.getExtension().equals("MAP")) {
                    app.pMenu.mClose();
                }
            }
        };

        list.topPal = 20;
        addItem(list, true);
    }


}
