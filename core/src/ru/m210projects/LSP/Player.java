// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.LSP.Factory.LSPRenderer;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClampAngle;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.LSP.Factory.LSPMenuHandler.AUDIOSET;
import static ru.m210projects.LSP.Factory.LSPMenuHandler.LASTSAVE;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.LoadSave.lastload;
import static ru.m210projects.LSP.Main.*;
import static ru.m210projects.LSP.Sectors.operatesector;
import static ru.m210projects.LSP.Sounds.playsound;
import static ru.m210projects.LSP.Sprites.changehealth;
import static ru.m210projects.LSP.Sprites.operatesprite;
import static ru.m210projects.LSP.Weapons.*;

public class Player {

    private static int word_5174F;
    private static final int[] byte_58A94 = new int[MAXPLAYERS];

    public static void InitPlayer(int plr, BuildPos startPos) {
        int spr = engine.insertsprite(startPos.sectnum, INANIMATE);
        gPlayer[plr].nSprite = spr;

        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite != null) {
            pSprite.setX(startPos.x);
            pSprite.setY(startPos.y);
            pSprite.setZ(startPos.z + 0x2000);
            pSprite.setCstat(1);
            pSprite.setPicnum(PLAYER);
            pSprite.setShade(0);
            pSprite.setXrepeat(64);
            pSprite.setYrepeat(64);
            pSprite.setAng(startPos.ang);
            pSprite.setXvel(0);
            pSprite.setYvel(0);
            pSprite.setZvel(0);
            pSprite.setOwner((short) (plr * 4096));
            pSprite.setLotag(0);
            gPlayer[plr].nNewWeapon = 999;
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isonwater(int snum) {
        Sector sec = boardService.getSector(gPlayer[snum].sectnum);
        return sec != null
                && (sec.getLotag() == 4 || sec.getLotag() == 5)
                && klabs(gPlayer[snum].z - sec.getFloorz()) <= 8192;
    }

    public static void processinput(int snum) {
        Sector plrSec = boardService.getSector(gPlayer[snum].sectnum);
        Sprite plrSpr = boardService.getSprite(gPlayer[snum].nSprite);
        if (plrSpr == null) {
            return;
        }

        if (gPlayer[snum].nHealth <= 0) {
            if (gPlayer[snum].horiz < 300) {
                gPlayer[snum].horiz += 4;
            }

            if (plrSec != null && (plrSec.getFloorz() - 1024) > gPlayer[snum].z) {
                gPlayer[snum].z += 256;
            }

            if (gPlayer[snum].horiz >= 200 && gPlayer[snum].nPlayerStatus != 1) {
                gPlayer[snum].nPlayerStatus = 1;
                gPlayer[snum].nWeaponState = 0;

                if (lastload != null && lastload.exists()) {
                    game.menu.mOpen(game.menu.mMenus[LASTSAVE], -1);
                }
                playsound(91);
            }

            if (game.isCurrentScreen(gGameScreen)) {
                if (game.getProcessor().isKeyPressed(Input.Keys.ENTER) || (gPlayer[snum].pInput.bits & 0x400) != 0) {
                    gPlayer[snum].nWeapon = nPlayerFirstWeapon;
                    gPlayer[snum].nLastChoosedWeapon = 6;
                    gPlayer[snum].nLastManaWeapon = 13;
                    gPlayer[snum].nLastWeapon = 1;
                    gGameScreen.newgame(mapnum);
                }
            }

            return;
        }

        if (nKickSprite != -1) {
            if (lockclock - nKickClock > 300) {
                playsound(93, nKickSprite);
                nKickClock = lockclock;
            }
        }

//		if (gPlayer[snum].nWeaponImpact != 0) {
//			int force = 3 * TICSPERFRAME * gPlayer[snum].nWeaponImpact;
//			xvect = mulscale(EngineUtils.sin(((int) gPlayer[snum].ang + 512) & 0x7FF), force, 4);
//			yvect = mulscale(EngineUtils.sin(((int) gPlayer[snum].ang) & 0x7FF), force, 4);
//
//			engine.clipmove(gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, gPlayer[snum].sectnum, xvect, yvect, 128,
//					1024, 1024, CLIPMASK0);
//			if (clipmove_sectnum != -1) {
//				gPlayer[snum].x = clipmove_x;
//				gPlayer[snum].y = clipmove_y;
//				gPlayer[snum].z = clipmove_z;
//				gPlayer[snum].sectnum = clipmove_sectnum;
//			}
//			gPlayer[snum].nWeaponImpact = 0;
//		} else

        if ((gPlayer[snum].pInput.xvel | gPlayer[snum].pInput.yvel | gPlayer[snum].nWeaponImpact) != 0) {
            int xvect = gPlayer[snum].pInput.xvel;
            int yvect = gPlayer[snum].pInput.yvel;

            if (gPlayer[snum].nWeaponImpact != 0) {
                int force = 3 * TICSPERFRAME * gPlayer[snum].nWeaponImpact;
                xvect += mulscale(EngineUtils.sin((gPlayer[snum].nWeaponImpactAngle + 512) & 0x7FF), force, 4);
                yvect += mulscale(EngineUtils.sin(gPlayer[snum].nWeaponImpactAngle & 0x7FF), force, 4);
                gPlayer[snum].nWeaponImpact = 0;
            }

            if (gPlayer[snum].noclip) {
                gPlayer[snum].x += xvect >> 14;
                gPlayer[snum].y += yvect >> 14;
                int sect = engine.updatesector(gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].sectnum);
                if (sect != -1) {
                    gPlayer[snum].sectnum = sect;
                    plrSec = boardService.getSector(sect);
                }
                engine.changespritesect(gPlayer[snum].nSprite, plrSpr.getSectnum());
            } else {
                engine.clipmove(gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, gPlayer[snum].sectnum, xvect, yvect,
                        128, 1024, 1024, CLIPMASK0);
                if (clipmove_sectnum != -1) {
                    gPlayer[snum].x = clipmove_x;
                    gPlayer[snum].y = clipmove_y;
                    gPlayer[snum].z = clipmove_z;
                    gPlayer[snum].sectnum = clipmove_sectnum;
                    plrSec = boardService.getSector(gPlayer[snum].sectnum);
                }
            }
        }

        if (!gPlayer[snum].noclip) {
            int sect = gPlayer[snum].sectnum;
            engine.pushmove(gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, sect, 128, 1024, 1024, CLIPMASK0);
            gPlayer[snum].x = pushmove_x;
            gPlayer[snum].y = pushmove_y;
            gPlayer[snum].z = pushmove_z;
            if (sect != pushmove_sectnum && pushmove_sectnum != -1) {
                gPlayer[snum].sectnum = pushmove_sectnum;
                engine.changespritesect(gPlayer[snum].nSprite, pushmove_sectnum);
                plrSec = boardService.getSector(gPlayer[snum].sectnum);
            }
        }

        if (gPlayer[snum].pInput.angvel != 0) {
            gPlayer[snum].ang += gPlayer[snum].pInput.angvel * TICSPERFRAME / 16.0f;
            gPlayer[snum].ang = BClampAngle(gPlayer[snum].ang);
        }

        short ocstat = plrSpr.getCstat();
        plrSpr.setCstat(plrSpr.getCstat() & ~1);
        engine.getzrange(gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, gPlayer[snum].sectnum, 128, CLIPMASK0);
        int globhiz = zr_ceilz;
        int globloz = zr_florz;
        int globlohit = zr_florhit;
        plrSpr.setCstat(ocstat);

        if (((gPlayer[snum].pInput.bits & 8) > 0) && (gPlayer[snum].horiz > 0)) {
            gPlayer[snum].horiz -= 8; // -
        }
        if (((gPlayer[snum].pInput.bits & 4) > 0) && (gPlayer[snum].horiz < 200)) {
            gPlayer[snum].horiz += 8; // +
        }
        gPlayer[snum].horiz = BClipRange(gPlayer[snum].horiz + gPlayer[snum].pInput.horiz, 0, 200);

        int word_51759 = 0;

        int goalz = globloz - 0x2000;
        // kens slime sector
        if (plrSec != null
                && (plrSec.getLotag() == 4 || plrSec.getLotag() == 5)) {
            int v23 = lockclock - gPlayer[snum].nSlimeDamageCount;
            if (v23 > 120) {
                if (plrSec.getLotag() == 4 && gPlayer[snum].nHealth > 0) {
                    changehealth(snum, -3);
                }
                gPlayer[snum].nSlimeDamageCount = lockclock;
            }
            int dword_51755 = v23 >> 5;
            if (dword_51755 > 2) {
                dword_51755 = 1;
            }
            word_51759 = 1;

            // if not on a sprite
            if ((globlohit & HIT_TYPE_MASK) != HIT_SPRITE) {
                goalz = globloz - ((dword_51755 + 8) << 8);
                if (gPlayer[snum].z >= goalz - 512) {
                    engine.clipmove(gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z, gPlayer[snum].sectnum,
                            -TICSPERFRAME << 14, -TICSPERFRAME << 14, 128, 4 << 8, 4 << 8, CLIPMASK0);
                    if (clipmove_sectnum != -1) {
                        gPlayer[snum].x = clipmove_x;
                        gPlayer[snum].y = clipmove_y;
                        gPlayer[snum].z = clipmove_z;
                        gPlayer[snum].sectnum = clipmove_sectnum;
                        plrSec = boardService.getSector(gPlayer[snum].sectnum);
                    }
                }
            }
        } else {
            gPlayer[snum].nSlimeDamageCount = lockclock;
        }

        if (goalz < globhiz + 4096) {
            goalz = (globloz + globhiz) >> 1;
        }

        if ((gPlayer[snum].pInput.bits & 1) != 0) { // jump
            if (gPlayer[snum].z >= globloz - 8192) {
                goalz -= (48 << 8);
            }
        } else if ((gPlayer[snum].pInput.bits & 2) != 0) { // crouch
            if (plrSec != null && gPlayer[snum].z < (plrSec.getFloorz() - 2048)) {
                goalz += (12 << 8);
            }
        }

        // player is on a groudraw area
        if (plrSec != null && (plrSec.getFloorstat() & 2) > 0) {
            ArtEntry pic = engine.getTile(plrSec.getFloorpicnum());
            if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
                pic = engine.allocatepermanenttile(pic);
                if (pic.hasSize()) {
                    goalz -= ((pic.getBytes()[0] + (((gPlayer[snum].x >> 4) & 63) << 6) + ((gPlayer[snum].y >> 4) & 63)) << 8);
                }
            }
        }

        if (goalz != gPlayer[snum].z) {
//			if (cfgParameter1[3] != 0) {
            if (goalz > gPlayer[snum].z) {
                gPlayer[snum].hvel += TICSPERFRAME << 6;
            }

            if (word_51759 != 0) {
                if (goalz < gPlayer[snum].z) {
                    gPlayer[snum].hvel += TICSPERFRAME * (goalz - gPlayer[snum].z) >> 5;
                }
            } else {
                if (goalz < gPlayer[snum].z) {
                    gPlayer[snum].hvel = TICSPERFRAME * (goalz - gPlayer[snum].z) >> 5;
                }
            }

            gPlayer[snum].z += gPlayer[snum].hvel;
            if (globloz - 1024 < gPlayer[snum].z) {
                gPlayer[snum].z = globloz - 1024;
                gPlayer[snum].hvel = 0;
            }
            if (globhiz + 1024 > gPlayer[snum].z) {
                gPlayer[snum].z = globhiz + 1024;
                gPlayer[snum].hvel = 0;
            }
//			} else
//				gPlayer[snum].z = goalz;
        }

        if (gPlayer[snum].gViewMode == 2) {
            if ((gPlayer[snum].pInput.bits & 0x20) > 0 && gPlayer[snum].zoom > 48) {
                gPlayer[snum].zoom -= gPlayer[snum].zoom >> 4;
            }

            if ((gPlayer[snum].pInput.bits & 0x10) > 0 && gPlayer[snum].zoom < 4096) {
                gPlayer[snum].zoom += gPlayer[snum].zoom >> 4;
            }
        }

        engine.setsprite(gPlayer[snum].nSprite, gPlayer[snum].x, gPlayer[snum].y, gPlayer[snum].z + 0x2000);
        plrSpr.setAng((short) gPlayer[snum].ang);

        if (plrSec == null || globhiz + 2048 > globloz) {
//			sndPlaySound("ouch.wav", sub_1D021(gPlayer[snum].x, gPlayer[snum].y));
            changehealth(snum, -TICSPERFRAME);
        }

        if ((gPlayer[snum].pInput.bits & 0x400) != 0) { // USE
            if (plrSec != null && plrSec.getHitag() == 0 && (plrSec.getLotag() == 20 || plrSec.getLotag() == 21)) {
                operatesector(gPlayer[snum].sectnum);
            }

            engine.neartag(gPlayer[snum].x, gPlayer[snum].y, (gPlayer[snum].z + (8 << 8)), gPlayer[snum].sectnum,
                    (short) gPlayer[snum].ang, neartag, 1024, 3);

            // kens water fountain
            Wall tagWall = boardService.getWall(neartag.tagwall);
            if (tagWall != null) {
                if (mapnum == 0) {
                    if (tagWall.getLotag() == 102) // sound setup
                    {
                        game.menu.mOpen(game.menu.mMenus[AUDIOSET], -1);
                    }
                    if (tagWall.getLotag() == 103) // music setup
                    {
                        game.menu.mOpen(game.menu.mMenus[AUDIOSET], -1);
                    }

                    if (tagWall.getLotag() == 101) {
                        if (tagWall.getPicnum() >= 830 && tagWall.getPicnum() <= 837) // choose player
                        {
                            Wall oldChooseWall = boardService.getWall(oldchoose);
                            if (oldChooseWall != null) {
                                oldChooseWall.setShade(0);
                                oldChooseWall.setPicnum(oldpic);
                            }

                            tagWall.setShade(-10);
                            oldchoose = neartag.tagwall;
                            oldpic = tagWall.getPicnum();
                            tagWall.setPicnum(tagWall.getPicnum() + 710);
                            Wall wal = boardService.getWall(nDiffDoor);
                            if (wal != null) {
                               wal.setCstat(0);
                            }
                            wal = boardService.getWall(nDiffDoorBack);
                            if (wal != null) {
                                wal.setCstat(0);
                            }
                            playsound(38);
                        }

                        if (tagWall.getPicnum() >= 805 && tagWall.getPicnum() <= 807) // choose difficult
                        {
                            if (tagWall.getPicnum() == 805) {
                                nDifficult = 1;
                            }
                            if (tagWall.getPicnum() == 806) {
                                nDifficult = 2;
                            }
                            if (tagWall.getPicnum() == 807) {
                                nDifficult = 3;
                            }
                            playsound(41);

                            Gdx.app.postRunnable(() -> {
                                int nPlayersWeapon = 17;

                                switch (oldpic) {
                                    case 831:
                                        nPlayersWeapon = 18;
                                        break;
                                    case 832:
                                        nPlayersWeapon = 19;
                                        break;
                                    case 833:
                                        nPlayersWeapon = 20;
                                        break;
                                    case 834:
                                        nPlayersWeapon = 21;
                                        break;
                                    case 835:
                                        nPlayersWeapon = 22;
                                        break;
                                    case 836:
                                        nPlayersWeapon = 23;
                                        break;
                                    case 837:
                                        nPlayersWeapon = 24;
                                        break;
                                    default:
                                        break;
                                }

                                LSPRenderer renderer = game.getRenderer();
                                renderer.setVisibility(15);
                                nPlayerFirstWeapon = (short) nPlayersWeapon;
                                gGameScreen.newgame(1);
                            });
                        }
                    }
                } else {
                    if (tagWall.getOverpicnum() == 54) {
                        tagWall.setOverpicnum(53);
                        waterfountainwall[snum] = neartag.tagwall;
                    } else if (tagWall.getPicnum() == 54) {
                        tagWall.setPicnum(53);
                        waterfountainwall[snum] = neartag.tagwall;
                    }
                    if (waterfountainwall[snum] >= 0) {
                        waterfountaincnt[snum] -= TICSPERFRAME;
                        while (waterfountaincnt[snum] < 0) {
                            waterfountaincnt[snum] += 120;
                            changehealth(snum, 2);
                        }
                    }
                }
            }

            // 1-time triggers
            Sector tagSec = boardService.getSector(neartag.tagsector);
            if (tagSec != null) {
                if (tagSec.getHitag() == 0) {
                    operatesector(neartag.tagsector);
                }
            }

            // tagWall = boardService.getWall(neartag.tagwall);
            if (tagWall != null) {
                if (tagWall.getLotag() == 2) {
                    for (int i = 0; i < boardService.getSectorCount(); i++) {
                        Sector sec = boardService.getSector(i);
                        if (sec != null && sec.getHitag() == tagWall.getHitag()) {
                            if (sec.getLotag() != 1) {
                                operatesector(i);
                            }
                        }
                    }
                    ListNode<Sprite> node = boardService.getStatNode(0);
                    while (node != null) {
                        ListNode<Sprite> nexti = node.getNext();
                        if (node.get().getHitag() == tagWall.getHitag()) {
                            operatesprite(node.getIndex());
                        }
                        node = nexti;
                    }

                    switch (tagWall.getOverpicnum()) {
                        case 730:
                            tagWall.setOverpicnum(731);
                            tagWall.setLotag(0);
                            tagWall.setHitag(0);
                            playsound(20);
                            break;
                        case 732:
                            tagWall.setOverpicnum(733);
                            tagWall.setLotag(0);
                            tagWall.setHitag(0);
                            playsound(25);
                            break;
                        case 734:
                            tagWall.setOverpicnum(735);
                            playsound(87);
                            break;
                        case 735:
                            tagWall.setOverpicnum(734);
                            playsound(87);
                            break;
                        case 736:
                            tagWall.setOverpicnum(737);
                            playsound(85);
                            break;
                        case 737:
                            tagWall.setOverpicnum(736);
                            playsound(85);
                            break;
                    }

                }
            }
        }

        if (mapnum > 0) {
            if ((gPlayer[snum].pInput.bits & 0x800) != 0) {
                weaponfire(snum);
            }

            moveweapons(snum);

            switch (gPlayer[snum].pInput.bits >> 14) {
                case 1:
                case 2:
                case 3:
                    switchweapgroup(snum, (gPlayer[snum].pInput.bits >> 14) & 3);
                    break;
                case 4:
                    prevweapon(snum);
                    break;
                case 5:
                    nextweapon(snum);
                    break;
                case 6:
                    switchweapon(snum, gPlayer[snum].nLastWeapon);
                    break;
            }
        }

        if (word_5174F != 0
                || (klabs(gPlayer[snum].pInput.xvel) >= 65536 || klabs(gPlayer[snum].pInput.yvel) >= 65536)) {
            if (byte_58A94[snum] >= 4) {
                gPlayer[snum].nBobCount = (short) ((gPlayer[snum].nBobCount + 1) & 0xF);
                weaponbobbing(snum);
                if (cfg.bHeadBob) {
                    gPlayer[snum].nBobbing = (8 - Math.abs(8 - gPlayer[snum].nBobCount));
                }
            }
            word_5174F = 0;
            byte_58A94[snum]++;
        } else {
            byte_58A94[snum] = 0;
            gPlayer[snum].nBobCount = 0;
            gPlayer[snum].nBobbing = 0;
        }
    }

}
