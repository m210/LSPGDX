// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.Pattern.Tools.SaveManager;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.LSP.Factory.LSPInput;
import ru.m210projects.LSP.Menus.MenuCorruptGame;
import ru.m210projects.LSP.Types.ANIMATION;
import ru.m210projects.LSP.Types.LSInfo;
import ru.m210projects.LSP.Types.SafeLoader;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.LSP.Animate.*;
import static ru.m210projects.LSP.Enemies.*;
import static ru.m210projects.LSP.Factory.LSPMenuHandler.CORRUPTLOAD;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.*;
import static ru.m210projects.LSP.Quotes.resetQuotes;
import static ru.m210projects.LSP.Quotes.viewSetMessage;
import static ru.m210projects.LSP.Screens.GameScreen.getMap;
import static ru.m210projects.LSP.Sounds.*;
import static ru.m210projects.LSP.Types.ANIMATION.*;

public class LoadSave {

    public static final String savsign = "LSP0";
    public static final int gdxSave = 100;
    public static final int currentGdxSave = 101; // v1.16 100
    public static final int SAVETIME = 8;
    public static final int SAVENAME = 32;
    public static final int SAVESCREENSHOTSIZE = 160 * 100;
    public static final int SAVEGDXDATA = SAVESCREENSHOTSIZE + 144 + 128;
    public static final char[] filenum = new char[4];
    public static boolean gQuickSaving;
    public static boolean gAutosaveRequest;
    public static final LSInfo lsInf = new LSInfo();
    public static FileEntry lastload;
    public static int quickslot = 0;
    public static final SafeLoader loader = new SafeLoader();


    public static void FindSaves(Directory dir) {
        for (Entry file : dir) {
            if (file.isExtension("sav") && file instanceof FileEntry) {
                try (InputStream is = file.getInputStream()) {
                    String signature = StreamUtils.readString(is, 4);
                    if (signature.isEmpty()) {
                        continue;
                    }

                    if (signature.equals(savsign)) {
                        int nVersion = StreamUtils.readShort(is);
                        if (nVersion >= gdxSave) {
                            long time = StreamUtils.readLong(is);
                            String savname = StreamUtils.readString(is, SAVENAME);
                            game.pSavemgr.add(savname, time, (FileEntry) file);
                        }
                    }
                } catch (Exception ignored) {
                }
            }
        }
        game.pSavemgr.sort();
    }

    public static int lsReadLoadData(FileEntry file) {
        if (file.exists()) {
            ArtEntry pic = engine.getTile(SaveManager.Screenshot);
            if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
                pic = engine.allocatepermanenttile(SaveManager.Screenshot, 160, 100);
            }

            try (InputStream is = file.getInputStream()) {
                int nVersion = checkSave(is) & 0xFFFF;
                lsInf.clear();

                if (nVersion == currentGdxSave) {
                    lsInf.date = game.date.getDate(StreamUtils.readLong(is));
                    StreamUtils.skip(is, SAVENAME);

                    lsInf.read(is);
                    if (is.available() <= SAVESCREENSHOTSIZE) {
                        return -1;
                    }

                    StreamUtils.readBytes(is, pic.getBytes(), SAVESCREENSHOTSIZE);
                    ((DynamicArtEntry) pic).invalidate();
                    return 1;
                } else {
                    lsInf.info = "Incompatible ver. " + nVersion + " != " + currentGdxSave;
                    return -1;
                }
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }

        lsInf.clear();
        return -1;
    }

    public static String makeNum(int num) {
        filenum[3] = (char) ((num % 10) + 48);
        filenum[2] = (char) (((num / 10) % 10) + 48);
        filenum[1] = (char) (((num / 100) % 10) + 48);
        filenum[0] = (char) (((num / 1000) % 10) + 48);

        return new String(filenum);
    }

    public static int checkSave(InputStream is) throws IOException {
        String signature = StreamUtils.readString(is, 4);
        if (!signature.equals(savsign)) {
            return 0;
        }

        return StreamUtils.readShort(is);
    }

    public static boolean checkfile(InputStream is) throws IOException {
        int nVersion = checkSave(is);

        if (nVersion != currentGdxSave) {
            return false;
        }

        return loader.load(is);
    }

    public static void load() {
        gDemoScreen.onLoad();

        stopallsounds();

        mapnum = loader.mapnum;
        nDifficult = (short) loader.skill;

        LoadGDXBlock();
        LoadStuff();
        MapLoad();
        SectorLoad();
        AnimationLoad();

        screenpeek = myconnectindex;
        engine.getTileManager().setTilesPath(mapnum == 0 ? 0 : 1);

        ListNode<Sprite> next;
        for (ListNode<Sprite> node = boardService.getStatNode(ATTACK); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            engine.changespritestat(node.getIndex(), GUARD);
            spr.setPicnum(spr.getExtra());
        }

        nKickSprite = -1;
        if (mapnum == 0) {
            for (short sec = 0; sec < boardService.getSectorCount(); sec++) {
                for (ListNode<Sprite> node = boardService.getSectNode(sec); node != null; node = node.getNext()) {
                    if (node.get().getPicnum() == 51) {
                        nKickSprite = node.getIndex();
                    }
                }
            }
        }

        gPlayer[screenpeek].isWeaponsSwitching = 0;
        gPlayer[screenpeek].nSwitchingClock = 0;

        int mnum = getMap(mapnum).num & 0xFF;
        book = (mnum % 100) % 10;
        chapter = mnum / 100;
        verse = (mnum % 100) / 10;

        game.doPrecache(() -> {
            game.nNetMode = NetMode.Single;
            if (mUserFlag != UserFlag.UserMap) {
                startmusic(getMap(mapnum).music - 1);
            } else {
                stopmusic();
            }

            game.changeScreen(gGameScreen);
            ((LSPInput) game.getProcessor()).reset();

            game.gPaused = false;
            engine.getTimer().setTotalClock(lockclock);
            game.pNet.ototalclock = lockclock;

            resetQuotes();
            viewSetMessage("Game loaded");

            System.gc();

            game.pNet.ResetTimers();
            game.pNet.ready2send = true;
        });
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean loadgame(FileEntry fil) {
        if (fil.exists()) {
            try (InputStream is = fil.getInputStream()) {
                Console.out.println("debug: start loadgame()", OsdColor.BLUE);
                boolean status = checkfile(is);
                if (status) {
                    load();
                    if (lastload == null || !lastload.exists()) {
                        lastload = fil;
                    }
                    return true;
                }

                viewSetMessage("Incompatible version of saved game found!");
                return false;
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }

        viewSetMessage("Can't access to file or file not found!");
        return false;
    }

    public static void LoadGDXBlock() {
        mUserFlag = UserFlag.None;
    }

    public static void SectorLoad() {
        System.arraycopy(loader.waterfountainwall, 0, waterfountainwall, 0, loader.waterfountainwall.length);
        System.arraycopy(loader.waterfountaincnt, 0, waterfountaincnt, 0, loader.waterfountaincnt.length);

        ypanningwallcnt = loader.ypanningwallcnt;
        System.arraycopy(loader.ypanningwalllist, 0, ypanningwalllist, 0, loader.ypanningwalllist.length);
        floorpanningcnt = loader.floorpanningcnt;
        System.arraycopy(loader.floorpanninglist, 0, floorpanninglist, 0, loader.floorpanninglist.length);

        warpsectorcnt = loader.warpsectorcnt;
        System.arraycopy(loader.warpsectorlist, 0, warpsectorlist, 0, loader.warpsectorlist.length);
        xpanningsectorcnt = loader.xpanningsectorcnt;
        System.arraycopy(loader.xpanningsectorlist, 0, xpanningsectorlist, 0, loader.xpanningsectorlist.length);
        warpsector2cnt = loader.warpsector2cnt;
        System.arraycopy(loader.warpsector2list, 0, warpsector2list, 0, loader.warpsector2list.length);
        subwaytrackcnt = loader.subwaytrackcnt;
        for (int i = 0; i < 5; i++) {
            System.arraycopy(loader.subwaytracksector[i], 0, subwaytracksector[i], 0,
                    loader.subwaytracksector[i].length);
        }
        System.arraycopy(loader.subwaynumsectors, 0, subwaynumsectors, 0, loader.subwaynumsectors.length);
        for (int i = 0; i < 5; i++) {
            System.arraycopy(loader.subwaystop[i], 0, subwaystop[i], 0, loader.subwaystop[i].length);
        }
        System.arraycopy(loader.subwaystopcnt, 0, subwaystopcnt, 0, loader.subwaystopcnt.length);
        System.arraycopy(loader.subwaytrackx1, 0, subwaytrackx1, 0, loader.subwaytrackx1.length);
        System.arraycopy(loader.subwaytracky1, 0, subwaytracky1, 0, loader.subwaytracky1.length);
        System.arraycopy(loader.subwaytrackx2, 0, subwaytrackx2, 0, loader.subwaytrackx2.length);
        System.arraycopy(loader.subwaytracky2, 0, subwaytracky2, 0, loader.subwaytracky2.length);
        System.arraycopy(loader.subwayx, 0, subwayx, 0, loader.subwayx.length);
        System.arraycopy(loader.subwaygoalstop, 0, subwaygoalstop, 0, loader.subwaygoalstop.length);
        System.arraycopy(loader.subwayvel, 0, subwayvel, 0, loader.subwayvel.length);
        System.arraycopy(loader.subwaypausetime, 0, subwaypausetime, 0, loader.subwaypausetime.length);

        revolvecnt = loader.revolvecnt;
        System.arraycopy(loader.revolvesector, 0, revolvesector, 0, loader.revolvesector.length);
        System.arraycopy(loader.revolveang, 0, revolveang, 0, loader.revolveang.length);
        for (int i = 0; i < 4; i++) {
            System.arraycopy(loader.revolvex[i], 0, revolvex[i], 0, loader.revolvex[i].length);
        }
        for (int i = 0; i < 4; i++) {
            System.arraycopy(loader.revolvey[i], 0, revolvey[i], 0, loader.revolvey[i].length);
        }
        System.arraycopy(loader.revolvepivotx, 0, revolvepivotx, 0, loader.revolvepivotx.length);
        System.arraycopy(loader.revolvepivoty, 0, revolvepivoty, 0, loader.revolvepivoty.length);

        swingcnt = loader.swingcnt;
        for (int i = 0; i < MAXSWINGDOORS; i++) {
            swingdoor[i].copy(loader.swingdoor[i]);
        }

        dragsectorcnt = loader.dragsectorcnt;
        System.arraycopy(loader.dragsectorlist, 0, dragsectorlist, 0, loader.dragsectorlist.length);
        System.arraycopy(loader.dragxdir, 0, dragxdir, 0, loader.dragxdir.length);
        System.arraycopy(loader.dragydir, 0, dragydir, 0, loader.dragydir.length);
        System.arraycopy(loader.dragx1, 0, dragx1, 0, loader.dragx1.length);
        System.arraycopy(loader.dragy1, 0, dragy1, 0, loader.dragy1.length);
        System.arraycopy(loader.dragx2, 0, dragx2, 0, loader.dragx2.length);
        System.arraycopy(loader.dragy2, 0, dragy2, 0, loader.dragy2.length);
        System.arraycopy(loader.dragfloorz, 0, dragfloorz, 0, loader.dragfloorz.length);
    }

    public static void LoadStuff() {
        for (int i = 0; i < MAXSPRITES; i++) {
            gEnemyClock[i] = loader.gEnemyClock[i];
            gMoveStatus[i] = loader.gMoveStatus[i];
        }

        for (int i = 0; i < 6; i++) {
            nKills[i] = loader.nKills[i];
            nTotalKills[i] = loader.nTotalKills[i];
        }

        nEnemyKills = loader.nEnemyKills;
        nEnemyMax = loader.nEnemyMax;

        nDiffDoor = loader.nDiffDoor;
        nDiffDoorBack = loader.nDiffDoorBack;
        nTrainWall = loader.nTrainWall;
        bActiveTrain = loader.bActiveTrain;
        bTrainSoundSwitch = loader.bTrainSoundSwitch;
        lockclock = loader.lockclock;
        totalmoves = loader.totalmoves;

        visibility = loader.visibility;
        engine.srand(loader.randomseed);

        show2dsector.copy(loader.show2dsector);
        show2dwall.copy(loader.show2dwall);
        show2dsprite.copy(loader.show2dsprite);
        automapping = loader.automapping;

        pskybits = loader.pskybits;
        Renderer renderer = game.getRenderer();
        renderer.setParallaxOffset(256);
        renderer.setParallaxScale(loader.parallaxyscale);
        System.arraycopy(loader.pskyoff, 0, pskyoff, 0, MAXPSKYTILES);
        System.arraycopy(pskyoff, 0, zeropskyoff, 0, MAXPSKYTILES);
        parallaxtype = 0;

        connecthead = loader.connecthead;
        System.arraycopy(loader.connectpoint2, 0, connectpoint2, 0, MAXPLAYERS);

        for (int i = 0; i < MAXPLAYERS; i++) {
            gPlayer[i].copy(loader.plr[i]);
        }

        nPlayerFirstWeapon = loader.nPlayerFirstWeapon;
        oldchoose = loader.oldchoose;
        oldpic = loader.oldpic;
    }

    public static void MapLoad() {
        boardService.setBoard(new Board(null, loader.sector, loader.wall, loader.sprite));
    }

    public static void AnimationLoad() {
        for (int i = 0; i < MAXANIMATES; i++) {
            gAnimationData[i].id = loader.gAnimationData[i].id;
            gAnimationData[i].type = loader.gAnimationData[i].type;
            gAnimationData[i].ptr = loader.gAnimationData[i].ptr;
            gAnimationData[i].goal = loader.gAnimationData[i].goal;
            gAnimationData[i].vel = loader.gAnimationData[i].vel;
            gAnimationData[i].acc = loader.gAnimationData[i].acc;
        }
        gAnimationCount = loader.gAnimationCount;

        for (int i = gAnimationCount - 1; i >= 0; i--) {
            ANIMATION gAnm = gAnimationData[i];
            Object object = (gAnm.ptr = getobject(gAnm.id, gAnm.type));
            switch (gAnm.type) {
                case WALLX:
                case WALLY:
                    game.pInt.setwallinterpolate(gAnm.id, (Wall) object);
                    break;
                case FLOORZ:
                    game.pInt.setfloorinterpolate(gAnm.id, (Sector) object);
                    break;
                case CEILZ:
                    game.pInt.setceilinterpolate(gAnm.id, (Sector) object);
                    break;
            }
        }
    }

    public static boolean canLoad(FileEntry fil) {
        if (fil.exists()) {
            try (InputStream is = fil.getInputStream()) {
                int nVersion = checkSave(is) & 0xFFFF;
                if (nVersion != currentGdxSave) {
                    if (nVersion >= gdxSave) {
                        loader.LoadGDXHeader(is);

                        if (loader.mapnum <= maps.length && loader.skill >= 0 && loader.skill < 4) {
                            MenuCorruptGame menu = (MenuCorruptGame) game.menu.mMenus[CORRUPTLOAD];
                            menu.setRunnable(() -> {
                                nDifficult = (short) loader.skill;
                                gGameScreen.newgame(loader.mapnum);
                            });
                            game.menu.mOpen(menu, -1);
                        }
                    }
                }
                return nVersion == currentGdxSave;
            } catch (Exception ignored) {
            }
        }
        return false;
    }

    public static void quickload() {
        if (numplayers > 1) {
            return;
        }

        final FileEntry loadFile = game.pSavemgr.getLast();
        if (canLoad(loadFile)) {
            game.changeScreen(gLoadingScreen.setTitle(loadFile.getName()));
            gLoadingScreen.init(() -> {
                if (!loadgame(loadFile)) {
                    game.setPrevScreen();
                    if (game.isCurrentScreen(gGameScreen)) {
                        viewSetMessage("Incompatible version of saved game found!");
                        game.pNet.ready2send = true;
                    }
                }
            });
        }
    }

    private static void save(OutputStream fil, String savename, long time) throws IOException {
        SaveHeader(fil, savename, time);
        SaveGDXBlock(fil);
        StuffSave(fil);
        MapSave(fil);
        SectorSave(fil);
        AnimationSave(fil);
    }

    public static void savegame(Directory dir, String savename, String filename) {
        FileEntry file = dir.getEntry(filename);
        if (file.exists()) {
            if (!file.delete()) {
                viewSetMessage("Game not saved. Access denied!");
                return;
            }
        }

        Path path = dir.getPath().resolve(filename);
        try (OutputStream os = new BufferedOutputStream(Files.newOutputStream(path))) {
            long time = game.date.getCurrentDate();
            save(os, savename, time);

            file = dir.addEntry(path);
            if (file.exists()) {
                game.pSavemgr.add(savename, time, file);
                lastload = file;
                viewSetMessage("Game saved");
            } else {
                throw new FileNotFoundException(filename);
            }
        } catch (Exception e) {
            viewSetMessage("Game not saved! " + e);
        }

    }

    public static void SaveHeader(OutputStream os, String savename, long time) throws IOException {
        SaveVersion(os, currentGdxSave);

        StreamUtils.writeLong(os, time);
        StreamUtils.writeString(os, savename, SAVENAME);

        StreamUtils.writeInt(os, mapnum);
        StreamUtils.writeInt(os, nDifficult);
    }

    public static void SaveGDXBlock(OutputStream os) throws IOException {
        ByteBuffer bb = ByteBuffer.allocate(SAVEGDXDATA);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.put(gGameScreen.captBuffer);
        gGameScreen.captBuffer = null;

        StreamUtils.writeBytes(os, bb.array());
    }

    public static void SectorSave(OutputStream os) throws IOException {
        for (int i = 0; i < MAXPLAYERS; i++) {
            StreamUtils.writeShort(os, waterfountainwall[i]);
        }
        for (int i = 0; i < MAXPLAYERS; i++) {
            StreamUtils.writeShort(os, waterfountaincnt[i]);
        }

        StreamUtils.writeShort(os, ypanningwallcnt);
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, ypanningwalllist[i]);
        }
        StreamUtils.writeShort(os, floorpanningcnt);
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, floorpanninglist[i]);
        }
        StreamUtils.writeShort(os, warpsectorcnt);
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, warpsectorlist[i]);
        }
        StreamUtils.writeShort(os, xpanningsectorcnt);
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, xpanningsectorlist[i]);
        }
        StreamUtils.writeShort(os, warpsector2cnt);
        for (int i = 0; i < 32; i++) {
            StreamUtils.writeShort(os, warpsector2list[i]);
        }

        StreamUtils.writeShort(os, subwaytrackcnt);
        for (int a = 0; a < 5; ++a) {
            for (int b = 0; b < 128; ++b) {
                StreamUtils.writeShort(os, subwaytracksector[a][b]);
            }
        }
        for (int i = 0; i < 5; i++) {
            StreamUtils.writeShort(os, subwaynumsectors[i]);
        }

        for (int a = 0; a < 5; ++a) {
            for (int b = 0; b < 8; ++b) {
                StreamUtils.writeInt(os, subwaystop[a][b]);
            }
        }
        for (int i = 0; i < 5; i++) {
            StreamUtils.writeInt(os, subwaystopcnt[i]);
        }
        for (int i = 0; i < 5; i++) {
            StreamUtils.writeInt(os, subwaytrackx1[i]);
        }
        for (int i = 0; i < 5; i++) {
            StreamUtils.writeInt(os, subwaytracky1[i]);
        }
        for (int i = 0; i < 5; i++) {
            StreamUtils.writeInt(os, subwaytrackx2[i]);
        }
        for (int i = 0; i < 5; i++) {
            StreamUtils.writeInt(os, subwaytracky2[i]);
        }
        for (int i = 0; i < 5; i++) {
            StreamUtils.writeInt(os, subwayx[i]);
        }
        for (int i = 0; i < 5; i++) {
            StreamUtils.writeInt(os, subwaygoalstop[i]);
        }
        for (int i = 0; i < 5; i++) {
            StreamUtils.writeInt(os, subwayvel[i]);
        }
        for (int i = 0; i < 5; i++) {
            StreamUtils.writeInt(os, subwaypausetime[i]);
        }

        StreamUtils.writeShort(os, revolvecnt);
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeShort(os, revolvesector[i]);
            StreamUtils.writeShort(os, revolveang[i]);
            for (int j = 0; j < 48; j++) {
                StreamUtils.writeInt(os, revolvex[i][j]);
                StreamUtils.writeInt(os, revolvey[i][j]);
            }
            StreamUtils.writeInt(os, revolvepivotx[i]);
            StreamUtils.writeInt(os, revolvepivoty[i]);
        }

        StreamUtils.writeShort(os, swingcnt);
        for (int i = 0; i < MAXSWINGDOORS; i++) {
            for (int j = 0; j < 8; j++) {
                StreamUtils.writeShort(os, (short) swingdoor[i].wall[j]);
            }
            StreamUtils.writeShort(os, (short) swingdoor[i].sector);
            StreamUtils.writeShort(os, (short) swingdoor[i].angopen);
            StreamUtils.writeShort(os, (short) swingdoor[i].angclosed);
            StreamUtils.writeShort(os, (short) swingdoor[i].angopendir);
            StreamUtils.writeShort(os, (short) swingdoor[i].ang);
            StreamUtils.writeShort(os, (short) swingdoor[i].anginc);
            for (int j = 0; j < 8; j++) {
                StreamUtils.writeInt(os, swingdoor[i].x[j]);
            }
            for (int j = 0; j < 8; j++) {
                StreamUtils.writeInt(os, swingdoor[i].y[j]);
            }
        }

        StreamUtils.writeShort(os, dragsectorcnt);
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, dragsectorlist[i]);
            StreamUtils.writeShort(os, dragxdir[i]);
            StreamUtils.writeShort(os, dragydir[i]);
            StreamUtils.writeInt(os, dragx1[i]);
            StreamUtils.writeInt(os, dragy1[i]);
            StreamUtils.writeInt(os, dragx2[i]);
            StreamUtils.writeInt(os, dragy2[i]);
            StreamUtils.writeInt(os, dragfloorz[i]);
        }
    }

    public static void StuffSave(OutputStream os) throws IOException {
        for (int i = 0; i < MAXSPRITES; i++) {
            StreamUtils.writeInt(os, gEnemyClock[i]);
            StreamUtils.writeShort(os, gMoveStatus[i]);
        }

        for (int i = 0; i < 6; i++) {
            StreamUtils.writeShort(os, nKills[i]);
            StreamUtils.writeShort(os, nTotalKills[i]);
        }
        StreamUtils.writeInt(os, nEnemyKills);
        StreamUtils.writeInt(os, nEnemyMax);

        StreamUtils.writeShort(os, nDiffDoor);
        StreamUtils.writeShort(os, nDiffDoorBack);
        StreamUtils.writeShort(os, nTrainWall);
        StreamUtils.writeBoolean(os, bActiveTrain);
        StreamUtils.writeBoolean(os, bTrainSoundSwitch);
        StreamUtils.writeInt(os, lockclock);
        StreamUtils.writeInt(os, totalmoves);

        StreamUtils.writeInt(os, visibility);
        StreamUtils.writeInt(os, engine.getrand());

        show2dsector.writeObject(os);
        show2dwall.writeObject(os);
        show2dsprite.writeObject(os);

        StreamUtils.writeByte(os, automapping);

        StreamUtils.writeShort(os, pskybits);
        Renderer renderer = game.getRenderer();
        StreamUtils.writeInt(os, renderer.getParallaxScale());
        for (int i = 0; i < MAXPSKYTILES; i++) {
            StreamUtils.writeShort(os, pskyoff[i]);
        }

        StreamUtils.writeShort(os, connecthead);
        for (int i = 0; i < MAXPLAYERS; i++) {
            StreamUtils.writeShort(os, connectpoint2[i]);
        }

        for (int i = 0; i < MAXPLAYERS; i++) {
            gPlayer[i].writeObject(os);
        }
        StreamUtils.writeShort(os, nPlayerFirstWeapon);
        StreamUtils.writeInt(os, oldchoose);
        StreamUtils.writeShort(os, oldpic);
    }

    public static void MapSave(OutputStream os) throws IOException {
        Board board = boardService.getBoard();
        Sector[] sectors = board.getSectors();
        StreamUtils.writeInt(os, sectors.length);
        for (Sector s : sectors) {
            s.writeObject(os);
        }

        Wall[] walls = board.getWalls();
        StreamUtils.writeInt(os, walls.length);
        for (Wall wal : walls) {
            wal.writeObject(os);
        }

        List<Sprite> sprites = board.getSprites();
        StreamUtils.writeInt(os, sprites.size());
        for (Sprite s : sprites) {
            s.writeObject(os);
        }
    }

    public static void SaveVersion(OutputStream os, int nVersion) throws IOException {
        StreamUtils.writeString(os, savsign);
        StreamUtils.writeShort(os, nVersion);
    }

    public static void AnimationSave(OutputStream os) throws IOException {
        for (int i = 0; i < MAXANIMATES; i++) {
            StreamUtils.writeShort(os, gAnimationData[i].id);
            StreamUtils.writeByte(os, gAnimationData[i].type);
            StreamUtils.writeInt(os, gAnimationData[i].goal);
            StreamUtils.writeInt(os, gAnimationData[i].vel);
            StreamUtils.writeInt(os, gAnimationData[i].acc);
        }
        StreamUtils.writeInt(os, gAnimationCount);
    }

    public static void quicksave() {
        if (gPlayer[myconnectindex].nHealth != 0) {
            gQuickSaving = true;
        }
    }

}
