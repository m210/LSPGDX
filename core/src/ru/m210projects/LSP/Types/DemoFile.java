// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Types;

import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.LSP.Screens.DemoScreen;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static java.lang.Math.min;
import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.LSP.Globals.*;

public class DemoFile {

    public int rcnt;
    public Input[][] recsync;

    public int reccnt;
    public int version;
    public int map, skill;
    public PlayerDemoData playerData;

    public DemoFile(InputStream is) throws Exception {
        rcnt = 0;

        String header = StreamUtils.readString(is, DemoScreen.header.length());
        if (!header.equalsIgnoreCase(DemoScreen.header)) {
            throw new Exception("Wrong header: " + header);
        }

        reccnt = StreamUtils.readInt(is);
        version = StreamUtils.readUnsignedByte(is);

        if( version != GDXBYTEVERSION )
        {
            throw new Exception("Wrong version!");
        }

        map = StreamUtils.readUnsignedByte(is);
        skill = StreamUtils.readUnsignedByte(is);

        playerData = new PlayerDemoData();
        playerData.readObject(is);

        recsync = new Input[reccnt][MAXPLAYERS];
        int rccnt = 0;
        for(int c = 0; c <= reccnt / RECSYNCBUFSIZ; c++)
        {
            int l = min(reccnt - rccnt, RECSYNCBUFSIZ);

            for(int rcnt = rccnt; rcnt < rccnt + l; rcnt++)
                for ( int i = 0; i < 1; i++ ) {
                    recsync[rcnt][i] = new Input().readObject(is);
                }
            rccnt += RECSYNCBUFSIZ;
        }
    }

    public static class PlayerDemoData {
        public short nMana = 160;
        public short nHealth = 160;
        public short[] nAmmo = { 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 };

        public short nWeapon = 17;
        public short nLastWeapon = 0;
        public short nLastChoosedWeapon = 6;
        public short nLastManaWeapon = 13;
        public short nRandDamage1 = 13;
        public short nRandDamage2 = 9;

        public void readObject(InputStream is) throws IOException {
            nHealth = (short) StreamUtils.readInt(is);
            nMana = (short) StreamUtils.readInt(is);

            for (int i = 0; i < nAmmo.length; i++) {
                nAmmo[i] = (short) StreamUtils.readInt(is);
            }

            nWeapon = (short) StreamUtils.readInt(is);
            nLastChoosedWeapon = (short) StreamUtils.readInt(is);
            nLastManaWeapon = (short) StreamUtils.readInt(is);
            nLastWeapon = (short) StreamUtils.readInt(is);
            nRandDamage1 = (short) StreamUtils.readInt(is);
            nRandDamage2 = (short) StreamUtils.readInt(is);
        }

        public void writeObject(OutputStream os) throws IOException {
            StreamUtils.writeInt(os, nHealth);
            StreamUtils.writeInt(os,nMana);

            for (short value : nAmmo) {
                StreamUtils.writeInt(os,value);
            }

            StreamUtils.writeInt(os, nWeapon);
            StreamUtils.writeInt(os, nLastChoosedWeapon);
            StreamUtils.writeInt(os, nLastManaWeapon);
            StreamUtils.writeInt(os, nLastWeapon);
            StreamUtils.writeInt(os, nRandDamage1);
            StreamUtils.writeInt(os, nRandDamage2);
        }

        public PlayerDemoData setFrom(PlayerStruct plr) {
            this.nHealth = plr.nHealth;
            this.nMana = plr.nMana;
            System.arraycopy(plr.nAmmo, 0, this.nAmmo, 0, nAmmo.length);

            this.nWeapon = plr.nWeapon;
            this.nLastChoosedWeapon = plr.nLastChoosedWeapon;
            this.nLastManaWeapon = plr.nLastManaWeapon;
            this.nLastWeapon = plr.nLastWeapon;
            this.nRandDamage1 = plr.nRandDamage1;
            this.nRandDamage2 = plr.nRandDamage2;

            return this;
        }

        public void applyTo(PlayerStruct plr) {
            plr.nHealth = this.nHealth;
            plr.nMana = this.nMana;
            System.arraycopy(this.nAmmo, 0, plr.nAmmo, 0, nAmmo.length);

            plr.nWeapon = this.nWeapon;
            plr.nLastChoosedWeapon = this.nLastChoosedWeapon;
            plr.nLastManaWeapon = this.nLastManaWeapon;
            plr.nLastWeapon = this.nLastWeapon;
            plr.nRandDamage1 = this.nRandDamage1;
            plr.nRandDamage2 = this.nRandDamage2;
        }
    }
}
