package ru.m210projects.LSP.Types;

import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.LSP.Screens.DemoScreen;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.StandardOpenOption.WRITE;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.gDemoScreen;
import static ru.m210projects.LSP.Main.game;

public class DemoRec {

    private final OutputStream os;
    private final Path filepath;
    public int reccnt;
    public int totalreccnt;
    public int recversion;

    public DemoRec(FileOutputStream os, Path filepath, int nVersion) throws IOException {
        this.filepath = filepath;

        StreamUtils.writeString(os, DemoScreen.header);
        StreamUtils.writeInt(os, 0);
        StreamUtils.writeByte(os, nVersion);

        StreamUtils.writeByte(os, mapnum);
        StreamUtils.writeByte(os, nDifficult);

        DemoFile.PlayerDemoData playerData = new DemoFile.PlayerDemoData().setFrom(gPlayer[myconnectindex]);
        playerData.writeObject(os);

        totalreccnt = 0;
        reccnt = 0;
        recversion = nVersion;
        this.os = new BufferedOutputStream(os, RECSYNCBUFSIZ * Input.sizeof(nVersion));
    }

    public void record() {
        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            try {
                gPlayer[i].pInput.writeObject(os);
                reccnt++;
                if (reccnt >= RECSYNCBUFSIZ) {
                    os.flush();
                    reccnt = 0;
                }
                totalreccnt++;
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
                close();
            }
        }
    }

    public void close() {
        try {
            os.close();
            try (OutputStream out = Files.newOutputStream(filepath, WRITE)) {
                StreamUtils.writeString(out, DemoScreen.header);
                StreamUtils.writeInt(out, totalreccnt);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Console.out.println("Stop recording");
        Directory dir = game.getCache().getGameDirectory();

        Entry entry = dir.addEntry(filepath);
        if (entry.exists()) {
            gDemoScreen.demofiles.add(entry);
            dir.revalidate();
        }
    }

}
