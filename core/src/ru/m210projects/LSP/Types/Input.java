// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Types;

import ru.m210projects.Build.Pattern.BuildNet.NetInput;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

public class Input implements NetInput {

    public int xvel, yvel;
    public float angvel;
    public int bits;
    public float horiz;

    public Input readObject(InputStream is) throws IOException {
        xvel = StreamUtils.readInt(is);
        yvel = StreamUtils.readInt(is);
        angvel = StreamUtils.readFloat(is);
        bits = StreamUtils.readInt(is);
        horiz = StreamUtils.readFloat(is);
        return this;
    }

    public void writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, xvel);
        StreamUtils.writeInt(os, yvel);
        StreamUtils.writeFloat(os, angvel);
        StreamUtils.writeInt(os, bits);
        StreamUtils.writeFloat(os, horiz);
    }

    public static int sizeof(int version) {
        return 20;
    }

    @Override
    public int GetInput(byte[] p, int offset, NetInput oldInput) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int PutInput(byte[] p, int offset, NetInput oldInput) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void reset() {
        xvel = 0;
        yvel = 0;
        angvel = 0;
        horiz = 0;
        bits = 0;
    }

    @Override
    public NetInput Copy(NetInput src) {
        Input i = (Input) src;

        this.xvel = i.xvel;
        this.yvel = i.yvel;
        this.angvel = i.angvel;
        this.horiz = i.horiz;
        this.bits = i.bits;

        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Input input = (Input) o;
        return xvel == input.xvel && yvel == input.yvel && Float.compare(angvel, input.angvel) == 0 && bits == input.bits && Float.compare(horiz, input.horiz) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xvel, yvel, angvel, bits, horiz);
    }

    @Override
    public String toString() {
        return "Input{" +
                "xvel=" + xvel +
                ", yvel=" + yvel +
                ", angvel=" + angvel +
                ", bits=" + bits +
                ", horiz=" + horiz +
                '}';
    }
}
