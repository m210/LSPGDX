package ru.m210projects.LSP.Types;

import ru.m210projects.Build.filehandle.InputStreamProvider;
import ru.m210projects.Build.filehandle.grp.GrpEntry;

import java.io.IOException;

public class MapGroup extends LSPDatFile {

    public MapGroup(InputStreamProvider provider) throws IOException {
        super("LMART000.DAT", 36, provider);
    }

    @Override
    protected GrpEntry newEntry(InputStreamProvider provider, int index, int offset, int size) {
        String name = "lev" + index + ".map";
        return new GrpEntry(provider, name, offset, size);
    }
}
