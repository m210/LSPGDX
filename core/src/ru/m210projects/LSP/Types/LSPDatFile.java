package ru.m210projects.LSP.Types;

import ru.m210projects.Build.filehandle.InputStreamProvider;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.grp.GrpEntry;
import ru.m210projects.Build.filehandle.grp.GrpFile;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class LSPDatFile extends GrpFile {

    public LSPDatFile(String name, int numFiles, InputStreamProvider provider) throws IOException {
        super(name);

        try (InputStream is = new BufferedInputStream(provider.newInputStream())) {
            int fileSize = is.available();
            int[] offset = new int[numFiles];
            for (int i = 0; i < numFiles; i++) {
                offset[i] = StreamUtils.readInt(is);
            }

            for (int i = 0; i < numFiles; i++) {
                int len = fileSize;
                if (i < numFiles - 1) {
                    len = offset[i + 1];
                }

                int siz = len - offset[i];
                if (siz > 0 && siz < fileSize && offset[i] > 0 && offset[i] < fileSize) {
                    GrpEntry entry = newEntry(provider, i, offset[i], siz);
                    entry.setParent(this);
                    entries.put(entry.getName().toUpperCase(), entry);
                }
            }
        }
    }

    protected GrpEntry newEntry(InputStreamProvider provider, int index, int offset, int size) {
        return new GrpEntry(provider, Integer.toString(index), offset, size);
    }

}
