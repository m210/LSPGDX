// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Types;

import ru.m210projects.Build.Gameutils;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

public class PCXFile {

    private final int width;
    private final int height;
    private int pos;
    private final byte[] buffer;
    private byte[] palette;

    public PCXFile(Entry fil) throws Exception {
        try (InputStream is = fil.getInputStream()) {
            byte manufacturer = StreamUtils.readByte(is);
            byte version = StreamUtils.readByte(is);
            byte rle = StreamUtils.readByte(is);
            byte bpp = StreamUtils.readByte(is);

            if (manufacturer != 10 || version != 5 || rle != 1 || bpp != 8) {
                throw new UnsupportedOperationException("Couldn't read the pcx file. Unsupported version");
            }

            StreamUtils.skip(is, 4);
            width = StreamUtils.readShort(is) + 1;
            height = StreamUtils.readShort(is) + 1;
            StreamUtils.skip(is, 116);

            pos = 0;
            buffer = new byte[width * height];
            for (int y = 0; y < height; y++) {
                decodeLine(is);
            }

            StreamUtils.readByte(is);
            if (is.available() >= 768) {
                palette = StreamUtils.readBytes(is, 768);
            }
        }
    }

    private void decodeLine(InputStream is) throws IOException {
        int i = 0, len;
        while (i < width) {
            byte header = StreamUtils.readByte(is);
            if ((header & 0xC0) == 0xC0) {
                len = header & 0x3F;
                Gameutils.fill(buffer, pos, pos += len, StreamUtils.readByte(is));
                i += len;
            } else {
                buffer[pos++] = header;
                i++;
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public byte[] getData() {
        return buffer;
    }

    public byte[] getPalette() {
        return palette;
    }
}
