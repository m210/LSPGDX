// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Types;

import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;
import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static ru.m210projects.LSP.Globals.MAXHEALTH;
import static ru.m210projects.LSP.Globals.MAXMANNA;
import static ru.m210projects.LSP.Main.*;

public class PlayerStruct implements Serializable<PlayerStruct> {

    public boolean noclip = false;
    public transient final Input pInput = new Input();
    public int x, y, z, ox, oy, oz;
    public float oang, ang, ohoriz, horiz, nBobbing;
    public int sectnum, osectnum;
    public byte gViewMode;
    public int ozoom, zoom;
    public int nSprite;
    public int hvel;
    public byte nWeaponImpact;
    public short nWeaponImpactAngle;
    public short nHealth;
    public short nWeaponState;
    public short nBobCount;
    public short nWeaponSeq;
    public short nMana;
    public final short[] nAmmo = {0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
    public short nLastWeapon;
    public short nWeapon;
    public short nLastChoosedWeapon;
    public short nLastManaWeapon;
    public short word_586FC; // sector
    public short word_58794; // weapon sound
    public short word_5878C; // weapon
    public int nWeaponShootCount;
    public int nSlimeDamageCount;
    public short nPlayerStatus; // live = 0, dead = 1, god = 5
    public int nFirstWeaponDamage;
    public int nSecondWeaponDamage;
    public short nRandDamage1;
    public short nRandDamage2;
    public int nWeaponClock;
    public int isWeaponFire;
    public int nNewWeapon;
    public int isWeaponsSwitching;
    public int nSwitchingClock;

    private transient short invWeapon, invChoosedWeapon, invMannaWeapon, invLastWeapon, invRand1, invRand2;

    public void savePlayersInventory() {
        invWeapon = nWeapon;
        invChoosedWeapon = nLastChoosedWeapon;
        invMannaWeapon = nLastManaWeapon;
        invLastWeapon = nLastWeapon;
        invRand1 = nRandDamage1;
        invRand2 = nRandDamage2;
    }

    public void loadPlayersInventory() {
        nWeapon = invWeapon;
        nLastChoosedWeapon = invChoosedWeapon;
        nLastManaWeapon = invMannaWeapon;
        nLastWeapon = invLastWeapon;
        nRandDamage1 = invRand1;
        nRandDamage2 = invRand2;
    }

    public void UpdatePlayerLoc() {
        Sprite spr = boardService.getSprite(nSprite);
        ILoc oldLoc = game.pInt.getsprinterpolate(nSprite);
        if (spr != null && oldLoc != null) {
            oldLoc.x = spr.getX();
            oldLoc.y = spr.getY();
            oldLoc.z = spr.getZ();
        } else {
            game.pInt.setsprinterpolate(nSprite, boardService.getSprite(nSprite));
        }

        ox = x;
        oy = y;
        oz = z;
        ozoom = zoom;
        ohoriz = horiz + nBobbing;
        oang = ang;
    }

    public void calcRandomVariables() {
        int v24 = engine.rand();

        nRandDamage1 = (short) (v24 % 9 + 7);
        nRandDamage2 = (short) ((v24 >> 5) % 5 + 6);
    }

    public void copy(PlayerStruct src) {
        pInput.reset();

        x = src.x;
        y = src.y;
        z = src.z;
        ox = src.ox;
        oy = src.oy;
        oz = src.oz;
        ang = src.ang;
        oang = src.oang;
        horiz = src.horiz;
        ohoriz = src.ohoriz;
        nBobbing = src.nBobbing;
        sectnum = src.sectnum;
        osectnum = src.osectnum;
        gViewMode = src.gViewMode;
        zoom = src.zoom;
        ozoom = src.ozoom;
        nSprite = src.nSprite;
        hvel = src.hvel;
        nWeaponImpact = src.nWeaponImpact;
        nWeaponImpactAngle = src.nWeaponImpactAngle;
        nHealth = src.nHealth;
        nWeaponState = src.nWeaponState;
        nBobCount = src.nBobCount;
        nWeaponSeq = src.nWeaponSeq;
        nMana = src.nMana;
        System.arraycopy(src.nAmmo, 0, nAmmo, 0, nAmmo.length);
        nLastWeapon = src.nLastWeapon;
        nWeapon = src.nWeapon;
        nLastChoosedWeapon = src.nLastChoosedWeapon;
        nLastManaWeapon = src.nLastManaWeapon;
        word_586FC = src.word_586FC;
        word_58794 = src.word_58794;
        word_5878C = src.word_5878C;
        nWeaponShootCount = src.nWeaponShootCount;
        nSlimeDamageCount = src.nSlimeDamageCount;
        nPlayerStatus = src.nPlayerStatus;
        nFirstWeaponDamage = src.nFirstWeaponDamage;
        nSecondWeaponDamage = src.nSecondWeaponDamage;
        nRandDamage1 = src.nRandDamage1;
        nRandDamage2 = src.nRandDamage2;
        nWeaponClock = src.nWeaponClock;
        isWeaponFire = src.isWeaponFire;
        nNewWeapon = src.nNewWeapon;
        isWeaponsSwitching = src.isWeaponsSwitching;
        nSwitchingClock = src.nSwitchingClock;
    }

    @Override
    public PlayerStruct readObject(InputStream is) throws IOException {
        pInput.reset();

        x = StreamUtils.readInt(is);
        y = StreamUtils.readInt(is);
        z = StreamUtils.readInt(is);
        ox = StreamUtils.readInt(is);
        oy = StreamUtils.readInt(is);
        oz = StreamUtils.readInt(is);
        ang = StreamUtils.readFloat(is);
        oang = StreamUtils.readFloat(is);
        horiz = StreamUtils.readFloat(is);
        ohoriz = StreamUtils.readFloat(is);
        nBobbing = StreamUtils.readFloat(is);
        sectnum = StreamUtils.readShort(is);
        osectnum = StreamUtils.readShort(is);
        gViewMode = StreamUtils.readByte(is);
        zoom = StreamUtils.readInt(is);
        ozoom = StreamUtils.readInt(is);
        nSprite = StreamUtils.readShort(is);
        hvel = StreamUtils.readInt(is);
        nWeaponImpact = StreamUtils.readByte(is);
        nWeaponImpactAngle = StreamUtils.readShort(is);
        nHealth = StreamUtils.readShort(is);
        nWeaponState = StreamUtils.readShort(is);
        nBobCount = StreamUtils.readShort(is);
        nWeaponSeq = StreamUtils.readShort(is);
        nMana = StreamUtils.readShort(is);
        for (int i = 0; i < nAmmo.length; i++) {
            nAmmo[i] = StreamUtils.readShort(is);
        }
        nLastWeapon = StreamUtils.readShort(is);
        nWeapon = StreamUtils.readShort(is);
        nLastChoosedWeapon = StreamUtils.readShort(is);
        nLastManaWeapon = StreamUtils.readShort(is);
        word_586FC = StreamUtils.readShort(is);
        word_58794 = StreamUtils.readShort(is);
        word_5878C = StreamUtils.readShort(is);
        nWeaponShootCount = StreamUtils.readInt(is);
        nSlimeDamageCount = StreamUtils.readInt(is);
        nPlayerStatus = StreamUtils.readShort(is);
        nFirstWeaponDamage = StreamUtils.readInt(is);
        nSecondWeaponDamage = StreamUtils.readInt(is);
        nRandDamage1 = StreamUtils.readShort(is);
        nRandDamage2 = StreamUtils.readShort(is);
        nWeaponClock = StreamUtils.readInt(is);
        isWeaponFire = StreamUtils.readInt(is);
        nNewWeapon = StreamUtils.readInt(is);
        isWeaponsSwitching = StreamUtils.readInt(is);
        nSwitchingClock = StreamUtils.readInt(is);

        return this;
    }

    @Override
    public PlayerStruct writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, x);
        StreamUtils.writeInt(os, y);
        StreamUtils.writeInt(os, z);
        StreamUtils.writeInt(os, ox);
        StreamUtils.writeInt(os, oy);
        StreamUtils.writeInt(os, oz);
        StreamUtils.writeFloat(os, ang);
        StreamUtils.writeFloat(os, oang);
        StreamUtils.writeFloat(os, horiz);
        StreamUtils.writeFloat(os, ohoriz);
        StreamUtils.writeFloat(os, nBobbing);
        StreamUtils.writeShort(os, sectnum);
        StreamUtils.writeShort(os, osectnum);
        StreamUtils.writeByte(os, gViewMode);
        StreamUtils.writeInt(os, zoom);
        StreamUtils.writeInt(os, ozoom);
        StreamUtils.writeShort(os, nSprite);
        StreamUtils.writeInt(os, hvel);
        StreamUtils.writeByte(os, nWeaponImpact);
        StreamUtils.writeShort(os, nWeaponImpactAngle);
        StreamUtils.writeShort(os, nHealth);
        StreamUtils.writeShort(os, nWeaponState);
        StreamUtils.writeShort(os, nBobCount);
        StreamUtils.writeShort(os, nWeaponSeq);
        StreamUtils.writeShort(os, nMana);
        for (short value : nAmmo) {
            StreamUtils.writeShort(os, value);
        }
        StreamUtils.writeShort(os, nLastWeapon);
        StreamUtils.writeShort(os, nWeapon);
        StreamUtils.writeShort(os, nLastChoosedWeapon);
        StreamUtils.writeShort(os, nLastManaWeapon);
        StreamUtils.writeShort(os, word_586FC);
        StreamUtils.writeShort(os, word_58794);
        StreamUtils.writeShort(os, word_5878C);

        StreamUtils.writeInt(os, nWeaponShootCount);
        StreamUtils.writeInt(os, nSlimeDamageCount);

        StreamUtils.writeShort(os, nPlayerStatus);
        StreamUtils.writeInt(os, nFirstWeaponDamage);
        StreamUtils.writeInt(os, nSecondWeaponDamage);
        StreamUtils.writeShort(os, nRandDamage1);
        StreamUtils.writeShort(os, nRandDamage2);

        StreamUtils.writeInt(os, nWeaponClock);
        StreamUtils.writeInt(os, isWeaponFire);
        StreamUtils.writeInt(os, nNewWeapon);
        StreamUtils.writeInt(os, isWeaponsSwitching);
        StreamUtils.writeInt(os, nSwitchingClock);

        return this;
    }

    public void reset() {
        this.nHealth = MAXHEALTH;
        this.nMana = MAXMANNA;
        Arrays.fill(this.nAmmo, (short) 0);
        this.nAmmo[1] = 100;
        this.nAmmo[7] = 1;

        this.nWeapon = 0;
        this.nLastChoosedWeapon = 0;
        this.nLastManaWeapon = 0;
        this.nLastWeapon = 0;
        this.nRandDamage1 = 0;
        this.nRandDamage2 = 0;
    }
}
