// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Types;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.BitMap;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.LSP.Animate.MAXANIMATES;
import static ru.m210projects.LSP.LoadSave.*;

public class SafeLoader {

    public final int MAXSWINGDOORS = 32;
    public int gAnimationCount = 0;
    public final ANIMATION[] gAnimationData = new ANIMATION[MAXANIMATES];
    public final PlayerStruct[] plr = new PlayerStruct[MAXPLAYERS];
    public final int[] gEnemyClock = new int[MAXSPRITES];
    public final short[] gMoveStatus = new short[MAXSPRITES];
    public final short[] nKills = new short[6];
    public final short[] nTotalKills = new short[6];
    public int nEnemyKills, nEnemyMax;
    public short nDiffDoor, nDiffDoorBack, nTrainWall;
    public boolean bActiveTrain, bTrainSoundSwitch;
    public int lockclock;
    public int totalmoves;
    public short nPlayerFirstWeapon = 17;
    public int oldchoose;
    public short oldpic;
    public int mapnum, skill;
    public final short[] pskyoff = new short[MAXPSKYTILES];
    public short pskybits;
    public int parallaxyscale;
    public short connecthead;
    public final short[] connectpoint2 = new short[MAXPLAYERS];
    public int randomseed;
    public int visibility;
    public byte automapping;

    public final BitMap show2dsector = new BitMap(MAXSECTORSV7);
    public final BitMap show2dwall = new BitMap(MAXWALLSV7);
    public final BitMap show2dsprite = new BitMap(MAXSPRITESV7);

    public Sector[] sector;
    public Wall[] wall;
    public List<Sprite> sprite;
    public final short[] waterfountainwall = new short[MAXPLAYERS];
    public final short[] waterfountaincnt = new short[MAXPLAYERS];
    public short ypanningwallcnt;
    public final short[] ypanningwalllist = new short[16];
    public short floorpanningcnt;
    public final short[] floorpanninglist = new short[16];
    public short warpsectorcnt;
    public final short[] warpsectorlist = new short[16];
    public short xpanningsectorcnt;
    public final short[] xpanningsectorlist = new short[16];
    public short warpsector2cnt;
    public final short[] warpsector2list = new short[32];
    public final short[][] subwaytracksector = new short[5][128];
    public final short[] subwaynumsectors = new short[5];
    public short subwaytrackcnt;
    public final int[][] subwaystop = new int[5][8];
    public final int[] subwaystopcnt = new int[5];
    public final int[] subwaytrackx1 = new int[5];
    public final int[] subwaytracky1 = new int[5];
    public final int[] subwaytrackx2 = new int[5];
    public final int[] subwaytracky2 = new int[5];
    public final int[] subwayx = new int[5];
    public final int[] subwaygoalstop = new int[5];
    public final int[] subwayvel = new int[5];
    public final int[] subwaypausetime = new int[5];
    public final short[] revolvesector = new short[4];
    public final short[] revolveang = new short[4];
    public short revolvecnt;
    public final int[][] revolvex = new int[4][48];
    public final int[][] revolvey = new int[4][48];
    public final int[] revolvepivotx = new int[4];
    public final int[] revolvepivoty = new int[4];
    public short swingcnt;
    public final SwingDoor[] swingdoor = new SwingDoor[MAXSWINGDOORS];

    public final short[] dragsectorlist = new short[16];
    public final short[] dragxdir = new short[16];
    public final short[] dragydir = new short[16];
    public short dragsectorcnt;
    public final int[] dragx1 = new int[16];
    public final int[] dragy1 = new int[16];
    public final int[] dragx2 = new int[16];
    public final int[] dragy2 = new int[16];
    public final int[] dragfloorz = new int[16];


    public SafeLoader() {
        for (int i = 0; i < MAXPLAYERS; i++) {
            plr[i] = new PlayerStruct();
        }

        for (int i = 0; i < MAXANIMATES; i++) {
            gAnimationData[i] = new ANIMATION();
        }
        for (int i = 0; i < MAXSWINGDOORS; i++) {
            swingdoor[i] = new SwingDoor();
        }
    }

    public boolean load(InputStream is) {
        try {
            LoadGDXHeader(is);
            LoadGDXBlock(is);
            StuffLoad(is);
            MapLoad(is);
            SectorLoad(is);
            AnimationLoad(is);

            if (is.available() == 0) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void SectorLoad(InputStream is) throws IOException {
        for (int i = 0; i < MAXPLAYERS; i++) {
            waterfountainwall[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < MAXPLAYERS; i++) {
            waterfountaincnt[i] = StreamUtils.readShort(is);
        }

        ypanningwallcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            ypanningwalllist[i] = StreamUtils.readShort(is);
        }
        floorpanningcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            floorpanninglist[i] = StreamUtils.readShort(is);
        }
        warpsectorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            warpsectorlist[i] = StreamUtils.readShort(is);
        }
        xpanningsectorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            xpanningsectorlist[i] = StreamUtils.readShort(is);
        }
        warpsector2cnt = StreamUtils.readShort(is);
        for (int i = 0; i < 32; i++) {
            warpsector2list[i] = StreamUtils.readShort(is);
        }

        subwaytrackcnt = StreamUtils.readShort(is);
        for (int a = 0; a < 5; ++a) {
            for (int b = 0; b < 128; ++b) {
                subwaytracksector[a][b] = StreamUtils.readShort(is);
            }
        }
        for (int i = 0; i < 5; i++) {
            subwaynumsectors[i] = StreamUtils.readShort(is);
        }

        for (int a = 0; a < 5; ++a) {
            for (int b = 0; b < 8; ++b) {
                subwaystop[a][b] = StreamUtils.readInt(is);
            }
        }
        for (int i = 0; i < 5; i++) {
            subwaystopcnt[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 5; i++) {
            subwaytrackx1[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 5; i++) {
            subwaytracky1[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 5; i++) {
            subwaytrackx2[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 5; i++) {
            subwaytracky2[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 5; i++) {
            subwayx[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 5; i++) {
            subwaygoalstop[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 5; i++) {
            subwayvel[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 5; i++) {
            subwaypausetime[i] = StreamUtils.readInt(is);
        }

        revolvecnt = StreamUtils.readShort(is);
        for (int i = 0; i < 4; i++) {
            revolvesector[i] = StreamUtils.readShort(is);
            revolveang[i] = StreamUtils.readShort(is);
            for (int j = 0; j < 48; j++) {
                revolvex[i][j] = StreamUtils.readInt(is);
                revolvey[i][j] = StreamUtils.readInt(is);
            }
            revolvepivotx[i] = StreamUtils.readInt(is);
            revolvepivoty[i] = StreamUtils.readInt(is);
        }

        swingcnt = StreamUtils.readShort(is);
        for (int i = 0; i < MAXSWINGDOORS; i++) {
            for (int j = 0; j < 8; j++) {
                swingdoor[i].wall[j] = StreamUtils.readShort(is);
            }
            swingdoor[i].sector = StreamUtils.readShort(is);
            swingdoor[i].angopen = StreamUtils.readShort(is);
            swingdoor[i].angclosed = StreamUtils.readShort(is);
            swingdoor[i].angopendir = StreamUtils.readShort(is);
            swingdoor[i].ang = StreamUtils.readShort(is);
            swingdoor[i].anginc = StreamUtils.readShort(is);
            for (int j = 0; j < 8; j++) {
                swingdoor[i].x[j] = StreamUtils.readInt(is);
            }
            for (int j = 0; j < 8; j++) {
                swingdoor[i].y[j] = StreamUtils.readInt(is);
            }
        }

        dragsectorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            dragsectorlist[i] = StreamUtils.readShort(is);
            dragxdir[i] = StreamUtils.readShort(is);
            dragydir[i] = StreamUtils.readShort(is);
            dragx1[i] = StreamUtils.readInt(is);
            dragy1[i] = StreamUtils.readInt(is);
            dragx2[i] = StreamUtils.readInt(is);
            dragy2[i] = StreamUtils.readInt(is);
            dragfloorz[i] = StreamUtils.readInt(is);
        }
    }

    public void AnimationLoad(InputStream is) throws IOException {
        for (int i = 0; i < MAXANIMATES; i++) {
            short index = StreamUtils.readShort(is);
            byte type = StreamUtils.readByte(is);
            gAnimationData[i].id = index;
            gAnimationData[i].type = type;
            gAnimationData[i].ptr = null;
            gAnimationData[i].goal = StreamUtils.readInt(is);
            gAnimationData[i].vel = StreamUtils.readInt(is);
            gAnimationData[i].acc = StreamUtils.readInt(is);
        }
        gAnimationCount = StreamUtils.readInt(is);
    }

    public void StuffLoad(InputStream is) throws IOException {
        for (int i = 0; i < MAXSPRITES; i++) {
            gEnemyClock[i] = StreamUtils.readInt(is);
            gMoveStatus[i] = StreamUtils.readShort(is);
        }

        for (int i = 0; i < 6; i++) {
            nKills[i] = StreamUtils.readShort(is);
            nTotalKills[i] = StreamUtils.readShort(is);
        }
        nEnemyKills = StreamUtils.readInt(is);
        nEnemyMax = StreamUtils.readInt(is);

        nDiffDoor = StreamUtils.readShort(is);
        nDiffDoorBack = StreamUtils.readShort(is);
        nTrainWall = StreamUtils.readShort(is);
        bActiveTrain = StreamUtils.readBoolean(is);
        bTrainSoundSwitch = StreamUtils.readBoolean(is);
        lockclock = StreamUtils.readInt(is);
        totalmoves = StreamUtils.readInt(is);

        visibility = StreamUtils.readInt(is);
        randomseed = StreamUtils.readInt(is);

        show2dsector.readObject(is);
        show2dwall.readObject(is);
        show2dsprite.readObject(is);

        automapping = StreamUtils.readByte(is);

        pskybits = StreamUtils.readShort(is);
        parallaxyscale = StreamUtils.readInt(is);
        for (int i = 0; i < MAXPSKYTILES; i++) {
            pskyoff[i] = StreamUtils.readShort(is);
        }

        connecthead = StreamUtils.readShort(is);
        for (int i = 0; i < MAXPLAYERS; i++) {
            connectpoint2[i] = StreamUtils.readShort(is);
        }

        for (int i = 0; i < MAXPLAYERS; i++) {
            plr[i].readObject(is);
        }

        nPlayerFirstWeapon = StreamUtils.readShort(is);
        oldchoose = StreamUtils.readInt(is);
        oldpic = StreamUtils.readShort(is);
    }

    public void MapLoad(InputStream is) throws IOException {
        sector = new Sector[StreamUtils.readInt(is)];
        for (int i = 0; i < sector.length; i++) {
            sector[i] = new Sector().readObject(is);
        }

        wall = new Wall[StreamUtils.readInt(is)];
        for (int i = 0; i < wall.length; i++) {
            wall[i] = new Wall().readObject(is);
        }

        int numSprites = StreamUtils.readInt(is);
        sprite = new ArrayList<>(numSprites * 2);

        for (int i = 0; i < numSprites; i++) {
            sprite.add(new Sprite().readObject(is));
        }
    }

    public void LoadGDXHeader(InputStream is) {
        mapnum = -1;
        skill = -1;

        try {
            StreamUtils.skip(is, SAVETIME + SAVENAME);

            mapnum = StreamUtils.readInt(is);
            skill = StreamUtils.readInt(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LoadGDXBlock(InputStream is) throws IOException {
        // reserve place for extra data
        StreamUtils.skip(is, SAVEGDXDATA);
    }
}
