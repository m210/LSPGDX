// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Types;

import ru.m210projects.Build.Pattern.ScreenAdapters.MovieScreen;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static ru.m210projects.LSP.Main.game;

public class LSPMovieFile implements MovieScreen.MovieFile {

    public boolean paletteChanged;
    private byte[] pal;
    private byte[] frameBuffer;
    private Frame[] frames;
    private final int rate;
    private final int width;
    private final int height;

    public LSPMovieFile(String path) throws Exception {
        Entry fil = game.getCache().getEntry(path, true);
        if (!fil.exists()) {
            throw new FileNotFoundException(path + " not found!");
        }
        try(InputStream is = fil.getInputStream()) {
            int size = StreamUtils.readInt(is); /* whole file size */
            if (size != fil.getSize()) {
                throw new Exception("Wrong size");
            }

            int type = StreamUtils.readUnsignedShort(is); /* must be 0xAF11 */
            if (type != 0xAF11) {
                throw new Exception("Wrong type");
            }

            int numframes = StreamUtils.readShort(is); /* total frames in file */
            width = StreamUtils.readShort(is); /* video frame width */
            height = StreamUtils.readShort(is); /* video frame height */
            rate = StreamUtils.readShort(is);
            StreamUtils.readShort(is); /* ? always 3 */
            StreamUtils.readShort(is); /* ? always 0xF or 0xA - transparent/background color? frame time? */
            StreamUtils.skip(is, 110); /* padding to 128 bytes (0x80) */

            frameBuffer = new byte[width * height];
            frames = new Frame[numframes];

            for (int i = 0; i < numframes; i++) {
                frames[i] = new Frame(is);
            }
        }
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getFrames() {
        if (frames != null) {
            return frames.length;
        }
        return 0;
    }

    @Override
    public byte[] getPalette() {
        return pal;
    }

    @Override
    public float getRate() {
        return 1000f / rate;
    }

    @Override
    public byte[] getFrame(int num) {
        return frames[num].draw();
    }

    @Override
    public void close() {
        frameBuffer = null;
        pal = null;
        frames = null;
    }

    @Override
    public void playAudio() {

    }

    private class Frame {
        final int size; /* whole frame size */
        final int type; /* must be 0xF1FA */
        final int chunks; /* chunks in frame */
        final ByteBuffer data;

        public Frame(InputStream is) throws IOException {
            size = StreamUtils.readInt(is); /* whole frame size */
            type = StreamUtils.readUnsignedShort(is); /* must be 0xF1FA */
            chunks = StreamUtils.readUnsignedShort(is); /* chunks in frame */
            StreamUtils.skip(is, 8);
            data = ByteBuffer.allocate(size - 16).order(ByteOrder.LITTLE_ENDIAN);
            StreamUtils.readBuffer(is, data);
        }

        public byte[] draw() {
            paletteChanged = false;
            if (type != 0xF1FA) {
                return null;
            }

            data.rewind();
            for (int j = 0; j < chunks; j++) {
                int chunk_size = data.getInt();
                int chunk_type = data.getShort();
                switch (chunk_type) {
                    case 0x0B:
                        decode_palette();
                        break;
                    case 0x0C:
                        decode_block_0c();
                        break;
                    case 0x0F:
                        decode_block_0f();
                        break;
                    case 0x10:
                        data.get(frameBuffer, 0, chunk_size - 4);
                        break;
                    default:
                        break; // unknown chunk - skip it
                }
            }

            return frameBuffer;
        }

        private void decode_palette() {
            data.getInt(); // (skip uint32_t - unused, always 1?)
            pal = new byte[768];
            data.get(pal);
            for (int p = 0; p < 768; p++) {
                pal[p] <<= 2;
            }
            paletteChanged = true;
        }

        private void decode_block_0c() {
            int line = 320 * data.getShort();
            int nextline = line;
            int width = data.getShort();
            for (int j = 0; j < width; j++) {
                nextline += 320;

                int v21 = data.get() & 0xFF;
                if (v21 > 0) {
                    for (int i = 0; i < v21; i++) {
                        line += data.get() & 0xFF;
                        int v20 = data.get() & 0xFF;
                        if (v20 < 128) {
                            for (int p = 0; p < v20; p++) {
                                frameBuffer[line + p] = data.get();
                            }
                            line += v20;
                        }
                        if (v20 > 128) {
                            byte col = data.get();
                            for (int p = 0; p < 256 - v20; p++) {
                                frameBuffer[line + p] = col;
                            }
                            line += (256 - v20);
                        }
                    }
                }

                line = nextline;
            }
        }

        private void decode_block_0f() {
            int line = 0;
            for (int i = 1; i <= 200; i++) {
                int v18 = data.get() & 0xFF;
                if (v18 > 0) {
                    for (int j = 0; j < v18; j++) {
                        int v8 = data.get() & 0xFF;
                        if (v8 > 128) {
                            for (int p = 0; p < 256 - v8; p++) {
                                frameBuffer[line + p] = data.get();
                            }
                            line += 256 - v8;
                        }
                        if (v8 < 128) {
                            byte col = data.get();
                            for (int p = 0; p < v8; p++) {
                                frameBuffer[line + p] = col;
                            }
                            line += v8;
                        }
                    }
                }
                line = 320 * i;
            }
        }
    }
}
