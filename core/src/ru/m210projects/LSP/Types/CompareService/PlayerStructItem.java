package ru.m210projects.LSP.Types.CompareService;

import ru.m210projects.LSP.Types.PlayerStruct;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.LSP.Globals.gPlayer;

@SuppressWarnings("unused")
public class PlayerStructItem extends StructItem<PlayerStruct> {

    public PlayerStructItem(PlayerStruct objectStruct, int index) {
        super(objectStruct, index);
    }

    @Override
    protected PlayerStruct readObject(InputStream is) throws IOException {
        return new PlayerStruct().readObject(is);
    }

    @Override
    public void write(OutputStream os) throws IOException {
        gPlayer[index].writeObject(os);
    }
}
