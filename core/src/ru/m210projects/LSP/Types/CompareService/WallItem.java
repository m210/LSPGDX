package ru.m210projects.LSP.Types.CompareService;

import ru.m210projects.Build.Types.Wall;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@SuppressWarnings("unused")
public class WallItem extends StructItem<Wall> {

    public WallItem(Wall objectStruct, int index) {
        super(objectStruct, index);

        objectStruct.setCstat(objectStruct.getCstat() & 0xFF);
    }

    @Override
    protected Wall readObject(InputStream is) throws IOException {
        return new Wall().readObject(is);
    }

    @Override
    public void write(OutputStream os) throws IOException {
        objectStruct.writeObject(os);
    }
}
