// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.osd.Console;

import static ru.m210projects.Build.Gameutils.BClipHigh;
import static ru.m210projects.Build.Gameutils.BClipLow;
import static ru.m210projects.Build.Pragmas.scale;
import static ru.m210projects.LSP.Globals.lockclock;
import static ru.m210projects.LSP.Main.*;

public class Quotes {

    public static final int showQuotes = 4;
    public static final int quoteTime = 500;
    public static int nextTime;
    public static int nextY;
    public static final int kMaxQuotes = 16;
    public static int numQuotes;
    public static int hideQuotes;
    public static int totalQuotes;
    public static final QUOTE[] quotes = new QUOTE[kMaxQuotes];
    public static final int yOffset = 11;

    public static void InitQuotes() {
        for (int i = 0; i < kMaxQuotes; i++) {
            quotes[i] = new QUOTE();
        }
    }

    public static void resetQuotes() {
        numQuotes = 0;
        totalQuotes = 0;
        hideQuotes = 0;
    }

    public static void viewSetMessage(String message) {
        QUOTE quote = quotes[totalQuotes];
        quote.messageText = message;

        Console.out.println(message);

        quote.messageTime = 4 * quoteTime + lockclock;

        totalQuotes += 1;
        totalQuotes %= kMaxQuotes;
        numQuotes += 1;
        if (numQuotes > showQuotes) {
            hideQuotes += 1;
            hideQuotes %= kMaxQuotes;
            nextTime = 0;
            numQuotes = showQuotes;
            nextY = yOffset;
        }
    }

    public static void viewDisplayMessage() {
        Renderer renderer = game.getRenderer();
        if (!cfg.gShowMessages || game.menu.gShowMenu) {
            return;
        }

        int x = scale(10, cfg.gHUDSize, 65536), y = scale(10, cfg.gHUDSize, 65536);
        int nShade = BClipHigh(numQuotes << 3, 48);
        y += scale(nextY, cfg.gHUDSize, 65536);

        for (int i = 0; i < numQuotes; i++) {
            QUOTE quote = quotes[(i + hideQuotes) % kMaxQuotes];
            if (lockclock < quote.messageTime) {
                int col = (Math.max(16 + (32 - nShade), 0) * 255 / 48); //48 - max shade (black)
                PaletteManager paletteManager = engine.getPaletteManager();
                byte palcol = engine.getPaletteManager().getFastColorLookup().getClosestColorIndex(paletteManager.getCurrentPalette().getBytes(), col, col, col);

                game.getFont(1).drawText(renderer, x + 1, y, quote.messageText, cfg.gHUDSize / 65536.0f, nShade, palcol, TextAlign.Left, Transparent.None, true);
                y += scale(yOffset, cfg.gHUDSize, 65536);
                nShade = BClipLow(nShade - 64 / numQuotes, -128);
            } else {
                numQuotes--;
                hideQuotes += 1;
                hideQuotes %= kMaxQuotes;
            }
        }
        if (nextY != 0) {
            nextY = nextTime * yOffset / 4;
        }
    }

    public static class QUOTE {
        public String messageText;
        public int messageTime;
    }

}
