// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.LSP.Factory.LSPRenderer;
import ru.m210projects.LSP.Types.PlayerStruct;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.LSP.Enemies.*;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.*;
import static ru.m210projects.LSP.Quotes.viewSetMessage;
import static ru.m210projects.LSP.Sounds.playsound;
import static ru.m210projects.LSP.View.TintPalette;
import static ru.m210projects.LSP.Weapons.shoot;

public class Sprites {

    public static void operatesprite(int dasprite) {
        Sprite spr = boardService.getSprite(dasprite);
        if (spr != null && spr.getLotag() == 2) {
            shoot(dasprite, spr.getX(), spr.getY(), spr.getZ(), spr.getAng(), 100, spr.getSectnum(), 9);
        }
    }

    public static int changeammo(int plr, int nWeapon, int value) {
        if (gPlayer[plr].nPlayerStatus == 5) {
            gPlayer[plr].nAmmo[nWeapon] = 160;
            return 0;
        }
        if (value <= 0) {
            gPlayer[plr].nAmmo[nWeapon] += (short) value;
            if (gPlayer[plr].nAmmo[nWeapon] < 0) {
                gPlayer[plr].nAmmo[nWeapon] = 0;
            }
            return 0;
        }
        if (gPlayer[plr].nAmmo[nWeapon] >= 160) {
            return 0;
        }
        gPlayer[plr].nAmmo[nWeapon] += (short) value;
        if (gPlayer[plr].nAmmo[nWeapon] > 160) {
            gPlayer[plr].nAmmo[nWeapon] = 160;
        }
        TintPalette(0, 16, 0);
        return 1;
    }

    public static int changemanna(int a1, int a2) {
        if (gPlayer[a1].nPlayerStatus == 5) {
            gPlayer[a1].nMana = 160;
            return 0;
        }
        if (a2 <= 0) {
            gPlayer[a1].nMana += (short) a2;
            if (gPlayer[a1].nMana < 0) {
                gPlayer[a1].nMana = 0;
            }
            return 0;
        }
        if (gPlayer[a1].nMana >= 160) {
            return 0;
        }
        gPlayer[a1].nMana += (short) a2;
        if (gPlayer[a1].nMana > 160) {
            gPlayer[a1].nMana = 160;
        }

        TintPalette(0, 4, 16);
        return 1;
    }

    public static boolean changehealth(int snum, int deltahealth) {
        LSPRenderer renderer = game.getRenderer();
        if (gPlayer[snum].nPlayerStatus == 5) {
            gPlayer[snum].nHealth = 160;
            return false;
        }

        boolean out = false;
        if (deltahealth <= 0) {
            if (gPlayer[snum].nHealth > 0) {
                TintPalette(16, 0, 0);
            }
            switch (nDifficult) {
                case 1:
                    gPlayer[snum].nHealth += (short) (deltahealth / 2);
                    break;
                case 3:
                    gPlayer[snum].nHealth += (short) (deltahealth * 2);
                    break;
                default:
                    gPlayer[snum].nHealth += (short) deltahealth;
                    break;
            }
        } else {
            if (gPlayer[snum].nHealth < 160) {
                gPlayer[snum].nHealth += (short) deltahealth;
                if (gPlayer[snum].nHealth > 160) {
                    gPlayer[snum].nHealth = 160;
                }
                out = true;
            }
        }

        if (gPlayer[snum].nHealth <= 0) {
            if ((engine.krand() & 1) != 0) {
                playsound(9);
            } else {
                playsound(10);
            }
            renderer.setVisibility(15);
            gPlayer[snum].nHealth = 0;
            return false;
        }

        if (gPlayer[snum].nHealth >= 25) {
            renderer.setVisibility(15);
            return out;
        }

        if (gPlayer[snum].nHealth < 8) {
            renderer.setVisibility(9);
        } else if (gPlayer[snum].nHealth < 18) {
            renderer.setVisibility(11);
        } else {
            renderer.setVisibility(13);
        }

        playsound(88);
        return out;
    }

    public static void checktouchsprite(int snum) {
        int sectnum = gPlayer[snum].sectnum;
        if (!boardService.isValidSprite(sectnum)) {
            return;
        }

        ListNode<Sprite> i = boardService.getSectNode(sectnum), nexti;
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int k = i.getIndex();
            if ((Math.abs(gPlayer[snum].x - spr.getX()) + Math.abs(gPlayer[snum].y - spr.getY()) < 512)) {
                int height = (spr.getZ() >> 8) - (engine.getTile(spr.getPicnum()).getHeight() >> 1);
                if (Math.abs((gPlayer[snum].z >> 8) - height) <= 40) {
                    switch (spr.getPicnum()) {
                        case 601:
                        case 602:
                            if (changehealth(snum, spr.getPicnum() == 601 ? 10 : 30)) {
                                viewSetMessage("Picked up " + (spr.getPicnum() == 601 ? "a Parasol" : "a Cookie"));
                                TintPalette(16, 16, 0);
                                playsound(51);
                                engine.deletesprite(k);
                            }
                            break;
                        case 603: // armor
                            if (changehealth(snum, 100)) {
                                viewSetMessage("Picked up an Armor");
                                TintPalette(16, 16, 0);
                                playsound(51);
                                playsound(51);
                                playsound(51);
                                engine.deletesprite(k);
                            }
                            break;
                        case 604:
                            if (changeammo(snum, 1, 12) == 0) {
                                break;
                            }

                            viewSetMessage("Picked up Shakens");
                            playsound(56);
                            engine.deletesprite(k);
                            break;
                        case 605: // balls
                            if (changeammo(snum, 2, 10) == 0) {
                                break;
                            }

                            viewSetMessage("Picked up Balls");
                            playsound(26);
                            engine.deletesprite(k);
                            break;
                        case 606:
                            if (changeammo(snum, 3, 10) == 0) {
                                break;
                            }

                            viewSetMessage("Picked up Pikes");
                            playsound(27);
                            engine.deletesprite(k);
                            break;
                        case 607:
                            if (changeammo(snum, 4, 8) == 0) {
                                break;
                            }

                            viewSetMessage("Picked up Shurikens");
                            playsound(32);
                            engine.deletesprite(k);
                            break;
                        case 608:
                            if (changeammo(snum, 5, 6) == 0) {
                                break;
                            }

                            viewSetMessage("Picked up Daggers");
                            playsound(35);
                            engine.deletesprite(k);
                            break;
                        case 609:
                            if (changeammo(snum, 6, 3) == 0) {
                                break;
                            }

                            viewSetMessage("Picked up Kunai");
                            playsound(90);
                            engine.deletesprite(k);
                            break;
                        case 700: // tree root1?
                        case 701: // tree root2?
                            if (changehealth(snum, spr.getPicnum() == 700 ? 50 : 32)) {
                                viewSetMessage("Picked up Lucky Charm");
                                TintPalette(16, 16, 0);
                                playsound(51);
                                engine.deletesprite(k);
                            }
                            break;
                        case 702: // chocolate
                            if (changehealth(snum, 16)) {
                                viewSetMessage("Picked up Po Chai pills");
                                TintPalette(16, 16, 0);
                                playsound(50);
                                engine.deletesprite(k);
                            }
                            break;
                        case 703: // flowers with roots
                            if (changehealth(snum, 8)) {
                                viewSetMessage("Picked up Herbs");
                                TintPalette(16, 16, 0);
                                playsound(50);
                                playsound(50);
                                playsound(50);
                                engine.deletesprite(k);
                            }
                            break;
                        case 705: // flower
                            if (changemanna(snum, 25) != 0) {
                                viewSetMessage("Picked up a Flower");
                                playsound(29);
                                engine.deletesprite(k);
                            }
                            break;
                        case 706: // mushroom
                            if (changemanna(snum, 12) != 0) {
                                viewSetMessage("Picked up a Mushroom");
                                playsound(29);
                                engine.deletesprite(k);
                            }
                            break;
                        case 707: // manna bottle
                            if (changemanna(snum, 5) != 0) {
                                viewSetMessage("Picked up a Mana bottle");
                                playsound(29);
                                engine.deletesprite(k);
                            }
                            break;
                        case 710: // blue book
                            if (gPlayer[snum].nAmmo[8] == 0) {
                                TintPalette(0, 4, 16);
                                viewSetMessage("Picked up a Blue book");
                                changemanna(snum, 25);
                                playsound(58);
                                engine.deletesprite(k);
                                gPlayer[snum].nAmmo[8] = 1;
                            }
                            break;
                        case 711: // brown book
                            if (gPlayer[snum].nAmmo[9] == 0) {
                                TintPalette(0, 4, 16);
                                viewSetMessage("Picked up a Brown book");
                                changemanna(snum, 35);
                                playsound(59);
                                engine.deletesprite(k);
                                gPlayer[snum].nAmmo[9] = 1;
                            }
                            break;
                        case 712: // stone
                            if (gPlayer[snum].nAmmo[10] == 0) {
                                TintPalette(0, 4, 16);
                                viewSetMessage("Picked up a Stone plate");
                                changemanna(snum, 50);
                                playsound(60);
                                engine.deletesprite(k);
                                gPlayer[snum].nAmmo[10] = 1;
                            }
                            break;
                        case 713: // wooden book
                            if (gPlayer[snum].nAmmo[11] == 0) {
                                TintPalette(0, 4, 16);
                                viewSetMessage("Picked up a Wooden slip");
                                changemanna(snum, 75);
                                playsound(61);
                                engine.deletesprite(k);
                                gPlayer[snum].nAmmo[11] = 1;
                            }
                            break;
                        case 714: // papirus
                            if (gPlayer[snum].nAmmo[12] == 0) {
                                TintPalette(0, 4, 16);
                                viewSetMessage("Picked up a Scroll");
                                changemanna(snum, 100);
                                playsound(62);
                                engine.deletesprite(k);
                                gPlayer[snum].nAmmo[12] = 1;
                            }
                            break;
                        case 720: // gold key
                        case 721: // bronze key
                        case 722: // silver key
                        case 723: // grey key
                        case 724: // green key
                        case 725: // red key
                            viewSetMessage("Picked up a Key");
                            TintPalette(0, 16, 0);
                            changehealth(snum, engine.krand() % 100 + 60);
                            changemanna(snum, engine.krand() % 100 + 60);
                            playsound(50);
                            playsound(29);
                            playsound(50);
                            playsound(29);
                            engine.deletesprite(k);
                            if (spr.getPicnum() == 725) {
                                for (short sec = 0; sec < boardService.getSectorCount(); sec++) {
                                    for (ListNode<Sprite> node = boardService.getSectNode(sec); node != null; node = node.getNext()) {
                                        switch (node.get().getPicnum()) {
                                            case 275:
                                            case 339:
                                            case BLUEDUDE:
                                            case GREENDUDE:
                                            case REDDUDE:
                                            case PURPLEDUDE:
                                            case YELLOWDUDE:
                                                PlayerStruct plr = gPlayer[snum];
                                                Sprite pspr = node.get();
                                                int kk = node.getIndex();

                                                int sin = EngineUtils.sin((int) plr.ang & 2047);
                                                int cos = EngineUtils.sin((int) (plr.ang + 512) & 2047);

                                                if (plr.sectnum >= 0
                                                        && (cos * (pspr.getX() - plr.x)) + (sin * (pspr.getY() - plr.y)) >= 0) {
                                                    if (engine.cansee(plr.x, plr.y, plr.z, plr.sectnum, pspr.getX(), pspr.getY(),
                                                            pspr.getZ() - (engine.getTile(pspr.getPicnum()).getHeight() << 7), pspr.getSectnum())) {
                                                        if (pspr.getPicnum() != 275 && pspr.getPicnum() != 339) {
                                                            enemydie(kk);
                                                        }
                                                        if (kk % 5 == 1) {
                                                            playsound((engine.krand() & 7) + 12, kk);
                                                        }
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }
                                changemanna(snum, -200);
                            }
                            break;
                        case 727: // gold coin

                            break;
                    }
                }
            }
            i = nexti;
        }
    }

    private static void moveprojectiles() {
        ListNode<Sprite> next;
        for (ListNode<Sprite> node = boardService.getStatNode(PROJECTILE); node != null; node = next) {
            int i = node.getIndex();
            next = node.getNext();
            Sprite projectileSprite = node.get();

            game.pInt.setsprinterpolate(i, projectileSprite);
            Sector sec = boardService.getSector(projectileSprite.getSectnum());
            if (sec == null) {
                continue;
            }

            int xvel, yvel, zvel;
            if (projectileSprite.getPicnum() != 1166 && projectileSprite.getPicnum() != 1156 && projectileSprite.getPicnum() != 1136 && projectileSprite.getPicnum() != 1116
                    && projectileSprite.getPicnum() != 1179 && projectileSprite.getPicnum() != 1192 && projectileSprite.getPicnum() != 1203 && projectileSprite.getPicnum() != 1214) {
                if (projectileSprite.getPicnum() == 1259) {
                    projectileSprite.setAng(projectileSprite.getAng() + 16);
                    xvel = EngineUtils.sin((projectileSprite.getAng() + 2560) & 0x7FF) >> 5;
                    yvel = EngineUtils.sin((projectileSprite.getAng() + 2048) & 0x7FF) >> 5;
                } else {
                    xvel = projectileSprite.getXvel();
                    yvel = projectileSprite.getYvel();
                }
                zvel = projectileSprite.getZvel();
            } else {
                projectileSprite.setZ(projectileSprite.getZ() + projectileSprite.getZvel());
                projectileSprite.setZvel(projectileSprite.getZvel() + 32 * TICSPERFRAME);
                int cz = sec.getCeilingz() + 512;
                if (cz > projectileSprite.getZ()) {
                    projectileSprite.setZ(cz);
                    projectileSprite.setZvel((short) -(projectileSprite.getZvel() >> 1));
                }

                int fz = sec.getFloorz() - 512;
                if (fz < projectileSprite.getZ()) {
                    projectileSprite.setZ(fz);
                    projectileSprite.setZvel((short) -(projectileSprite.getZvel() >> 1));
                }
                xvel = projectileSprite.getXvel();
                yvel = projectileSprite.getYvel();
                zvel = 0;
            }

            clipmoveboxtracenum = 1;

            Sprite blowspr = null;
            int hitmove = engine.movesprite(i, xvel, yvel, zvel, projectileSprite.getClipdist() << 2, 512, 512, CLIPMASK0, TICSPERFRAME);

            clipmoveboxtracenum = 3;

            if (hitmove != 0) {
                switch (projectileSprite.getPicnum()) {
                    case 1203: {
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum(1354);
                        blowspr.setShade(-24);
                        blowspr.setXrepeat(72);
                        blowspr.setYrepeat(72);
                        blowspr.setAng(0);
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag(80);
                        break;
                    }
                    case 1214: {
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum(1369);
                        blowspr.setShade(-24);
                        blowspr.setXrepeat(64);
                        blowspr.setYrepeat(64);
                        blowspr.setAng(projectileSprite.getAng());
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag(100);
                        break;
                    }
                    case 1225:
                    case 1226:
                    case 1227:
                    case 1228:
                    case 1229:
                    case 1230:
                    case 1231:
                    case 1232:
                    case 1233: {
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum((short) ((engine.krand() & 3) + 1374));
                        blowspr.setShade(-24);
                        blowspr.setXrepeat(48);
                        blowspr.setYrepeat(48);
                        blowspr.setAng(projectileSprite.getAng());
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag((short) ((engine.krand() & 0x7F) + 100));
                        break;
                    }
                    case 1259: {
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum(1328);
                        blowspr.setShade(-24);
                        blowspr.setXrepeat(72);
                        blowspr.setYrepeat(72);
                        blowspr.setAng(0);
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag((short) ((engine.krand() & 0x3F) + 12));
                        break;
                    }
                    case 1322:
                    case 2352: {
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum(2356);
                        blowspr.setShade(-23);
                        blowspr.setXrepeat(32);
                        blowspr.setYrepeat(32);
                        blowspr.setAng(0);
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag(63);
                        break;
                    }
                    case 1840: {
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum(1844);
                        blowspr.setShade(-23);
                        blowspr.setXrepeat(32);
                        blowspr.setYrepeat(32);
                        blowspr.setAng(projectileSprite.getAng());
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag(63);
                        break;
                    }
                    case 2608: {
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum(2612);
                        blowspr.setShade(-23);
                        blowspr.setXrepeat(24);
                        blowspr.setYrepeat(24);
                        blowspr.setAng(projectileSprite.getAng());
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag(63);
                        break;
                    }
                    case 2096: { // gray coin
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum(1364);
                        blowspr.setShade(-23);
                        blowspr.setXrepeat(32);
                        blowspr.setYrepeat(32);
                        blowspr.setAng(0);
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag(48);
                        break;
                    }
                    case 1116:
                    case 1126:
                    case 1136:
                    case 1146:
                    case 1156:
                    case 1166: {
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum(1364);
                        blowspr.setShade(-23);
                        blowspr.setXrepeat(48);
                        blowspr.setYrepeat(48);
                        blowspr.setAng(0);
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag(63);
                        break;
                    }
                    case 1179: {
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum(1358);
                        blowspr.setShade(-24);
                        blowspr.setXrepeat(128);
                        blowspr.setYrepeat(128);
                        blowspr.setAng(0);
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag(64);
                        break;
                    }
                    case 1192: {
                        int j = engine.insertsprite(projectileSprite.getSectnum(), EXPLOSION);
                        blowspr = boardService.getSprite(j);
                        if (blowspr == null) {
                            break;
                        }

                        blowspr.setX(projectileSprite.getX());
                        blowspr.setY(projectileSprite.getY());
                        blowspr.setZ(projectileSprite.getZ());
                        blowspr.setCstat(128);
                        blowspr.setPicnum(1345);
                        blowspr.setShade(-24);
                        blowspr.setXrepeat(72);
                        blowspr.setYrepeat(72);
                        blowspr.setAng(0);
                        blowspr.setXvel(0);
                        blowspr.setYvel(0);
                        blowspr.setZvel(0);
                        blowspr.setOwner(projectileSprite.getOwner());
                        blowspr.setLotag(64);
                        break;
                    }
                }

                final int nHitObject = hitmove & HIT_INDEX_MASK;
                switch (hitmove & HIT_TYPE_MASK) {
                    case HIT_SECTOR:
                    case HIT_WALL:
                        switch (projectileSprite.getPicnum()) {
                            case 1840:
                            case 1322:
                            case 2352:
                                playsound(39, i);
                                break;
                            case 1179:
                            case 1192:
                            case 1203:
                            case 1214:
                            case 1225:
                            case 1226:
                            case 1227:
                            case 1228:
                            case 1229:
                            case 1230:
                            case 1231:
                            case 1232:
                            case 1233:
                            case 1259:
                                playsound((engine.krand() & 7) + 37, i);
                                break;
                            case 1116:
                            case 1126:
                            case 1136:
                            case 1146:
                            case 1156:
                            case 1166:
                                playsound(49, i);
                                break;
                        }

                        if (projectileSprite.getPicnum() == 1259 && (hitmove & HIT_TYPE_MASK) != HIT_SECTOR) {
                            projectileSprite.setAng((short) ((projectileSprite.getAng() + 0x100) & 0x7FF));
                            projectileSprite.setZvel(projectileSprite.getZvel() - 2);
                            continue;
                        }

                        engine.deletesprite(i);
                        continue;
                    case HIT_SPRITE:
                        Sprite hitSprite = boardService.getSprite(nHitObject);
                        if (hitSprite == null) {
                            break;
                        }

                        if (hitSprite.getPicnum() >= 256 && hitSprite.getPicnum() < 320) {
                            switch (projectileSprite.getPicnum()) {
                                case 1116:
                                case 1126:
                                case 1136:
                                case 1146:
                                case 1156:
                                case 1166:
                                    engine.deletesprite(i);
                                    continue;
                                case 1179: // Check it
                                case 1192:
                                case 1203:
                                case 1214:
                                case 1225:
                                case 1226:
                                case 1227:
                                case 1228:
                                case 1229:
                                case 1230:
                                case 1231:
                                case 1232:
                                case 1233:
                                case 1259:
                                    playsound((engine.krand() & 7) + 37, i);
                                    hitSprite.setPicnum(hitSprite.getPicnum() + 64);
                                    hitSprite.setCstat(hitSprite.getCstat() & ~257);
                                    engine.deletesprite(i);
                                    continue;
                            }
                        }

                        if (projectileSprite.getOwner() != nHitObject) {
                            if (hitSprite.getPicnum() != REDDUDE && hitSprite.getPicnum() != BLUEDUDE
                                    && hitSprite.getPicnum() != GREENDUDE && hitSprite.getPicnum() != PURPLEDUDE
                                    && hitSprite.getPicnum() != YELLOWDUDE) {

                                for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                                    if (nHitObject == gPlayer[p].nSprite) {
                                        // if ( dword_516B4 != 0 ) XXX
                                        // sub_32E4C(2000, 20, 30);

                                        if (projectileSprite.getPicnum() == 2096) {
                                            // changepalette(palette2); XXX
                                            // word_58784[target] = 1;
                                            // dword_569DC[target] = engine.getTotalClock();

                                            gPlayer[p].nWeaponImpact = -72;
                                            gPlayer[p].nWeaponImpactAngle = (short) gPlayer[p].ang;
                                            playsound(7);
                                            changehealth(p, projectileSprite.getLotag());
                                            engine.deletesprite(i);
                                            continue;
                                        }

                                        if (blowspr != null) {
                                            switch (projectileSprite.getPicnum()) {
                                                case 1840:
                                                    blowspr.setPicnum(1844);
                                                    blowspr.setShade(-18);
                                                    blowspr.setXrepeat(24);
                                                    blowspr.setYrepeat(24);
                                                    blowspr.setLotag(64);
                                                    playsound(33);
                                                    gPlayer[p].nWeaponImpact = -100;
                                                    gPlayer[p].nWeaponImpactAngle = (short) gPlayer[p].ang;
                                                    break;
                                                case 2352:
                                                    blowspr.setPicnum(2356);
                                                    blowspr.setShade(-16);
                                                    blowspr.setXrepeat(32);
                                                    blowspr.setYrepeat(32);
                                                    blowspr.setLotag(160);
                                                    playsound(1);
                                                    gPlayer[p].nWeaponImpact = -100;
                                                    gPlayer[p].nWeaponImpactAngle = (short) gPlayer[p].ang;
                                                    break;
                                                case 2608:
                                                    blowspr.setPicnum(2612);
                                                    blowspr.setShade(-16);
                                                    blowspr.setXrepeat(32);
                                                    blowspr.setYrepeat(32);
                                                    blowspr.setLotag(160);
                                                    playsound(5);
                                                    gPlayer[p].nWeaponImpact = -100;
                                                    gPlayer[p].nWeaponImpactAngle = (short) gPlayer[p].ang;
                                                    break;
                                            }
                                            blowspr.setZ(projectileSprite.getZ() + 2048);
                                        }

                                        changehealth(p, projectileSprite.getLotag());
                                        engine.deletesprite(i);
                                        break;
                                    }
                                }

                                if (projectileSprite.getPicnum() != 1259) { // boomerang
                                    engine.deletesprite(i);
                                }
                                continue;
                            }
                        }


                        if (projectileSprite.getOwner() != nHitObject && projectileSprite.getPicnum() != 1322 // skull proj
                                && projectileSprite.getPicnum() != 1840 // greendude proj
                                && projectileSprite.getPicnum() != 2096 // gray coin (reddude)
                                && projectileSprite.getPicnum() != 2352 // purpdude proj
                                && projectileSprite.getPicnum() != 2608) // yelldude proj
                        {
                            switch (hitSprite.getPicnum()) {
                                case BLUEDUDE: // blue enemy
                                case GREENDUDE: // green enemy
                                case REDDUDE: // red enemy
                                case PURPLEDUDE: // purple enemy
                                case YELLOWDUDE: // yellow enemy
                                    for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                                        if ((projectileSprite.getOwner() - 4096) == p) {
                                            if (hitSprite.getLotag() > 0) {
                                                hitSprite.setLotag(hitSprite.getLotag() - (projectileSprite.getLotag() + gPlayer[p].nSecondWeaponDamage));
                                                gEnemyClock[nHitObject] = lockclock;

                                                if (hitSprite.getStatnum() == GUARD) {
                                                    engine.changespritestat((short) nHitObject, CHASE);
                                                }
                                            }
                                        }
                                    }

                                    if (hitSprite.getLotag() > 0) {
                                        switch (projectileSprite.getPicnum()) {
                                            case 1179:
                                            case 1192:
                                            case 1203:
                                            case 1214:
                                            case 1225:
                                            case 1226:
                                            case 1227:
                                            case 1228:
                                            case 1229:
                                            case 1230:
                                            case 1231:
                                            case 1232:
                                            case 1233:
                                            case 1259:
                                                switch (hitSprite.getPicnum()) {
                                                    case 1536:
                                                    case 2560:
                                                        playsound(12, nHitObject);
                                                        break;
                                                    case 1792:
                                                        playsound(68, nHitObject);
                                                        break;
                                                    case 2048:
                                                        playsound(70, nHitObject);
                                                        break;
                                                    case 2304:
                                                        playsound(73, nHitObject);
                                                        break;

                                                }
                                                break;
                                            case 1116:
                                            case 1126:
                                            case 1136:
                                            case 1146:
                                            case 1156:
                                            case 1166:
                                                switch (hitSprite.getPicnum()) {
                                                    case 1536:
                                                        playsound(65, nHitObject);
                                                        break;
                                                    case 1792:
                                                        playsound(66, nHitObject);
                                                        break;
                                                    case 2048:
                                                        playsound(70, nHitObject);
                                                        break;
                                                    case 2304:
                                                        playsound(72, nHitObject);
                                                        break;
                                                    case 2560:
                                                        playsound(68, nHitObject);
                                                        break;
                                                }
                                                break;
                                        }

                                        engine.deletesprite(i);
                                        continue;
                                    }

                                    switch (projectileSprite.getPicnum()) {
                                        case 1179:
                                        case 1192:
                                        case 1203:
                                        case 1214:
                                        case 1225:
                                        case 1226:
                                        case 1227:
                                        case 1228:
                                        case 1229:
                                        case 1230:
                                        case 1231:
                                        case 1232:
                                        case 1233:
                                        case 1259:
                                            switch (hitSprite.getPicnum()) {
                                                case 1536: // blue enemy
                                                case 2048: // red enemy
                                                    playsound(11, nHitObject);
                                                    break;
                                                case 1792: // green enemy
                                                case 2304: // purple enemy
                                                    playsound(16, nHitObject);
                                                    break;
                                                case 2560: // yellow enemy
                                                    playsound(19, nHitObject);
                                                    break;
                                            }
                                            break;
                                        case 1116:
                                        case 1126:
                                        case 1136:
                                        case 1146:
                                        case 1156:
                                        case 1166:
                                            switch (hitSprite.getPicnum()) {
                                                case 1536: // blue enemy
                                                case 2048: // red enemy
                                                    playsound(15, nHitObject);
                                                    break;
                                                case 1792: // green enemy
                                                case 2304: // purple enemy
                                                    playsound(18, nHitObject);
                                                    break;
                                                case 2560: // yellow enemy
                                                    playsound(11, nHitObject);
                                                    break;
                                            }
                                            break;
                                    }

                                    enemydie(nHitObject);
                            }
                        }
                }

                engine.deletesprite(i);
            }
        }
    }

    public static void statuslistcode() {
        moveprojectiles();
        moveenemies();

        ListNode<Sprite> i = boardService.getStatNode(SHOTSPARK), nexti;
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            game.pInt.setsprinterpolate(i.getIndex(), spr);

            spr.setZ(spr.getZ() - (TICSPERFRAME << 6));
            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                engine.deletesprite(i.getIndex());
            }

            i = nexti;
        }

        i = boardService.getStatNode(4); // unused?
        while (i != null) {
            nexti = i.getNext();

            Sprite spr = i.get();
            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            spr.setPicnum((short) (((63 - spr.getLotag()) >> 4) + 144));
            if (spr.getLotag() < 0) {
                engine.deletesprite(i.getIndex());
            }

            i = nexti;
        }

        i = boardService.getStatNode(EXPLOSION);
        while (i != null) {
            nexti = i.getNext();
            Sprite ispr = i.get();

            short sectnum = ispr.getSectnum();
            if (!cfg.bOriginal && ispr.getPicnum() != 1364) {
                for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = node.getNext()) {
                    Sprite spr = node.get();
                    int sprIndex = node.getIndex();
                    switch (spr.getPicnum()) {
                        case PLAYER:
                        case BLUEDUDE:
                        case GREENDUDE:
                        case REDDUDE:
                        case PURPLEDUDE:
                        case YELLOWDUDE:
                            int dx = ispr.getX() - spr.getX();
                            int dy = ispr.getY() - spr.getY();
                            int dist = klabs(dx) + klabs(dy);
                            if (dist <= ispr.getXrepeat() << 3) {

                                if (engine.cansee(ispr.getX(), ispr.getY(), ispr.getZ(), sectnum, spr.getX(),
                                        spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                                        spr.getSectnum())) {
                                    if (spr.getPicnum() != PLAYER) {
                                        if (spr.getStatnum() == GUARD) {
                                            engine.changespritestat(sprIndex, CHASE);
                                        }
                                        spr.setLotag(spr.getLotag() - 1);
                                        if (spr.getLotag() <= 0) {
                                            if (spr.getPicnum() == PURPLEDUDE || spr.getPicnum() == BLUEDUDE) {
                                                playsound(14, sprIndex);
                                            } else if (spr.getPicnum() == YELLOWDUDE) {
                                                playsound(18, sprIndex);
                                            } else {
                                                playsound(17, sprIndex);
                                            }
                                            enemydie(sprIndex);
                                        }
                                    } else {
                                        for (short p = connecthead; p >= 0; p = connectpoint2[p]) {
                                            if (sprIndex == gPlayer[p].nSprite) {
                                                if (gPlayer[p].nPlayerStatus != 5) {
                                                    gPlayer[p].nHealth--;
                                                    TintPalette(3, 0, 0);
                                                }
                                                gPlayer[p].nWeaponImpact = -32;
                                                gPlayer[p].nWeaponImpactAngle = (short) EngineUtils.getAngle(dx, dy);
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }

            ispr.setLotag(ispr.getLotag() - TICSPERFRAME);
            if (ispr.getLotag() < 0) {
                engine.deletesprite(i.getIndex());
            }

            i = nexti;
        }

        i = boardService.getStatNode(7); // unused?
        while (i != null) {
            nexti = i.getNext();

            Sprite spr = i.get();
            game.pInt.setsprinterpolate(i.getIndex(), spr);

            spr.setX(spr.getX() + (TICSPERFRAME * spr.getXvel() >> 4));
            spr.setY(spr.getY() + (TICSPERFRAME * spr.getYvel() >> 4));
            spr.setZ(spr.getZ() + (TICSPERFRAME * spr.getZvel() >> 4));
            spr.setZvel(spr.getZvel() + (TICSPERFRAME << 7));

            Sector sec = boardService.getSector(spr.getSectnum());
            if (sec != null) {
                if ((sec.getCeilingz() + 1024) > spr.getZ()) {
                    spr.setZ(sec.getCeilingz() + 1024);
                    spr.setZvel((short) -(spr.getZvel() >> 1));
                }

                if (sec.getFloorz() - 1024 < spr.getZ()) {
                    spr.setZ(sec.getFloorz() - 1024);
                    spr.setZvel((short) -(spr.getZvel() >> 1));
                }

                spr.setXrepeat((short) (spr.getLotag() >> 2));
                spr.setYrepeat((short) (spr.getLotag() >> 2));
                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    engine.deletesprite(i.getIndex());
                }
            }

            i = nexti;
        }

    }
}
