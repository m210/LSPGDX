// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP;

import com.badlogic.gdx.audio.Music;
import ru.m210projects.Build.Architecture.common.audio.BuildAudio;
import ru.m210projects.Build.Architecture.common.audio.SoundData;
import ru.m210projects.Build.Architecture.common.audio.Source;
import ru.m210projects.Build.Architecture.common.audio.SourceListener;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.LSP.Factory.MusicEntry;
import ru.m210projects.LSP.Types.VOCDecoder;

import java.nio.ByteBuffer;

import static ru.m210projects.Build.Gameutils.BClipLow;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.LSP.Globals.gPlayer;
import static ru.m210projects.LSP.Globals.screenpeek;
import static ru.m210projects.LSP.Main.*;

public class Sounds {

    public static final SoundResource[] SoundBufs = new SoundResource[100];
    public static int currChannel;
    public static ActiveSound[] sActiveSound;
    public static int currTrack = -1;
    public static Music currMusic = null;
    public static int currSong = -1;

    public static BuildAudio audio;

    public static void sndInit() {
        Console.out.println("Initializing sound system");
        cfg.setAudioDriver(cfg.getAudioDriver());
        cfg.setMidiDevice(cfg.getMidiDevice());
        Sounds.audio = cfg.getAudio();
        Sounds.audio.registerDecoder("VOC", new VOCDecoder());

        sActiveSound = new ActiveSound[cfg.getMaxvoices()];
        for (int i = 0; i < sActiveSound.length; i++) {
            sActiveSound[i] = new ActiveSound();
        }
        currChannel = 0;
    }

    public static void stopallsounds() {
        currChannel = 0;
        stopAllSounds();
        for (ActiveSound activeSound : sActiveSound) {
            activeSound.stop();
        }
    }

    public static void stopmusic() {
        if (currMusic != null) {
            currMusic.stop();
        }

        currTrack = -1;
        currMusic = null;
    }

    public static void startmusic(int num) {
        if (cfg.isMuteMusic() || (currMusic != null && currMusic.isPlaying() && currSong != -1 && currSong == num)) {
            return;
        }

        if (cfg.getMusicType() == 1 && game.currentDef != null) { //music from def file
            String himus = game.currentDef.audInfo.getDigitalInfo(Integer.toString(num));
            if (himus != null) {
                stopmusic();
                if ((currMusic = newMusic(himus)) != null) {
                    currSong = num;
                    System.out.println("Play ogg music " + num);
                    currMusic.setLooping(true);
                    currMusic.play();
                    return;
                }
            }
        }

        stopmusic();
        if ((currMusic = newMusic(num + 103)) != null) {
            System.out.println("Play midi music " + num);
            currSong = num;
            currMusic.setLooping(true);
            currMusic.play();
        }
    }

    private static boolean loadsound(int num) {
        Entry entry = game.cache.getEntry(Integer.toString(num + 3), false);
        if (!entry.exists()) {
            Console.out.println("Sound " + num + " not found.", OsdColor.RED);
            return false;
        }

        SoundData soundData = audio.getSoundDecoder("VOC").decode(entry);
        if (soundData == null) {
            Console.out.println("Could not open sound " + num, OsdColor.RED);
            soundData = new SoundData(8000, 1, 8, ByteBuffer.allocateDirect(0));
        }

        SoundBufs[num] = new SoundResource(soundData, num);
        return true;
    }

    private static ActiveSound preparesound(int num, int priority) {
        if (cfg.isNoSound() || num < 0 || num >= SoundBufs.length) {
            return null;
        }

        if (SoundBufs[num] == null && !loadsound(num)) {
            return null;
        }

        int channel = (currChannel++) & (sActiveSound.length - 1);
        ActiveSound pSound = sActiveSound[channel];
        SoundResource res = SoundBufs[num];
        Source hVoice = newSound(res.getBuffer(), res.getRate(), res.getBits(), priority);
        if (hVoice != null) {
            hVoice.setListener(pSound);
            pSound.pHandle = hVoice;
            pSound.soundnum = channel;

            return pSound;
        }

        return null;
    }

    public static void playsound(int num) {
        ActiveSound pSound = preparesound(num, 256);
        if (pSound != null) {
            pSound.spr = -1;
            pSound.pHandle.play(1.0f);
        }
    }

    public static void playsound(int num, int spriteid) {
        int vol = calcvolume(spriteid);
        if (vol == 0) {
            return;
        }

        ActiveSound pSound = preparesound(num, vol);
        if (pSound != null) {
            pSound.spr = spriteid;
            pSound.pHandle.play(vol / 255.0f);
        }
    }

    public static int calcvolume(int spr) {
        Sprite pSprite = boardService.getSprite(spr);
        if (pSprite == null) {
            return 0;
        }

        int dist = klabs(gPlayer[screenpeek].x - pSprite.getX()) + klabs(gPlayer[screenpeek].y - pSprite.getY());
        int vol = BClipLow(65536 - (dist << 2), 0);
        if (dist < 1500) {
            vol = 65536;
        }
        return BClipRange((int) ((vol / 65536.0f) * 255), 0, 255);
    }

    public static void updatesounds() {
        if (cfg.isNoSound()) {
            return;
        }

        audio.setListener(gPlayer[screenpeek].x, gPlayer[screenpeek].z >> 4, gPlayer[screenpeek].y, (int) gPlayer[screenpeek].ang);
        for (ActiveSound activeSound : sActiveSound) {
            if (activeSound.pHandle == null) {
                continue;
            }

            if (!activeSound.pHandle.isActive()) {
                activeSound.onStop();
                continue;
            }

            int spr = activeSound.spr;
            if (spr != -1) {
                Sprite pSprite = boardService.getSprite(spr);
                if (pSprite != null) {
                    activeSound.pHandle.setPosition(pSprite.getX(), pSprite.getZ() >> 4, pSprite.getY());
                    activeSound.pHandle.setVolume(calcvolume(spr) / 255.0f);
                }
            }
        }
    }

    public static void sndHandlePause(boolean gPaused) {
        if (gPaused) {
            if (currMusic != null) {
                currMusic.pause();
            }
            stopAllSounds();
        } else {
            if (!cfg.isMuteMusic() && currMusic != null) {
                currMusic.play();
            }
        }
    }

    public static void stopAllSounds() {
        audio.stopAllSounds();
    }

    public static Source newSound(ByteBuffer buffer, int rate, int bits, int priority) {
        return (Source) audio.newSound(buffer, rate, bits, priority);
    }

    public static Music newMusic(int index) {
        Entry entry = game.cache.getEntry(Integer.toString(index), false);
        if (entry.exists()) {
            return audio.newMusic(new MusicEntry(entry, "mid"));
        }
        return null;
    }

    public static Music newMusic(String filePath) {
        Entry entry = game.cache.getEntry(filePath, true);
        return audio.newMusic(entry);
    }

    public static class SoundResource {

        public final int num;
        public final SoundData data; // pointer to voc data

        public SoundResource(SoundData soundData, int num) {
            this.data = soundData;
            this.num = num;
        }

        public int getRate() {
            return data.getRate();
        }

        public ByteBuffer getBuffer() {
            return data.getData();
        }

        public int getBits() {
            return data.getBits();
        }
    }

    public static class ActiveSound implements SourceListener {
        public Source pHandle;
        public int spr;
        public int soundnum;

        public void stop() {
            if (pHandle != null) {
                pHandle.stop();
            }
            pHandle = null;
            spr = soundnum = -1;
        }

        @Override
        public void onStop() {
            stop();
        }
    }

}
