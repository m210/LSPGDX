package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class SectorVer4 extends Sector {

    @Override
    public Sector readObject(InputStream is) throws IOException {
        this.setWallptr(StreamUtils.readShort(is));
        this.setWallnum(StreamUtils.readShort(is));
        this.setCeilingstat(StreamUtils.readByte(is));
        this.setCeilingxpanning(StreamUtils.readByte(is));
        this.setCeilingypanning(StreamUtils.readByte(is));
        this.setCeilingshade(StreamUtils.readByte(is));
        this.setCeilingz(StreamUtils.readInt(is));
        this.setCeilingpicnum(StreamUtils.readShort(is));
        this.setCeilingheinum(max(min(StreamUtils.readShort(is) << 5, Short.MAX_VALUE), Short.MIN_VALUE));
        this.setFloorstat(StreamUtils.readByte(is));
        this.setFloorxpanning(StreamUtils.readByte(is));
        this.setFloorypanning(StreamUtils.readByte(is));
        this.setFloorshade(StreamUtils.readByte(is));
        this.setFloorz(StreamUtils.readInt(is));
        this.setFloorpicnum(StreamUtils.readShort(is));
        this.setFloorheinum(max(min(StreamUtils.readShort(is) << 5, Short.MAX_VALUE), Short.MIN_VALUE));
        this.setLotag(StreamUtils.readShort(is));
        this.setHitag(StreamUtils.readShort(is));
        this.setExtra(-1);

        return this;
    }
}
