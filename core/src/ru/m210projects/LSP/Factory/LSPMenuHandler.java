// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Factory;


import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuRendererSettings;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.LSP.Fonts.MenuFont;
import ru.m210projects.LSP.Menus.MenuInterfaceSet;
import ru.m210projects.LSP.Screens.MenuScreen;

import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Pragmas.scale;
import static ru.m210projects.LSP.GdxResources.*;

public class LSPMenuHandler extends MenuHandler {

    public static final int MAIN = 0;
    public static final int GAME = 1;
    public static final int LOADGAME = 3;
    public static final int SAVEGAME = 4;
    public static final int QUIT = 6;
    public static final int HELP = 7;
    public static final int AUDIOSET = 8;
    public static final int CONTROLSET = 9;
    public static final int OPTIONS = 10;
    public static final int COLORCORR = 11;
    public static final int CREDITS = 12;
    public static final int ADVERTISING = 13;
    public static final int LASTSAVE = 14;
    public static final int CORRUPTLOAD = 15;
    private final Engine engine;
    private final BuildGame app;

    public LSPMenuHandler(BuildGame app) {
        super(app);
        mMenus = new BuildMenu[16];
        this.engine = app.pEngine;
        this.app = app;
    }

    @Override
    public void mDrawMenu() {
        if (!(app.getScreen() instanceof MenuScreen) && !(app.pMenu.getCurrentMenu() instanceof BuildMenuList) && !(app.pMenu.getCurrentMenu() instanceof MenuInterfaceSet)) {
            int tile = 611;
            Renderer renderer = game.getRenderer();
            ArtEntry pic = renderer.getTile(tile);

            int xdim = renderer.getWidth();
            int ydim = renderer.getHeight();

            float kt = xdim / (float) ydim;
            float kv = pic.getWidth() / (float) pic.getHeight();
            float scale;
            if (kv >= kt) {
                scale = (ydim + 1) / (float) pic.getHeight();
            } else {
                scale = (xdim + 1) / (float) pic.getWidth();
            }

            renderer.rotatesprite(0, 0, (int) (scale * 65536), 0, tile, 127, 0, 8 | 16 | 1);
        }

        super.mDrawMenu();
    }

    @Override
    public int getShade(MenuItem item) {
        if (item != null && item.getClass().getSuperclass().equals(MenuSlider.class) && !item.isEnabled()) {
            return -127;
        }

        if (item != null && item.isFocused()) {
            return 16 - (engine.getTotalClock() & 0x3F);
        }
        return 0;
    }

    @Override
    public int getPal(Font font, MenuItem item) {
        if (item != null) {
            if (item.isFocused()) {
                return 70;
            }

            if (!item.isEnabled()) {
                return 96;
            }

            if (font != null && font.getClass().equals(MenuFont.class)) {
                return item.pal;
            }
            return 228;
        }

        return 228;
    }

    @Override
    public void mDrawMouse(int x, int y) {
        if (!app.pCfg.isMenuMouse()) {
            return;
        }

        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        int zoom = scale(0x10000, ydim, 200);
        int czoom = mulscale(0x8000, mulscale(zoom, app.pCfg.getgMouseCursorSize(), 16), 16);
        int xoffset = 0;
        int yoffset = 0;
        int ang = 0;

        renderer.rotatesprite((x + xoffset) << 16, (y + yoffset) << 16, czoom, ang, MOUSECURSOR, 0, 0, 8);
    }

    @Override
    public void mDrawBackButton() {
        if (!app.pCfg.isMenuMouse()) {
            return;
        }

        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();

        int zoom = scale(BACKSCALE, ydim, 200);
        if (mCount > 1) {
            //Back button
            ArtEntry pic = engine.getTile(BACKBUTTON);

            int shade = 4 + mulscale(16, EngineUtils.sin((20 * engine.getTotalClock()) & 2047), 16);
            renderer.rotatesprite(0, (ydim - mulscale(pic.getHeight(), zoom, 16)) << 16, zoom, 0, BACKBUTTON, shade, 0, 8 | 16, 0, 0, mulscale(zoom, pic.getWidth() - 1, 16), ydim - 1);
        }
    }

    @Override
    public boolean mCheckBackButton(int mx, int my) {
        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();

        int zoom = scale(BACKSCALE, ydim, 200);
        ArtEntry pic = engine.getTile(BACKBUTTON);

        int size = mulscale(pic.getWidth(), zoom, 16);
        int bx = 0;
        int by = ydim - mulscale(pic.getHeight(), zoom, 16);
        if (mx >= bx && mx < bx + size) {
            return my >= by && my < by + size;
        }
        return false;
    }

    @Override
    public void mSound(MenuItem item, MenuOpt opt) {
        /* nothing */
    }

    @Override
    public void mPostDraw(MenuItem item) {
        Renderer renderer = game.getRenderer();
        if (item.isFocused()) {
            int scale = 32768;
            int xoff = 0, yoff = 6;

            if (item instanceof MenuList) {
                if (item instanceof MenuResolutionList) {
                    MenuList list = (MenuList) item;

                    int px = list.x;
                    scale = 24000;

                    int focus = list.l_nFocus;
                    if (focus == -1 || focus < list.l_nMin || focus >= list.l_nMin + list.rowCount) {
                        return;
                    }

                    int py = list.y + (focus - list.l_nMin) * (list.mFontOffset());
                    xoff = 35;
                    yoff = 3;
                    renderer.rotatesprite((px + xoff) << 16, (py + yoff) << 16, scale, 0, 626, 0, 0, 10);
                } else if (item instanceof MenuKeyboardList || item instanceof MenuJoyList) {
                    MenuList list = (MenuList) item;
                    int px = list.x;
                    int py = list.y + (list.l_nFocus - list.l_nMin) * (list.mFontOffset());
                    scale = 24000;
                    xoff = -10;
                    yoff = 3;

                    int focus = list.l_nFocus;
                    if (focus == -1 || focus < list.l_nMin || focus >= list.l_nMin + list.rowCount) {
                        return;
                    }

                    renderer.rotatesprite((px + xoff) << 16, (py + yoff) << 16, scale, 0, 626, 0, 0, 10);

                }
            } else {
                int px = item.x;
                int py = item.y;


                if (item.align == 1) {
                    xoff = 320 >> 2;
                    if (item.font != app.getFont(2)) {
                        xoff -= 15;
                    }
                }

                if (item.font != app.getFont(2)) {
                    xoff -= 10;
                    yoff -= 3;
                    scale = 24000;
                }

                if (item instanceof MenuVariants) {
                    xoff += 10;
                    yoff += 15;
                }
                renderer.rotatesprite((px + xoff) << 16, (py + yoff) << 16, scale, 0, 626, 0, 0, 10);
            }
        }
    }

}
