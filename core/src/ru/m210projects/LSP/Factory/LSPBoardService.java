package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.SpriteMap;
import ru.m210projects.Build.Types.collections.ValueSetter;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.Engine.*;

public class LSPBoardService extends BoardService {

    public LSPBoardService() {
        registerBoard(4, SectorVer4.class, WallVer4.class, SpriteVer4.class);
    }

    @Override
    protected Board loadBoard(Entry entry) throws IOException {
        try (InputStream is = entry.getInputStream()) {
            int version = StreamUtils.readInt(is);
            if (version != 4) {
                is.close();
                return super.loadBoard(entry);
            }

            BuildPos startPos = new BuildPos(StreamUtils.readInt(is),
                    StreamUtils.readInt(is),
                    StreamUtils.readInt(is),
                    StreamUtils.readShort(is),
                    StreamUtils.readShort(is));

            List<Sprite> sprites = new ArrayList<>();
            Constructor<? extends Sector> sectorConstructor = versionToSectorClass.get(version).getConstructor();
            Constructor<? extends Wall> wallConstructor = versionToWallClass.get(version).getConstructor();
            Constructor<? extends Sprite> spriteConstructor = versionToSpriteClass.get(version).getConstructor();


            int numSectors = StreamUtils.readShort(is);
            int numWalls = StreamUtils.readShort(is);
            int numSprites = StreamUtils.readShort(is);


            Sector[] sectors = new Sector[numSectors];
            for (int i = 0; i < sectors.length; i++) {
                sectors[i] = sectorConstructor.newInstance().readObject(is);
            }

            Wall[] walls = new Wall[numWalls];
            for (int i = 0; i < walls.length; i++) {
                walls[i] = wallConstructor.newInstance().readObject(is);
            }

            for (int i = 0; i < numSprites; i++) {
                sprites.add(spriteConstructor.newInstance().readObject(is));
            }

            return new Board(startPos, sectors, walls, sprites);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void initSpriteLists(Board board) {
        List<Sprite> sprites = board.getSprites();
        this.spriteStatMap = createSpriteMap(MAXSTATUS, sprites, MAXSPRITES, Sprite::setStatnum);
        this.spriteSectMap = createSpriteMap(MAXSECTORS, sprites, MAXSPRITES, Sprite::setSectnum);
    }

    @Override
    protected SpriteMap createSpriteMap(int listCount, List<Sprite> spriteList, int spriteCount, ValueSetter<Sprite> valueSetter) {
        return new SpriteMap(listCount, spriteList, spriteCount, valueSetter) {
            @Override
            protected Sprite getInstance() {
                return new SpriteVer4();
            }
        };
    }
}
