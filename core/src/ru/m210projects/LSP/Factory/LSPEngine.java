// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Tables;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.TileManager;

import static ru.m210projects.LSP.Globals.TICSPERFRAME;
import static ru.m210projects.LSP.Main.*;

public class LSPEngine extends Engine {

    public static String randomStack = "";


    public LSPEngine(BuildGame game) throws Exception {
        super(game);
        inittimer(game.pCfg.isLegacyTimer(), 120, TICSPERFRAME);
    }

    @Override
    public int krand() {
//		if (gDemoScreen != null && gDemoScreen.demfile != null /*&& conditionCheck()*/) {
//			StringBuilder sb = new StringBuilder();
//			randomStack += "\r\n";
//			randomStack += "bseed: " + randomseed;
//			for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
//				if (element.getMethodName().equalsIgnoreCase("ProcessFrame")) {
//					break;
//				}
//				sb.append("\t").append(element.toString());
//				sb.append("\r\n");
//			}
//			randomStack += sb.toString();
//		}
        return super.krand();
    }

    @Override
    protected TileManager loadTileManager() {
        return new LSPTileManager();
    }

    @Override
    public LSPTileManager getTileManager() {
        return (LSPTileManager) super.getTileManager();
    }

    @Override
    public PaletteManager loadpalette() throws Exception { // jfBuild
        PaletteManager paletteManager = new LSPPaletteManager(this, game.getCache().getEntry("palette.dat", true));

        byte[] remapbuf = new byte[256];
        remapbuf[56] = remapbuf[57] = remapbuf[58] = 96;
        paletteManager.makePalookup(96, remapbuf, 0, 0, 0, 1); //disabled font

        remapbuf[56] = remapbuf[57] = remapbuf[58] = (byte) 227;
        paletteManager.makePalookup(228, remapbuf, 0, 0, 0, 1); //font color

        remapbuf[56] = remapbuf[57] = remapbuf[58] = (byte) 17; //choosed font
        remapbuf[227] = 17; //pink
        remapbuf[163] = (byte) 174;
        remapbuf[0] = 28;
        paletteManager.makePalookup(70, remapbuf, 0, 0, 0, 1);

        return paletteManager;
    }

    @Override
    protected BoardService createBoardService() {
        return new LSPBoardService();
    }

    public int movesprite(int spritenum, int dx, int dy, int dz, int clipdist, int ceildist, int flordist, int cliptype, int vel) {
        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null) {
            return 0;
        }

        game.pInt.setsprinterpolate(spritenum, spr);

        int zoffs = 0;
        if ((spr.getCstat() & 128) == 0) {
            zoffs = -((engine.getTile(spr.getPicnum()).getHeight() * spr.getYrepeat()) << 1);
        }

        int dasectnum = spr.getSectnum();
        int daz = spr.getZ() + zoffs;

        int retval = clipmove(spr.getX(), spr.getY(), daz, dasectnum, (long) vel * dx << 11, (long) vel * dy << 11, clipdist, ceildist, flordist, cliptype);
        if (clipmove_sectnum != -1) {
            spr.setX(clipmove_x);
            spr.setY(clipmove_y);
            dasectnum = clipmove_sectnum;
        }

        if (dasectnum != spr.getSectnum() && dasectnum >= 0) {
            engine.changespritesect(spritenum, dasectnum);
        }

        int oldcstat = spr.getCstat();
        spr.setCstat(spr.getCstat() & ~1);
        engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), clipdist, cliptype);
        spr.setCstat(oldcstat);

        daz = spr.getZ() + zoffs + (vel * dz >> 3);
        if ((daz <= zr_ceilz) || (daz > zr_florz)) {
            if (retval != 0) {
                return (retval);
            }
            return (HIT_SECTOR | dasectnum);
        }
        spr.setZ((daz - zoffs));
        return (retval);
    }


    @Override
    public int clipmove(int x, int y, int z, int sectnum, long xvect, long yvect, int walldist, int ceildist,
                        int flordist, int cliptype) {

        clipmove.setTraceNum(clipmoveboxtracenum);
        return super.clipmove(x, y, z, sectnum, xvect, yvect, walldist, ceildist, flordist, cliptype);
    }

    @Override
    protected Tables loadtables() throws Exception {
        return new LSPTables(game.getCache().getEntry("tables.dat", true));
    }

}
