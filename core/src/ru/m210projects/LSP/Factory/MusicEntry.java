package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;

import java.io.IOException;
import java.io.InputStream;

public class MusicEntry implements Entry {

    private final Entry entry;
    private final String extension;

    public MusicEntry(Entry entry, String extension) {
        this.entry = entry;
        this.extension = extension;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return entry.getInputStream();
    }

    @Override
    public String getName() {
        return entry.getName();
    }

    @Override
    public String getExtension() {
        return extension;
    }

    @Override
    public long getSize() {
        return entry.getSize();
    }

    @Override
    public boolean exists() {
        return entry.exists();
    }

    @Override
    public Group getParent() {
        return entry.getParent();
    }

    @Override
    public void setParent(Group parent) {
        entry.setParent(parent);
    }
}
