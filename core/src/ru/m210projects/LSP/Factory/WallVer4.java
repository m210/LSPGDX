package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

public class WallVer4 extends Wall {

    @Override
    public Wall readObject(InputStream is) throws IOException {
        this.setX(StreamUtils.readInt(is));
        this.setY(StreamUtils.readInt(is));
        this.setPoint2(StreamUtils.readShort(is));
        this.setCstat(StreamUtils.readByte(is));
        this.setShade(StreamUtils.readByte(is));
        this.setXrepeat(StreamUtils.readByte(is));
        this.setYrepeat(StreamUtils.readByte(is));
        this.setXpanning(StreamUtils.readByte(is));
        this.setYpanning(StreamUtils.readByte(is));
        this.setPicnum(StreamUtils.readShort(is));
        this.setOverpicnum(StreamUtils.readShort(is));
        this.setNextsector(StreamUtils.readShort(is));
        this.setNextwall(StreamUtils.readShort(is));
        StreamUtils.readShort(is); // nextsector2
        StreamUtils.readShort(is); // nextwall2
        this.setLotag(StreamUtils.readShort(is));
        this.setHitag(StreamUtils.readShort(is));
        this.setExtra(-1);

        return this;
    }
}
