package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.Types.TileManager;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;

import static ru.m210projects.LSP.Main.game;

public class LSPTileManager extends TileManager {

    public static final String sIntroTiles = "L0ART000.DAT";
    public static final String sGameTiles = "L1ART000.DAT";

    public LSPTileManager() {
        tilesPath = sIntroTiles;
    }

    public void setTilesPath(int mapnum) {
        String newPath = sIntroTiles;
        if (mapnum != 0) {
            newPath = sGameTiles;
        }

        if (newPath.equals(tilesPath)) {
            return;
        }

        int tile = 770;
        ArtEntry pic = getTile(tile);
//        byte[] data = null;
//        int sizx = 0, sizy = 0;
//        if (newPath.equals(sGameTiles)) {
//            if (engine.loadtile(tile) != null) {
//                sizx = pic.getWidth();
//                sizy = pic.getHeight();
//                data = new byte[sizx * sizy];
//                System.arraycopy(pic.data, 0, data, 0, data.length);
//            }
//        }

        System.err.println("Reset to default resources");
        tilesPath = newPath;
        loadpics(game.getCache());

        DynamicArtEntry entry = allocatepermanenttile(tile, pic.getWidth(), pic.getHeight());
        entry.copyData(pic.getBytes());

        if (newPath.equals(sGameTiles)) {
            game.setDefs(new DefScript(game.baseDef, null));
        } else {
            game.setDefs(game.baseDef);
        }
    }
}
