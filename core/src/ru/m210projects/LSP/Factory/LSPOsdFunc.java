// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Factory;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.ScreenAdapters.InitScreen;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.DefaultOsdFunc;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.LSP.GdxResources.LOGO;
import static ru.m210projects.LSP.Main.game;

public class LSPOsdFunc extends DefaultOsdFunc {

    public LSPOsdFunc() {
        super(game.getRenderer());

        BGTILE = 770;
        BGCTILE = LOGO;
        BORDTILE = 0;
        SHADE = 20;

        BITSTH = 1 + 8 + 16;

        OsdColor.RED.setPal(175);
        OsdColor.BLUE.setPal(197);
        OsdColor.YELLOW.setPal(63); //161
        OsdColor.BROWN.setPal(236);
        OsdColor.GREEN.setPal(120);
    }

    @Override
    protected Font getFont() {
        return EngineUtils.getLargeFont();
    }

    @Override
    public void showOsd(boolean captured) {
        super.showOsd(captured);
        if (!(game.getScreen() instanceof InitScreen) && !game.pMenu.gShowMenu) {
            Gdx.input.setCursorCatched(!captured);
        }
    }
}
