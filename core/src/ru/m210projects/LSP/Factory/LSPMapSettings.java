// This file is part of LSPGDX.
// Copyright (C) 2021  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Factory;


import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Render.DefaultMapSettings;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Wall;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.LSP.Globals.followmode;
import static ru.m210projects.LSP.Globals.gPlayer;
import static ru.m210projects.LSP.Main.cfg;
import static ru.m210projects.LSP.Main.engine;

public class LSPMapSettings extends DefaultMapSettings {

    public LSPMapSettings(BoardService boardService) {
        super(boardService);
    }

    @Override
    public int getWallColor(int w, int i) {
        Wall wal = boardService.getWall(w);
        if (wal != null && boardService.isValidSector(wal.getNextsector())) // red wall
        {
            return 175;
        }

        int walcol = 4;
        Sector sec = boardService.getSector(i);
        if (sec == null) {
            return walcol;
        }

        switch (sec.getLotag()) {
            case 97:
            case 98:
            case 99:
                if (!cfg.bShowExit) {
                    return -1;
                }

                walcol = 120;
                if (sec.getLotag() == 99) // right exit
                {
                    walcol += (engine.getTotalClock() & 10);
                }
                break;
        }
        return walcol; // white wall
    }

    @Override
    public boolean isSpriteVisible(MapView view, int index) {
        return false;
    }

    @Override
    public int getPlayerPicnum(int player) {
        return -1;
    }

    @Override
    public int getPlayerSprite(int player) {
        return gPlayer[player].nSprite;
    }

    @Override
    public boolean isScrollMode() {
        return followmode;
    }

    @Override
    public int getViewPlayer() {
        return myconnectindex;
    }
}
