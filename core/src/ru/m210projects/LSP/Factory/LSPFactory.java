// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.Pattern.*;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.SliderDrawable;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Renderer.RenderType;
import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.OsdFunc;
import ru.m210projects.LSP.Fonts.FontA;
import ru.m210projects.LSP.Fonts.MenuFont;
import ru.m210projects.LSP.Main;
import ru.m210projects.LSP.Types.LSPDatFile;
import ru.m210projects.LSP.Types.MapGroup;

import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.NORMAL;
import static ru.m210projects.LSP.Main.*;

public class LSPFactory extends BuildFactory {

    private final Main app;

    public LSPFactory(Main app) {
        super("stuff.dat");
        this.app = app;

        OsdColor.DEFAULT.setPal(4);
    }

    @Override
    public void drawInitScreen() {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(0, 0, 65536, 0, 770, -128, 0, 10 | 16);
    }

    @Override
    public Engine engine() throws Exception {
        Entry entry = game.cache.getGameDirectory().getEntry("LVART000.DAT");
        if (entry.exists() && !entry.isDirectory()) {
            gResGroup = new LSPDatFile(entry.getName(), 135, entry::getInputStream);
            game.getCache().addGroup(gResGroup, NORMAL);
            Console.out.println("Found " + gResGroup.getSize() + " files in " + gResGroup.getName() + " archive");
        }

        entry = game.cache.getGameDirectory().getEntry("LMART000.DAT");
        if (entry.exists() && !entry.isDirectory()) {
            gMapGroup = new MapGroup(entry::getInputStream);
            game.getCache().addGroup(gMapGroup, NORMAL);
            Console.out.println("Found " + gMapGroup.getSize() + " files in " + gMapGroup.getName() + " archive");
        }

        return Main.engine = new LSPEngine(app);
    }

    @Override
    public Renderer renderer(RenderType type) {
        if (type == RenderType.Software) {
            return new LSPSoftware(app.pCfg);
        }
        if (type == RenderType.PolyGDX) {
            return new LSPPolygdx(app.pCfg);
        } else {
            return new LSPPolymost(app.pCfg);
        }
    }

    @Override
    public DefScript getBaseDef(Engine engine) {
        return new DefScript(engine);
    }

    @Override
    public OsdFunc getOsdFunc() {
        return new LSPOsdFunc();
    }

    @Override
    public MenuHandler menus() {
        return app.menu = new LSPMenuHandler(app);
    }

    @Override
    public FontHandler fonts() {
        return new FontHandler(3) {
            @Override
            protected Font init(int i) {
                if (i == 0) {
                    return new FontA(app.pEngine);
                }
                if (i == 2) {
                    return new MenuFont(app.pEngine);
                }
                return EngineUtils.getLargeFont();
            }
        };
    }

    @Override
    public BuildNet net() {
        return new LSPNetwork(app);
    }

    @Override
    public SliderDrawable slider() {
        return new LSPSliderDrawable();
    }
}
