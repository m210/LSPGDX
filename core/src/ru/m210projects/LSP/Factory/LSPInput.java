// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Factory;

import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Build.Pattern.BuildNet.NetInput;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.LSP.Config.LSPKeys;
import ru.m210projects.LSP.Main;
import ru.m210projects.LSP.Types.Input;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.LSP.Globals.*;
import static ru.m210projects.LSP.Main.cfg;

public class LSPInput extends GameProcessor {

    private boolean crouch;
    private int lPlayerXVel, lPlayerYVel;

    public LSPInput(Main main) {
        super(main);
    }

    public void reset() {
        lPlayerXVel = lPlayerYVel = 0;
    }

    @Override
    public void fillInput(NetInput input) {
        mouseDelta.x /= 4.0f;
        mouseDelta.y /= 16.0f;

        Input gInput = (Input) input;

        if (game.pMenu.gShowMenu || Console.out.isShowing()) {
            return;
        }

        gInput.reset();
        int vel;
        int svel = vel = 0;
        float angvel;
        float horiz = angvel = 0;

        gInput.bits |= isGameKeyJustPressed(GameKeys.Jump) ? 1 : 0;
        gInput.bits |= isGameKeyPressed(GameKeys.Crouch) ? 2 : 0;
        gInput.bits |= isGameKeyPressed(GameKeys.Look_Up) ? 4 : 0;
        gInput.bits |= isGameKeyPressed(GameKeys.Look_Down) ? 8 : 0;
        //16, 32 - zoom in, out

        gInput.bits |= isGameKeyPressed(GameKeys.Run) ? 256 : 0;
        gInput.bits |= isGameKeyJustPressed(GameKeys.Open) ? 1024 : 0;
        gInput.bits |= isGameKeyPressed(GameKeys.Weapon_Fire) ? 2048 : 0;
        gInput.bits |= isGameKeyJustPressed(LSPKeys.Weapon_1) ? 1 << 14 : 0;
        gInput.bits |= isGameKeyJustPressed(LSPKeys.Weapon_2) ? 2 << 14 : 0;
        gInput.bits |= isGameKeyJustPressed(LSPKeys.Weapon_3) ? 3 << 14 : 0;
        if (isGameKeyJustPressed(GameKeys.Previous_Weapon)) {
            gInput.bits |= (4) << 14;
        }
        if (isGameKeyJustPressed(GameKeys.Next_Weapon)) {
            gInput.bits |= (5) << 14;
        }
        if (isGameKeyJustPressed(LSPKeys.Last_Used_Weapon)) {
            gInput.bits |= (6) << 14;
        }
        gInput.bits |= isKeyJustPressed(com.badlogic.gdx.Input.Keys.PAUSE) ? 1 << 17 : 0;
        if (isGameKeyJustPressed(LSPKeys.Crouch_toggle)) {
            crouch = !crouch;
        }

        if (crouch) {
            gInput.bits |= 2;
        }

        int keymove = 20 * TICSPERFRAME;
        int keyturn = 32 * TICSPERFRAME;

        boolean running = ((!cfg.gAutoRun && (gInput.bits & 256) != 0) || ((gInput.bits & 256) == 0 && cfg.gAutoRun));

        if (running) {
            keymove = 32 * TICSPERFRAME;
            keyturn = 48 * TICSPERFRAME;
        }

        if (isGameKeyPressed(GameKeys.Strafe)) {
            if (isGameKeyPressed(GameKeys.Turn_Left)) {
                svel -= -keymove;
            }
            if (isGameKeyPressed(GameKeys.Turn_Right)) {
                svel -= keymove;
            }
            svel = (short) BClipRange(svel - 20 * ctrlGetMouseStrafe(), -keymove, keymove);
        } else {
            if (isGameKeyPressed(GameKeys.Turn_Left)) {
                angvel -= keyturn;
            } else if (isGameKeyPressed(GameKeys.Turn_Right)) {
                angvel += keyturn;
            }
            angvel = BClipRange(angvel + ctrlGetMouseTurn(), -1024, 1024);
        }

        if (isGameKeyPressed(GameKeys.Strafe_Left)) {
            svel += keymove;
        }
        if (isGameKeyPressed(GameKeys.Strafe_Right)) {
            svel -= keymove;
        }
        if (isGameKeyPressed(GameKeys.Move_Forward)) {
            vel += keymove;
        }
        if (isGameKeyPressed(GameKeys.Move_Backward)) {
            vel -= keymove;
        }

        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();

        if (cfg.isgMouseAim()) {
            horiz = BClipRange(ctrlGetMouseLook(!cfg.isgInvertmouse()), -(ydim >> 1), 100 + (ydim >> 1));
        } else {
            vel = (short) BClipRange(vel - ctrlGetMouseMove(), -4 * keymove, 4 * keymove);
        }

        Vector2 stick1 = ctrlGetStick(JoyStick.LOOKING);
        Vector2 stick2 = ctrlGetStick(JoyStick.MOVING);

        float lookx = stick1.x;
        float looky = -stick1.y;

        if (looky != 0) {
            float k = 4.0f;
            horiz = BClipRange(horiz + k * looky * cfg.getJoyLookSpeed(), -(ydim >> 1), 100 + (ydim >> 1));
        }

        if (lookx != 0) {
            float k = 80.0f;
            angvel = (short) BClipRange(angvel + k * lookx * cfg.getJoyTurnSpeed(), -1024, 1024);
        }

        if (stick2.y != 0) {
            float k = 80 * TICSPERFRAME;
            vel = (short) BClipRange(vel - (k * stick2.y), -128, 127);
        }
        if (stick2.x != 0) {
            float k = 80 * TICSPERFRAME;
            svel = (short) BClipRange(svel - (k * stick2.x), -128, 127);
        }

        if (svel < 0) {
            svel = min(svel + 2 * TICSPERFRAME, 0);
        }
        if (svel > 0) {
            svel = max(svel - 2 * TICSPERFRAME, 0);
        }
        if (vel < 0) {
            vel = min(vel + 2 * TICSPERFRAME, 0);
        }
        if (vel > 0) {
            vel = max(vel - 2 * TICSPERFRAME, 0);
        }

        if (followmode && gPlayer[myconnectindex].gViewMode != 3) {
            followvel = vel;
            followsvel = svel;
            followang = angvel;
            return;
        }

        float k = 0.17f;
        float angle = BClampAngle(gPlayer[myconnectindex].ang);

        double xvel = (vel * BCosAngle(angle) + svel * BSinAngle(angle));
        double yvel = (vel * BSinAngle(angle) - svel * BCosAngle(angle));

        double len = Math.sqrt(xvel * xvel + yvel * yvel);
        if (len > (keymove << 14)) {
            xvel = (xvel / len) * (keymove << 14);
            yvel = (yvel / len) * (keymove << 14);
        }

        lPlayerXVel += (int) (xvel * k);
        lPlayerYVel += (int) (yvel * k);

        lPlayerXVel = mulscale(lPlayerXVel, 0xD000, 16);
        lPlayerYVel = mulscale(lPlayerYVel, 0xD000, 16);

        if (klabs(lPlayerXVel) < 2048 && klabs(lPlayerYVel) < 2048) {
            lPlayerXVel = lPlayerYVel = 0;
        }

        gInput.xvel = lPlayerXVel;
        gInput.yvel = lPlayerYVel;
        gInput.angvel = angvel;
        gInput.horiz = horiz;
    }

}
