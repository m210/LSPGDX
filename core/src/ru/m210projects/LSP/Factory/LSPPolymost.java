// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Polymost.Polymost;
import ru.m210projects.Build.Render.Polymost.Polymost2D;
import ru.m210projects.Build.settings.GameConfig;

public class LSPPolymost extends Polymost implements LSPRenderer {

    public LSPPolymost(GameConfig config) {
        super(config);
        globalfog.setFogScale(64);
    }

    @Override
    protected Polymost2D allocOrphoRenderer(Engine engine) {
        return new Polymost2D(this, new LSPMapSettings(engine.getBoardService()));
    }

    @Override
    public void setVisibility(int visibility) {
        this.globalvisibility = visibility;
    }
}
