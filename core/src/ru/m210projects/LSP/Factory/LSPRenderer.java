package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.Render.Renderer;

public interface LSPRenderer extends Renderer {

    void setVisibility(int visibility);
}
