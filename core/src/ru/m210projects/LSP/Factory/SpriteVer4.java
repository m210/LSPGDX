package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

public class SpriteVer4 extends Sprite {

    @Override
    public Sprite readObject(InputStream is) throws IOException {
        this.setX(StreamUtils.readInt(is));
        this.setY(StreamUtils.readInt(is));
        this.setZ(StreamUtils.readInt(is));
        this.setCstat(StreamUtils.readByte(is));
        this.setShade(StreamUtils.readByte(is));
        this.setXrepeat(StreamUtils.readByte(is));
        this.setYrepeat(StreamUtils.readByte(is));
        this.setPicnum(StreamUtils.readShort(is));
        this.setAng(StreamUtils.readShort(is));
        this.setXvel(StreamUtils.readShort(is));
        this.setYvel(StreamUtils.readShort(is));
        this.setZvel(StreamUtils.readShort(is));
        this.setOwner(StreamUtils.readShort(is));
        this.setSectnum(StreamUtils.readShort(is));
        this.setStatnum(StreamUtils.readShort(is));
        this.setLotag(StreamUtils.readShort(is));
        this.setHitag(StreamUtils.readShort(is));
        StreamUtils.readInt(is);
        this.setExtra(-1);
        this.setClipdist(32);

        return this;
    }
}
