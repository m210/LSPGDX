package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.DefaultPaletteManager;
import ru.m210projects.Build.filehandle.Entry;

import java.io.IOException;

public class LSPPaletteManager extends DefaultPaletteManager {

    public LSPPaletteManager(Engine engine, Entry entry) throws IOException {
        super(engine, new LSPPaletteLoader(entry));
    }

}
