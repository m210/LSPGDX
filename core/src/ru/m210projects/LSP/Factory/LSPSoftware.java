// This file is part of LSPGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LSPGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.LSP.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Software.Software;
import ru.m210projects.Build.Render.Software.SoftwareOrpho;
import ru.m210projects.Build.Render.Types.ScreenFade;
import ru.m210projects.Build.settings.GameConfig;

import static java.lang.Math.min;

public class LSPSoftware extends Software implements LSPRenderer {

    public LSPSoftware(GameConfig config) {
        super(config);
    }

    @Override
    protected SoftwareOrpho allocOrphoRenderer(Engine engine) {
        return new SoftwareOrpho(this, new LSPMapSettings(engine.getBoardService()));
    }

    @Override
    public void setVisibility(int visibility) {
        this.globalvisibility = visibility;
    }

    @Override
    public void showScreenFade(ScreenFade screenFade) {
        int red = min(63, screenFade.getRed() >> 2);
        int green = min(63, screenFade.getGreen() >> 2);
        int blue = min(63, screenFade.getBlue() >> 2);

        byte[] palette = engine.getPaletteManager().getBasePalette();

        int k = 0;
        int p = 0;
        for (int i = 0; i < 256; i++) {
            temppal[k++] = (byte) (Math.min((palette[p++] & 0xFF) + red, 63) << 2);
            temppal[k++] = (byte) (Math.min((palette[p++] & 0xFF) + green, 63) << 2);
            temppal[k++] = (byte) (Math.min((palette[p++] & 0xFF) + blue, 63) << 2);
        }

        changepalette(temppal);
    }
}
